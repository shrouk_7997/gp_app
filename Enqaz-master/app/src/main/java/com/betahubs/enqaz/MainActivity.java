package com.betahubs.enqaz;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.app.Config;
import com.betahubs.enqaz.app.MyApplication;
import com.betahubs.enqaz.fragment.FreeRidesFragment;
import com.betahubs.enqaz.fragment.HistoryFragment;
import com.betahubs.enqaz.fragment.MainFragment;
import com.betahubs.enqaz.fragment.PaymentFragment;
import com.betahubs.enqaz.fragment.SettingsFragment;
import com.betahubs.enqaz.fragment.YourTripsFragment;
import com.betahubs.enqaz.util.NotificationUtils;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.messaging.FirebaseMessaging;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();


    @BindView(R.id.bottom_navigation)
    AHBottomNavigation mBottomNavigation;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    private FragmentTransaction transaction;
    private Fragment mainFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        MyApplication application = (MyApplication) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.enableAutoActivityTracking(true);
        mTracker.setScreenName("Image~" + "Main Activity" );
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        createBottomNavication();
        mToolbar.setLogo(R.drawable.home_enqaz_navbar_logo);
        mToolbar.setTitle("");
        mainFragment = new MainFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame, mainFragment).commit();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            String notificationType = bundle.getString("notificationType");
            if (notificationType != null && notificationType.equals("canceledReq")) {
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(this);
                builder.setTitle("")
                        .setMessage(R.string.trip_canceled)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            }
            else if (notificationType != null && notificationType.equals("CanceledNoDrivers")) {
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Sorry")
                        .setMessage(R.string.cant_find_drivers)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();
            }
//            else if (notificationType != null && notificationType.equals("canceledAfterAcceptByCustomer")) {
//                cancelationReasonLayout.setVisibility(View.VISIBLE);
//            }
        }

        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void createBottomNavication() {
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.main, R.drawable.ic_home_black_24dp, R.color.color_navigation_icon);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.profits, R.drawable.ic_profits_black_24dp, R.color.color_navigation_icon);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.history, R.drawable.ic_history_black_24dp, R.color.color_navigation_icon);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.free_ridess, R.drawable.ic_card_giftcard_black_24dp, R.color.color_navigation_icon);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.settings, R.drawable.ic_settings_black_24dp, R.color.color_navigation_icon);

        mBottomNavigation.addItem(item1);
        mBottomNavigation.addItem(item2);
        mBottomNavigation.addItem(item3);
        mBottomNavigation.addItem(item4);
        mBottomNavigation.addItem(item5);

        mBottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        mBottomNavigation.setAccentColor(Color.parseColor("#ffffff"));
        mBottomNavigation.setInactiveColor(Color.parseColor("#80ffffff"));
        mBottomNavigation.setDefaultBackgroundColor(Color.parseColor("#dd4D4D4F"));


        mBottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                switch (position) {
                    case 0:
                        mainFragment = new MainFragment();

                        transaction.replace(R.id.frame, mainFragment).commit();
                        break;
                    case 1:
                        transaction.replace(R.id.frame, new PaymentFragment()).commit();
                        break;
                    case 2:
                        transaction.replace(R.id.frame, new YourTripsFragment()).commit();
                        break;
                    case 3:
                        transaction.replace(R.id.frame, new FreeRidesFragment()).commit();
                        break;
                    case 4:
                        transaction.replace(R.id.frame, new SettingsFragment()).commit();
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

}
