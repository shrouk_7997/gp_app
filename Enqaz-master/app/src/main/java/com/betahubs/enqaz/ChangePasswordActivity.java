package com.betahubs.enqaz;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.app.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ChangePasswordActivity extends BaseActivity {
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.current_password_edittext)
    EditText currentPasswordEdittext;
    @BindView(R.id.new_password_edittext)
    EditText newPasswordEdittext;
    @BindView(R.id.repeat_new_password_edittext)
    EditText repeatNewPasswordEdittext;
    @BindView(R.id.change_password_button)
    Button changePasswordButton;
    private SharedPreferences mSharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void validatePasswords() {

        currentPasswordEdittext.setError(null);
        newPasswordEdittext.setError(null);
        repeatNewPasswordEdittext.setError(null);

       ;
        String  currentPassword = currentPasswordEdittext.getText().toString();
        String  password = newPasswordEdittext.getText().toString();
        String  repeatPassword = repeatNewPasswordEdittext.getText().toString();

        boolean cancel = false;
        View focusView = null;

        //--------------------

        if (TextUtils.isEmpty(currentPassword)) {
            currentPasswordEdittext.setError(getString(R.string.error_field_required));
            focusView = currentPasswordEdittext;
            cancel = true;
        }
        //--------------------
        if (TextUtils.isEmpty(password)) {
            newPasswordEdittext.setError(getString(R.string.error_field_required));
            focusView = newPasswordEdittext;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            newPasswordEdittext.setError(getString(R.string.error_invalid_password));
            focusView = newPasswordEdittext;
            cancel = true;
        }
        //--------------------

        if (TextUtils.isEmpty(repeatPassword)) {
            repeatNewPasswordEdittext.setError(getString(R.string.error_field_required));
            focusView = repeatNewPasswordEdittext;
            cancel = true;
        } else if (!isRepeatedPasswordVaild(password,repeatPassword)) {
            repeatNewPasswordEdittext.setError(getString(R.string.error_invalid_repeat_password));
            focusView = repeatNewPasswordEdittext;
            cancel = true;
        }
        //--------------------
        // Check for a valid email address.


        if (cancel) {
            // There was an error; don't attempt signUp and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //showProgress(true);
            changePassword(currentPassword,password);
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }
    private boolean isRepeatedPasswordVaild(String password, String repeatedPassword) {
        return repeatedPassword.equals(password);
    }

    public void changePassword(String currentPassword, String password) {
        changePasswordButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.resetPassword(mSharedPreferences.getString("token", null), currentPassword,password ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("TAGG", "Register Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            Toast.makeText(ChangePasswordActivity.this, R.string.password_changed, Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            Toast.makeText(ChangePasswordActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(ChangePasswordActivity.this,  getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                changePasswordButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(ChangePasswordActivity.this,  getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                changePasswordButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.change_password_button)
    public void onViewClicked() {
        validatePasswords();
    }
}
