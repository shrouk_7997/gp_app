package com.betahubs.enqaz.api;

import com.betahubs.enqaz.model.AppRates;
import com.betahubs.enqaz.model.Car;
import com.betahubs.enqaz.model.EstimateFare;
import com.betahubs.enqaz.model.History;
import com.betahubs.enqaz.model.Services;
import com.betahubs.enqaz.model.TripDetails;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface EnqazService {
//    String baseURL = "https://enqaz-staging.herokuapp.com/";
    String baseURL = "http://safsor-staging.herokuapp.com/";

    @POST("/user/customer/login")
    @FormUrlEncoded
    Call<ResponseBody> loginVendor(@Field("email") String email, @Field("phoneNumber") String phoneNumber, @Field("password") String password, @Field("fcmToken") String fcmToken);

    @POST("/user/customer/login")
    @FormUrlEncoded
    Call<ResponseBody> socialLogin(@Field("loginWith") String loginWith, @Field("fbUserAccessToken") String fbUserAccessToken,@Field("gUserAccessToken") String gUserAccessToken,  @Field("fcmToken") String fcmToken,
                                   @Field("phoneNumber") String phoneNumber,@Field("password") String password,@Field("email") String email);
    @POST("/user/customer/is-user-exist")
    @FormUrlEncoded
    Call<ResponseBody> isUserExist(@Field("fbAccountId") String fbAccountId,@Field("email") String email);

    @POST("/user/customer/forget-pass-code")
    @FormUrlEncoded
    Call<ResponseBody> forgotPass(@Field("email") String email, @Field("phoneNumber") String phoneNumber);

    @POST("/user/customer/verify-forget-pass-code")
    @FormUrlEncoded
    Call<ResponseBody> verifyForgotPassCode(@Field("passVerificationCode") String passVerificationCode);

    @POST("/user/customer/update-account")
    @FormUrlEncoded
    Call<ResponseBody> updatePassword(@Header("Authorization") String userToken, @Field("password") String Password);

    @POST("/user/customer/update-account")
    @FormUrlEncoded
    Call<ResponseBody> updateProfile(@Header("Authorization") String userToken, @Field("password") String Password,@Field("name") String Name,
                                     @Field("username") String Username,@Field("email") String email,@Field("phoneNumber") String phoneNumber);
    @POST("/user/customer/update-account")
    @Multipart
    Call<ResponseBody> updateProfileImage(@Header("Authorization") String userToken, @Part MultipartBody.Part image);

    @POST("/user/customer/reset-pass")
    @FormUrlEncoded
    Call<ResponseBody> resetPassword(@Header("Authorization") String userToken, @Field("password") String currentPassword,@Field("newPassword") String newPassword);

    @GET("/services/list-all")
    Call<Services> getServicesList();

    @GET("/services/show-app-rate")
    Call<AppRates> getAppRates();

    @POST("/user/customer/register")
    @FormUrlEncoded
    Call<ResponseBody> registerCustomer(@Field("name") String name, @Field("email") String email, @Field("phoneNumber") String phoneNumber, @Field("username") String username, @Field("password") String Password);

     @POST("/requests/request-service")
        @FormUrlEncoded
        Call<ResponseBody> requestService(@Header("Authorization") String token, @Field("serviceId") ArrayList<String> serviceId, @Field("serviceName") ArrayList<String> serviceName, @Field("lat") String pickupLat, @Field("long") String pickupLng,
                                          @Field("paymentType") String paymentType, @Field("carManufactory") String carManufactory, @Field("carModel") String carModel,
                                          @Field("destinationLat") String destinationLat, @Field("destinationLong") String destinationLong,@Field("promoCode") String promoCode);
    @POST("/requests/send-request-admin")
        @FormUrlEncoded
        Call<ResponseBody> sendRequestAdmin(@Header("Authorization") String token, @Field("requestId") String serviceId,  @Field("lat") Double pickupLat, @Field("long") Double pickupLng,
                                            @Field("name") String name,  @Field("phoneNumber") String phoneNumber, @Field("serviceName") ArrayList<String> serviceName);

    @POST("/requests/estimate-fare")
    @FormUrlEncoded
    Call<EstimateFare> estimateFare(@Header("Authorization") String token, @Field("serviceId") ArrayList<String> serviceId, @Field("lat") String pickupLat, @Field("long") String pickupLng,
                                    @Field("destinationLat") String destinationLat, @Field("destinationLong") String destinationLong
                                    );

    @POST("/requests/user-cancel-request")
    @FormUrlEncoded
    Call<ResponseBody> cancelTrip(@Header("Authorization") String token,@Field("requestId") String requestId,@Field("cancellationReason") String cancellationReason
    );
    @POST("/trip/show-trip-details")
    @FormUrlEncoded
    Call<TripDetails> getTripDetails(@Header("Authorization") String token, @Field("requestId") String requestId
    );
    @POST("/requests/user-cancel")
    @FormUrlEncoded
    Call<ResponseBody> cancelTripBeforeAccept(@Header("Authorization") String token,@Field("requestId") String requestId
    );

    @POST("/requests/user-track-his-trip")
    Call<History> getHistory(@Header("Authorization") String token);

    @POST("/requests/is-customer-request")
    @FormUrlEncoded
    Call<ResponseBody> isCustomerInRequest(@Header("Authorization") String token, @Field("requestId") String requestId);

    @GET("/requests/customer-request")
    Call<ResponseBody> isCustomerInRequestSplash(@Header("Authorization") String token);

    @GET("/payment/payment-index")
    Call<ResponseBody> getPaymentKey(@Header("Authorization") String token);

    @GET("/car-selection/show-car-manufactories")
    Call<ResponseBody> getCarManufactories();

    @POST("/car-selection/show-car-model-for-manufactory")
    @FormUrlEncoded
    Call<ResponseBody> getCarModels(@Field("carManufactory") String carManufactory);

    @POST("/car-selection/show-years-for-car-model")
    @FormUrlEncoded
    Call<ResponseBody> getCarYears( @Field("carModel") String carModel);

    @POST("/payment/add-card")
    @FormUrlEncoded
    Call<ResponseBody> sendPaymentTokenIdentifier(@Header("Authorization") String token,@Field("token") String tokenIdentifier, @Field("maskedPan") String maskedPan);

    @POST("/requests/write-feedback")
    @FormUrlEncoded
    Call<ResponseBody> feedback(@Header("Authorization") String token,@Field("requestId") String requestId,@Field("rate") Integer rate,@Field("feedback") String feedback);

    @POST("/promo-codes/activate-promo-code")
    @FormUrlEncoded
    Call<ResponseBody> addPromoCode(@Header("Authorization") String token,@Field("promoCode") String promoCode);

    @GET("/promo-codes/list-my-promos")
    Call<ResponseBody> getMyPromoCodes(@Header("Authorization") String token);

    @GET("/referral/get-my-ref-code")
    Call<ResponseBody> getReferralCode(@Header("Authorization") String token);

    @POST("/cars/add-user-cars")
    Call<ResponseBody> addCar(@Header("Authorization") String token,@Header("Content-Type") String contentType, @Body RequestBody body);

    @GET("/cars/get-my-car")
    Call<ResponseBody> getCars(@Header("Authorization") String token);

    @GET("/payment/get-my-cards")
    Call<ResponseBody> getCards(@Header("Authorization") String token);
}