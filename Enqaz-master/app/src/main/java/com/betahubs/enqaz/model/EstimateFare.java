package com.betahubs.enqaz.model;

public class EstimateFare {

    private Boolean success;
    private Integer firstEstimate;
    private Integer lastEstimate;
    private Integer startingFare;
    private Integer waitingTimeCost;
    private Double kilometersCost;
    private Double distance;
    private String promoCodeFormat;
    private Integer promoCodeDiscountRate;

    public String getMessage() {
        return message;
    }

    private String message;


    public Double getDistance() {
        return distance;
    }

    public Boolean getSuccess() {

        return success;
    }

    public Integer getFirstEstimate() {
        return firstEstimate;
    }

    public Integer getLastEstimate() {
        return lastEstimate;
    }

    public Integer getStartingFare() {
        return startingFare;
    }

    public Integer getWaitingTimeCost() {
        return waitingTimeCost;
    }

    public Double getKilometersCost() {
        return kilometersCost;
    }

    public String getPromoCodeFormat() {
        return promoCodeFormat;
    }

    public Integer getPromoCodeDiscountRate() {
        return promoCodeDiscountRate;
    }
}
