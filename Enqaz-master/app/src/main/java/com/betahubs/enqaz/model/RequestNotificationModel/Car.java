package com.betahubs.enqaz.model.RequestNotificationModel;

import com.google.gson.annotations.SerializedName;

public class Car {


    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    @SerializedName("carNumber")
    private String carNumber;

    public Car(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarNumber() {
        return carNumber;
    }
}