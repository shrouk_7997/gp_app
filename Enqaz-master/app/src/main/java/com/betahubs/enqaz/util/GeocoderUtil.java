package com.betahubs.enqaz.util;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GeocoderUtil {

    public static String getAddress(Context context, Double lat, Double lng) {
        Geocoder geocoder;
        List<Address> addresses;
        Locale current = context.getResources().getConfiguration().locale;

        geocoder = new Geocoder(context, current);

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if(addresses!=null&&addresses.size()>0&&addresses.get(0)!=null) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                return address;
            }
        } catch (IOException ignored) {
            return "";
        }
        return "";
    }

}
