package com.betahubs.enqaz.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.HistoryTripDetailsActivity;
import com.betahubs.enqaz.R;
import com.betahubs.enqaz.model.History;
import com.betahubs.enqaz.util.DateFormaterUtil;
import com.betahubs.enqaz.util.GeocoderUtil;

import java.util.ArrayList;

public class HistoryAdapter extends  RecyclerView.Adapter<HistoryAdapter.RecyclerViewHolder> {
    private Context mContext;
    private ArrayList<History.Data> mHistories;


    public HistoryAdapter(Context context, ArrayList<History.Data> data) {
        mContext = context;
        mHistories = data;
    }


    @Override
    public HistoryAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_list_item,parent,false);
        HistoryAdapter.RecyclerViewHolder viewHolder = new HistoryAdapter.RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.RecyclerViewHolder holder, int position) {

        String a = DateFormaterUtil.formatToDateAndTime(mHistories.get(position).getCreatedAt());
        holder.dateTV.setText(a);

        if(mHistories.get(position).getStartTripLocation()!= null&&mHistories.get(position).getStartTripLocation().getLat()!=null){
            String from = GeocoderUtil.getAddress(mContext,mHistories.get(position).getStartTripLocation().getLat(),mHistories.get(position).getStartTripLocation().getLng());
            holder.pickFromTV.setText(from);}
        else if (mHistories.get(position).getUserLocation()!=null&&mHistories.get(position).getUserLocation().getLat()!=null){
            String address = GeocoderUtil.getAddress(mContext,mHistories.get(position).getUserLocation().getLat(),mHistories.get(position).getUserLocation().getLng());
            holder.pickFromTV.setText(address);}
            
        if(mHistories.get(position).getEndTripLocation()!= null&&mHistories.get(position).getEndTripLocation().getLat()!=null){
            String to = GeocoderUtil.getAddress(mContext,mHistories.get(position).getEndTripLocation().getLat(),mHistories.get(position).getEndTripLocation().getLng());
            holder.dropOffTV.setText(to);}
        //this is used for canceled trips


        holder.finalCost.setText(mHistories.get(position).getFinalCost());
    }

    @Override
    public int getItemCount() {
        return mHistories.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder{
        private TextView dateTV;
        private TextView pickFromTV;
        private TextView dropOffTV;
        private TextView finalCost;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            dateTV = itemView.findViewById(R.id.date_textview);
            pickFromTV = itemView.findViewById(R.id.address_from_textview);
            dropOffTV = itemView.findViewById(R.id.address_to_textview);
            finalCost = itemView.findViewById(R.id.final_cost_textview);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    History.Data trip = mHistories.get(getAdapterPosition());
                    if(trip.getStatus().equals("canceled")) {
                        Toast.makeText(mContext, mContext.getString(R.string.this_trip_was_canceled), Toast.LENGTH_SHORT).show();
                    }
                        else {
                        Intent intent = new Intent(mContext, HistoryTripDetailsActivity.class);
                        intent.putExtra("requestId", mHistories.get(getAdapterPosition()).getId());
                        mContext.startActivity(intent);
                    }
                }
            });
        }
    }
}

