package com.betahubs.enqaz.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.R;
import com.betahubs.enqaz.model.Service;
import com.betahubs.enqaz.model.Services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

public class RequestServiceAdapter extends  RecyclerView.Adapter<RequestServiceAdapter.RecyclerViewHolder> {
        final private ListItemClickListener mOnClickListener;
//        private ArrayList<Histories.History> mHistories;
        private Context mContext;
        private ArrayList<Services.Service> mServices;
    private boolean[] mCheckedServices;

    private int servicesPoints= 0; //max = 2 (winch = 2 points, other services = 1)



    public RequestServiceAdapter(Context context,ListItemClickListener onClickListener,ArrayList<Services.Service> services) {
            mOnClickListener = onClickListener;
            mContext = context;
            mServices = services;
        }

        @Override
        public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_list_item,parent,false);
            RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
            mCheckedServices= new boolean[mServices.size()];
            Arrays.fill(mCheckedServices, Boolean.FALSE);

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerViewHolder holder, int position) {

            String name = mServices.get(position).getName();
            holder.mServiceNameTV.setText(name);
            Locale current = mContext.getResources().getConfiguration().locale;
            if (current.getLanguage().equals("AR")||current.getLanguage().equals("ar")) {
                switch(name) {
                    case "Battery":
                        holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.home_battery_icon));
                        holder.mServiceNameTV.setText("بطارية");
                        break;
                    case "Gasoline":
                        holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.home_gas_icon));
                        holder.mServiceNameTV.setText("بنزين");
                        break;
                    case "Tires":
                        holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.your_trips_details_large_icons));
                        holder.mServiceNameTV.setText("كاوتش");
                        break;
                    case "Winch":
                        holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.home_winch_icon));
                        holder.mServiceNameTV.setText("وينش");
                        break;
                }
            }
            else {
                switch(name){
                    case "Battery":
                        holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.home_battery_icon));
                        break;
                    case "Gasoline":
                        holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.home_gas_icon));
                        break;
                    case "Tires":
                        holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.your_trips_details_large_icons));
                        break;
                    case "Winch":
                        holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.home_winch_icon));
                        break;
            }}
        }

        @Override
        public int getItemCount() {
            return mServices.size();
        }

    public interface ListItemClickListener {
            void onListItemClick(boolean[] selectedServices);
        }

        public class RecyclerViewHolder extends RecyclerView.ViewHolder{
            private TextView mServiceNameTV;
            private ImageView mServiceIV;
            public RecyclerViewHolder(View itemView) {
                super(itemView);
                mServiceIV = itemView.findViewById(R.id.service_imageview);
                mServiceNameTV = itemView.findViewById(R.id.service_name_textview);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(mCheckedServices[getAdapterPosition()]) {
                            if(getAdapterPosition()==0){servicesPoints-=2;}
                            else {servicesPoints--;}
                            itemView.setBackground(mContext.getResources().getDrawable(R.drawable.lightgray_radius3dp_efe9eaec_background));
                            mServiceNameTV.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                            mCheckedServices[getAdapterPosition()] = false;

                        }
                        // if not checked service
                        else {
                            if(getAdapterPosition()==0&&servicesPoints==0){servicesPoints+=2;
                                itemView.setBackground(mContext.getResources().getDrawable(R.drawable.gray_radius3dp_4d4d4f_background));
                                mServiceNameTV.setTextColor(mContext.getResources().getColor(R.color.white));
                                mCheckedServices[getAdapterPosition()] = true;

                            }
                            else if(servicesPoints<2&&getAdapterPosition()!=0){
                               servicesPoints++;
                               itemView.setBackground(mContext.getResources().getDrawable(R.drawable.gray_radius3dp_4d4d4f_background));
                                mServiceNameTV.setTextColor(mContext.getResources().getColor(R.color.white));
                               mCheckedServices[getAdapterPosition()] =true;
                            }
                            else{
                                Toast.makeText(mContext, R.string.cant_select_more_services, Toast.LENGTH_SHORT).show();
                                return;}
                        }
                        mOnClickListener.onListItemClick(mCheckedServices);


                    }
                });
            }
        }
    }

