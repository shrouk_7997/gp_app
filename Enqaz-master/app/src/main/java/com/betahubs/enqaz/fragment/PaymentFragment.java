package com.betahubs.enqaz.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.R;
import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.Config;
import com.betahubs.enqaz.model.PromoCode;
import com.betahubs.enqaz.util.DateFormaterUtil;
import com.google.gson.Gson;
import com.paymob.acceptsdk.IntentConstants;
import com.paymob.acceptsdk.PayActivity;
import com.paymob.acceptsdk.PayActivityIntentKeys;
import com.paymob.acceptsdk.PayResponseKeys;
import com.paymob.acceptsdk.SaveCardResponseKeys;
import com.paymob.acceptsdk.ToastMaker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PaymentFragment extends Fragment {

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    Unbinder unbinder;
    @BindView(R.id.add_new_credit_card_button)
    Button addNewCreditCardButton;
    @BindView(R.id.cash_radiobt_imageview)
    ImageView cashRadiobtImageview;
    @BindView(R.id.credit_radiobt_imageview)
    ImageView creditRadiobtImageview;
    @BindView(R.id.credit_textview)
    TextView creditTextView;
    @BindView(R.id.promocode_title_textview)
    TextView promocodeTitleTextview;
    @BindView(R.id.promo_code_2nd_textview)
    TextView promocode2ndTextview;
    @BindView(R.id.credit_layout)
    ConstraintLayout creditLayout;
    @BindView(R.id.addPromoCodeShowButton)
    ConstraintLayout addPromoCodeShowButton;


    private SharedPreferences mSharedPreferences;
    static final int ACCEPT_PAYMENT_REQUEST = 10;
    private String paymentKey;
    private Call mCall;
    private Call mCall2;
    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, container, false);
        unbinder = ButterKnife.bind(this, view);
        mContext = getContext();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        String paymentType = mSharedPreferences.getString(Config.PAYMENT_TYPE, "Cash");

        if (paymentType.equals("Cash")) {
            cashRadiobtImageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.payment_points_check));
        } else if (paymentType.equals("Visa")) {
            creditRadiobtImageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.payment_points_check));
        }
        getPromoCodes();
        getCards();
        return view;
    }

    private void startPayActivityNoToken(Boolean showSaveCard) {
        Intent pay_intent = new Intent(getContext(), PayActivity.class);
        putNormalExtras(pay_intent);

        pay_intent.putExtra(PayActivityIntentKeys.SAVE_CARD_DEFAULT, true);
        pay_intent.putExtra(PayActivityIntentKeys.SHOW_ALERTS, showSaveCard);
        pay_intent.putExtra(PayActivityIntentKeys.SHOW_SAVE_CARD, showSaveCard);
        pay_intent.putExtra(PayActivityIntentKeys.THEME_COLOR, 0xff4d4d4f);

        startActivityForResult(pay_intent, ACCEPT_PAYMENT_REQUEST);
    }

    private void putNormalExtras(Intent intent) {
        // Pass the correct values for the billing data keys
        intent.putExtra(PayActivityIntentKeys.FIRST_NAME, "Mr/Mrs");
        intent.putExtra(PayActivityIntentKeys.LAST_NAME, mSharedPreferences.getString(Config.USER_NAME,"none"));
        intent.putExtra(PayActivityIntentKeys.BUILDING, "none");
        intent.putExtra(PayActivityIntentKeys.FLOOR, "none");
        intent.putExtra(PayActivityIntentKeys.APARTMENT, "none");
        intent.putExtra(PayActivityIntentKeys.CITY, "none");
        intent.putExtra(PayActivityIntentKeys.STATE, "none");
        intent.putExtra(PayActivityIntentKeys.COUNTRY, "Egypt");
        intent.putExtra(PayActivityIntentKeys.EMAIL,  mSharedPreferences.getString(Config.USER_EMAIL,"none"));
        intent.putExtra(PayActivityIntentKeys.PHONE_NUMBER, mSharedPreferences.getString(Config.USER_PHONENUMBER,"none"));
        intent.putExtra(PayActivityIntentKeys.POSTAL_CODE, "none");

        intent.putExtra(PayActivityIntentKeys.PAYMENT_KEY, paymentKey);

        intent.putExtra(PayActivityIntentKeys.THREE_D_SECURE_ACTIVITY_TITLE, "Verification");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //PAYMOB -->
        Bundle extras = data.getExtras();

        if (requestCode == ACCEPT_PAYMENT_REQUEST) {

            if (resultCode == IntentConstants.USER_CANCELED) {
                // User canceled and did no payment request was fired
                ToastMaker.displayShortToast(getActivity(), "User canceled!!");
            } else if (resultCode == IntentConstants.MISSING_ARGUMENT) {
                // You forgot to pass an important key-value pair in the intent's extras
                ToastMaker.displayShortToast(getActivity(), "Missing Argument == " + extras.getString(IntentConstants.MISSING_ARGUMENT_VALUE));
            } else if (resultCode == IntentConstants.TRANSACTION_ERROR) {
                // An error occurred while handling an API's response
                ToastMaker.displayShortToast(getActivity(), "Reason == " + extras.getString(IntentConstants.TRANSACTION_ERROR_REASON));
            } else if (resultCode == IntentConstants.TRANSACTION_REJECTED) {
                // User attempted to pay but their transaction was rejected

                // Use the static keys declared in PayResponseKeys to extract the fields you want
                ToastMaker.displayShortToast(getActivity(), extras.getString(PayResponseKeys.DATA_MESSAGE));
            } else if (resultCode == IntentConstants.TRANSACTION_REJECTED_PARSING_ISSUE) {
                // User attempted to pay but their transaction was rejected. An error occured while reading the returned JSON
                ToastMaker.displayShortToast(getActivity(), extras.getString(IntentConstants.RAW_PAY_RESPONSE));
            } else if (resultCode == IntentConstants.TRANSACTION_SUCCESSFUL) {
                // User finished their payment successfully

                // Use the static keys declared in PayResponseKeys to extract the fields you want
                ToastMaker.displayShortToast(getActivity(), extras.getString(PayResponseKeys.DATA_MESSAGE));
            } else if (resultCode == IntentConstants.TRANSACTION_SUCCESSFUL_PARSING_ISSUE) {
                // User finished their payment successfully. An error occured while reading the returned JSON.
                ToastMaker.displayShortToast(getActivity(), "TRANSACTION_SUCCESSFUL - Parsing Issue");

                // ToastMaker.displayShortToast(this, extras.getString(IntentConstants.RAW_PAY_RESPONSE));
            } else if (resultCode == IntentConstants.TRANSACTION_SUCCESSFUL_CARD_SAVED) {
                // User finished their payment successfully and card was saved.
                Log.i("TOKENIDENTIFIER REQUEST", extras.getString(SaveCardResponseKeys.TOKEN));
                addCardRequest(extras.getString(SaveCardResponseKeys.TOKEN), extras.getString(SaveCardResponseKeys.MASKED_PAN));
                // Use the static keys declared in PayResponseKeys to extract the fields you want
                // Use the static keys declared in SaveCardResponseKeys to extract the fields you want
//                ToastMaker.displayShortToast(this, "Token == " + extras.getString(SaveCardResponseKeys.TOKEN));
            } else if (resultCode == IntentConstants.USER_CANCELED_3D_SECURE_VERIFICATION) {
                ToastMaker.displayShortToast(getActivity(), "User canceled 3-d secure verification!!");

                // Note that a payment process was attempted. You can extract the original returned values
                // Use the static keys declared in PayResponseKeys to extract the fields you want
                ToastMaker.displayShortToast(getActivity(), extras.getString(PayResponseKeys.PENDING));
            } else if (resultCode == IntentConstants.USER_CANCELED_3D_SECURE_VERIFICATION_PARSING_ISSUE) {
                ToastMaker.displayShortToast(getActivity(), "User canceled 3-d scure verification - Parsing Issue!!");

                // Note that a payment process was attempted.
                // User finished their payment successfully. An error occured while reading the returned JSON.
                ToastMaker.displayShortToast(getActivity(), extras.getString(IntentConstants.RAW_PAY_RESPONSE));
            }
        }
    }


    private void getPaymentKey() {
        addNewCreditCardButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.getPaymentKey(mSharedPreferences.getString("token", null)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    String result = null;
                    try {
                        result = response.body().string();

                        Log.d("REQUEST ACTIVITY", "Payment key " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            paymentKey = jsonObject.getString("token");
                            startPayActivityNoToken(false);
                        } else {
                            Toast.makeText(getContext(), "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
                addNewCreditCardButton.setEnabled(true);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
                addNewCreditCardButton.setEnabled(true);

            }
        });
    }

    private void addCardRequest(String tokenIdentifier, final String maskedPan) {
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.sendPaymentTokenIdentifier(mSharedPreferences.getString("token", null)
                , tokenIdentifier, maskedPan).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("AddCard RESPONSE:", result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");

                        if (success) {
                            SharedPreferences.Editor editor = mSharedPreferences.edit();
                            editor.putString(Config.MASKED_PAN, maskedPan);
                            editor.putString(Config.PAYMENT_TYPE, "Visa");
                            editor.apply();
                            creditRadiobtImageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.payment_points_check));
                            cashRadiobtImageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.payment_points_non_checked));


                            creditLayout.setVisibility(View.VISIBLE);
                            creditTextView.setText(maskedPan);
                            Toast.makeText(getContext(), R.string.card_is_saved, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getContext(), "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                }
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.add_new_credit_card_button, R.id.credit_layout, R.id.cash_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cash_layout:
                mSharedPreferences.edit().putString(Config.PAYMENT_TYPE, "Cash").apply();
                cashRadiobtImageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.payment_points_check));
                creditRadiobtImageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.payment_points_non_checked));
                break;
            case R.id.credit_layout:
                mSharedPreferences.edit().putString(Config.PAYMENT_TYPE, "Visa").apply();
                cashRadiobtImageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.payment_points_non_checked));
                creditRadiobtImageview.setImageDrawable(getContext().getResources().getDrawable(R.drawable.payment_points_check));

                break;
            case R.id.add_new_credit_card_button:
                getPaymentKey();
                break;
        }
    }


    @OnClick({R.id.addPromoCodeShowButton})
    public void onPromoButtonsClicked(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Promo Code");
// I'm using fragment here so I'm using getView() to provide ViewGroup
// but you can provide here any other instance of ViewGroup from your Fragment / Activity
        View viewInflated = LayoutInflater.from(getContext()).inflate(R.layout.input_dialog_layout, (ViewGroup) getView(), false);
// Set up the input
        final EditText input = (EditText) viewInflated.findViewById(R.id.input);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        builder.setView(viewInflated);

// Set up the buttons
        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                String promoCode = input.getText().toString();
                if (!promoCode.isEmpty()) {
                    addPromoCode(promoCode);
                }
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
//                requestLayout.setVisibility(View.GONE);
    }

    private void addPromoCode(String promoCode) {
        addNewCreditCardButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.addPromoCode(mSharedPreferences.getString("token", null), promoCode).
                enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                String result = response.body().string();

                                Log.d("addPromo PaymentFrag", "Payment key " + result);
                                JSONObject jsonObject = new JSONObject(result);
                                boolean success = jsonObject.getBoolean("success");
                                if (success) {
                                    String promocodeJSON = jsonObject.getJSONObject("promoCode").toString();
                                    Gson gson = new Gson();
                                    PromoCode promoCode1 = gson.fromJson(promocodeJSON, PromoCode.class);
                                    promocodeTitleTextview.setText(promoCode1.code);
                                    String date = DateFormaterUtil.formatToDateOnly(promoCode1.expiresAt);
                                    String secondText = String.format(getString(R.string.promo_code_description), promoCode1.discountRate, date);
                                    promocode2ndTextview.setText(secondText);
                                    addPromoCodeShowButton.setBackground(getContext().getResources().getDrawable(R.drawable.green_radius2dp_background));
                                } else {
                                    Toast.makeText(getContext(), "" + jsonObject.optString("message"), Toast.LENGTH_SHORT).show();

                                }
                            } catch (IOException | JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                        }
                        mProgressBar.setVisibility(View.GONE);
                        addNewCreditCardButton.setEnabled(true);

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if(mContext!=null&&mProgressBar!=null) {
                            Toast.makeText(getContext(), getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
                            mProgressBar.setVisibility(View.GONE);
                            addNewCreditCardButton.setEnabled(true);
                        }
                    }
                });
    }

    private void getPromoCodes() {
        addNewCreditCardButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        mCall2 =enqazService.getMyPromoCodes(mSharedPreferences.getString("token", null));
                mCall2.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                String result = response.body().string();

                                Log.d("addPromo PaymentFrag", "Payment key " + result);
                                JSONObject jsonObject = new JSONObject(result);
                                boolean success = jsonObject.getBoolean("success");
                                if (success) {
                                    String promocodeJSON = jsonObject.getJSONObject("promoCode").toString();
                                    Gson gson = new Gson();
                                    PromoCode promoCode1 = gson.fromJson(promocodeJSON, PromoCode.class);
                                    promocodeTitleTextview.setText(promoCode1.code);
                                    String date = DateFormaterUtil.formatToDateOnly(promoCode1.expiresAt);
                                    String secondText = String.format(getString(R.string.promo_code_description), promoCode1.discountRate, date);
                                    promocode2ndTextview.setText(secondText);
                                    addPromoCodeShowButton.setBackground(mContext.getResources().getDrawable(R.drawable.green_radius2dp_background));

                                } else {
                                }
                            } catch (IOException | JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                        }
                        mProgressBar.setVisibility(View.GONE);
                        addNewCreditCardButton.setEnabled(true);

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        if(mContext!=null&&mProgressBar!=null) {
                            Toast.makeText(mContext, getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
                            mProgressBar.setVisibility(View.GONE);
                            addNewCreditCardButton.setEnabled(true);
                        }
                    }
                });
    }
    private void getCards() {
        addNewCreditCardButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        mCall =enqazService.getCards(mSharedPreferences.getString("token", null));
        mCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            JSONArray cardsJSON = jsonObject.getJSONArray("cards");
                            JSONObject cardJSON = cardsJSON.getJSONObject(cardsJSON.length()-1);
                            String maskedPan = cardJSON.getString("maskedPan");
                            if (maskedPan != null) {
                                creditLayout.setVisibility(View.VISIBLE);
                                creditTextView.setText(maskedPan);
                            }
                        } else {
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
                addNewCreditCardButton.setEnabled(true);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(mContext!=null&&mProgressBar!=null) {
                    Toast.makeText(mContext, getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
                    mProgressBar.setVisibility(View.GONE);
                    addNewCreditCardButton.setEnabled(true);
                }
            }
        });
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mCall!=null){
            mCall.cancel();
        }
        if (mCall2 !=null){mCall2.cancel();}
    }
}
