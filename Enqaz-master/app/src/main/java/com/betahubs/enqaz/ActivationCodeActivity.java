package com.betahubs.enqaz;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivationCodeActivity extends BaseActivity {
    @BindView(R.id.submit_button)
    Button mSubmitButton;
    @BindView(R.id.resend_code_textview)
    TextView resendCodeTV;
    @BindView(R.id.email_or_phone_textview)
    TextView emailOrPhoneTV;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.activation_code_edittext)
    EditText activationCodeET;
    private String comingFromAction;
    private String email;
    private String phoneNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass_code);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        comingFromAction = intent.getStringExtra("COMING_FROM");

         email =intent.getStringExtra(Intent.EXTRA_EMAIL);
        phoneNumber =intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        if(email!=null){
        emailOrPhoneTV.setText(email);}
        else if (phoneNumber!=null){emailOrPhoneTV.setText(phoneNumber);}
        resendCode();
    }
    @OnClick(R.id.resend_code_textview) void resendCode(){
        resendCodeTV.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(EnqazService.baseURL).build();
        EnqazService enqazService =  retrofit.create(EnqazService.class);
        enqazService.forgotPass(email,phoneNumber).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try {
                        String result = response.body().string();
                        Log.d( "TAG" , " Response: "+ result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if(success){
                            Toast.makeText(ActivationCodeActivity.this, R.string.code_is_resent, Toast.LENGTH_LONG).show();
                        }
                        else {Toast.makeText(ActivationCodeActivity.this,"" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(ActivationCodeActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
                resendCodeTV.setEnabled(true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(ActivationCodeActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
                resendCodeTV.setEnabled(true);
            }
        });
    }
    @OnClick(R.id.submit_button) void onSubmitClicked(){
            mSubmitButton.setEnabled(false);
            mProgressBar.setVisibility(View.VISIBLE);
            EnqazService enqazService = ApiServiceUtil.getEnqazService();
            enqazService.verifyForgotPassCode(activationCodeET.getText().toString()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()){
                        try {
                            String result = response.body().string();
                            Log.d( "VErifycode" , " Response: "+ result);
                            JSONObject jsonObject = new JSONObject(result);
                            boolean success = jsonObject.getBoolean("success");
                            if(success){
                                if(comingFromAction!=null&&comingFromAction.equals("register")){
                                    Intent intent = new Intent(ActivationCodeActivity.this,IntroActivity.class);
                                    Toast.makeText(ActivationCodeActivity.this, getString(R.string.account_activated_please_login), Toast.LENGTH_LONG).show();
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                                else{
                                    String apiToken = jsonObject.getString("token");
                                    if(apiToken!=null&&!apiToken.isEmpty()){
                                        Intent intent = new Intent(ActivationCodeActivity.this,ForgotPassEnterNewPassActvity.class);
                                        intent.putExtra("token",apiToken);
                                        startActivity(intent);
                                    }
                                }
                            }
                            else {
                                Toast.makeText(ActivationCodeActivity.this,"" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        Toast.makeText(ActivationCodeActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();

                    }
                    mProgressBar.setVisibility(View.GONE);
                    mSubmitButton.setEnabled(true);
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(ActivationCodeActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    mProgressBar.setVisibility(View.GONE);
                    mSubmitButton.setEnabled(true);
                }
            });
    }
}
