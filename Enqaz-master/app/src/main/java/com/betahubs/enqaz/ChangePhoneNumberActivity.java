package com.betahubs.enqaz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.app.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ChangePhoneNumberActivity extends BaseActivity {

    @BindView(R.id.phone_number_edittext)
    EditText phoneNumberEdittext;
    @BindView(R.id.change_password_button)
    Button changePasswordButton;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_phone_number);
        ButterKnife.bind(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void validatePhone() {

        phoneNumberEdittext.setError(null);

        // Store values at the time of the login attempt.
        String phone = phoneNumberEdittext.getText().toString();

        boolean cancel = false;
        View focusView = null;
        if (isPhoneVaild(phone)) {
        } else {
            phoneNumberEdittext.setError(getString(R.string.error_invalid_phone));
            focusView = phoneNumberEdittext;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            changePhoneNumber(phone);
        }
    }

    private void changePhoneNumber(String phone) {
        changePasswordButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.updateProfile(mSharedPreferences.getString("token", null), null, null, null, null, phone).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("TAGG", "Register Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            Toast.makeText(ChangePhoneNumberActivity.this, R.string.phone_updated, Toast.LENGTH_LONG).show();
                            mSharedPreferences.edit().putString(Config.USER_PHONENUMBER,phone).apply();
                            Intent intent = new Intent(ChangePhoneNumberActivity.this,EditProfileActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(ChangePhoneNumberActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                changePasswordButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(ChangePhoneNumberActivity.this,getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                changePasswordButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private boolean isPhoneVaild(String phoneNumber) {
        //String regexStr = "^[+]?[0-9]{10,13}$";
        return PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber) && phoneNumber.length() < 15 && phoneNumber.length() > 7;
    }

    @OnClick(R.id.change_password_button)
    public void onViewClicked() {
        validatePhone();
    }
}