package com.betahubs.enqaz.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.betahubs.enqaz.R;
import com.betahubs.enqaz.model.AppRates;

public class AppRatesAdapter  extends  RecyclerView.Adapter<AppRatesAdapter.RecyclerViewHolder> {
    private Context mContext;
    private AppRates.Rates mRates;


    public AppRatesAdapter(Context context,AppRates.Rates rates) {
        mContext = context;
        mRates = rates;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_rates_list_item,parent,false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        String name = mRates.getServices()[position].getName();
        holder.mServiceNameTV.setText(name);
        switch(name){
            case "Battery":
                holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.setting_app_rates_battery_icon));
                break;
            case "Gasoline":
                holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.settings_app_rates_gas_icon));
                break;
            case "Tires":
                holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.settings_app_rates_tires_icon));
                break;
            case "Winch":
                holder.mServiceIV.setImageDrawable(mContext.getResources().getDrawable(R.drawable.settings_app_rates_winch_icon));
                break;
        }
        holder.mKilometerPriceTV.setText(mRates.getKilometerPrice()+ mContext.getString(R.string.le));
        holder.mStartingFareTV.setText(mRates.getServices()[position].getBaseFare() + mContext.getString(R.string.le));
        holder.mWaitingTimeCostTV.setText(mRates.getWaitingTimePrice() + mContext.getString(R.string.le));
    }

    @Override
    public int getItemCount() {
        return mRates.getServices().length;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder{
        private TextView mServiceNameTV;
        private ImageView mServiceIV;
        private TextView mKilometerPriceTV;
        private TextView mStartingFareTV;
        private TextView mWaitingTimeCostTV;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            mServiceIV = itemView.findViewById(R.id.service_imageview);
            mServiceNameTV = itemView.findViewById(R.id.service_name_textview);
            mKilometerPriceTV = itemView.findViewById(R.id.estimation_details_kilometer_cost_tv);
            mStartingFareTV = itemView.findViewById(R.id.estimation_details_startingfare_tv);
            mWaitingTimeCostTV = itemView.findViewById(R.id.estimation_details_waitingtime_cost_tv);
        }
    }
}