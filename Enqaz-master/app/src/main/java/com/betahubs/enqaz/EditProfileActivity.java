package com.betahubs.enqaz;

import android.Manifest;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.app.Config;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EditProfileActivity extends BaseActivity {

//    @BindView(R.id.username_edittext)
//    EditText usernameEdittext;
//    @BindView(R.id.mobileET)
//    EditText mobileEditText;
//    @BindView(R.id.nameET)
//    EditText nameEditText;
//    @BindView(R.id.emailET)
//    EditText emailEditText;
//    @BindView(R.id.passwordET)
//    EditText passwordEditText;
//    @BindView(R.id.update_profile_button)
    Button updateProfileButton;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.profile_imageview)
    ImageView profileImageview;
    @BindView(R.id.name_textview)
    TextView nameTextview;
    @BindView(R.id.email_textview)
    TextView emailTextview;
    @BindView(R.id.phonenumber_textview)
    TextView phonenumberTextview;
    @BindView(R.id.name_edittext)
    EditText nameEdittext;
    @BindView(R.id.submit_name_button)
    Button submitNameButton;
    @BindView(R.id.change_name_layout)
    ConstraintLayout changeNameLayout;
    @BindView(R.id.email_edittext)
    EditText emailEdittext;
    @BindView(R.id.submit_email_button)
    Button submitEmailButton;
    @BindView(R.id.change_email_layout)
    ConstraintLayout changeEmailLayout;
    private boolean isRequesting = false;
    private SharedPreferences mSharedPreferences;
//    private Uri fileUri, cameraImageUri;
//    private String mediaPath;
//    private static Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(EditProfileActivity.this);

        nameTextview.setText(mSharedPreferences.getString(Config.USER_NAME,""));
        emailTextview.setText(mSharedPreferences.getString(Config.USER_EMAIL,""));
        phonenumberTextview.setText(mSharedPreferences.getString(Config.USER_PHONENUMBER,""));

//        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder(); StrictMode.setVmPolicy(builder.build());
//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 0);
//        }
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 33);
//        }

    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == 100 && resultCode == RESULT_OK) {
//
//            fileUri = data.getData();
//
//            mediaPath = getPathFromUri(this, fileUri);
//            uploadImage(mediaPath);
//            try {
//                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri);
//                Bitmap rotatedImg = getRotatedImage();
//                profileImageview.setImageBitmap(rotatedImg);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }

//        if (requestCode == 200 && resultCode == RESULT_OK) {
//
//            if (cameraImageUri != null) {
//
//                mediaPath = getPathFromUri(this, cameraImageUri);
//                Uri selectedImage = cameraImageUri;
//                getContentResolver().notifyChange(selectedImage, null);
//                uploadImage(mediaPath);
//                try {
//                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), cameraImageUri);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                if (bitmap != null) {
//                    Bitmap rotatedImg = getRotatedImage();
//                    profileImageview.setImageBitmap(rotatedImg);
//                } else {
//                    Toast.makeText(this, R.string.error_while_capturing_image, Toast.LENGTH_LONG).show();
//                }
//            } else {
//                Toast.makeText(this,  R.string.error_while_capturing_image, Toast.LENGTH_LONG).show();
//            }
//
////            try {
////                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), fileUri);
////            } catch (IOException e) {
////                e.printStackTrace();
////            }
////            profileImage.setImageBitmap(bitmap);
//        }
//    }
//    private void uploadImage(String mediaPath) {
//
//        File imageFile = new File(mediaPath);
//
//
//        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), imageFile);
//        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", imageFile.getName(), requestBody);
//        Retrofit retrofit = new Retrofit.Builder().baseUrl(EnqazService.baseURL).build();
//          EnqazService enqazService = retrofit.create(EnqazService.class);
//        enqazService.updateProfileImage(mSharedPreferences.getString("token",null), fileToUpload).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    if(response.isSuccessful()){
//                    Log.d("IMAGEEEEUPLOAD",response.body().string()+"");}
//                    else{
//
//
//                }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//
//                Toast.makeText(EditProfileActivity.this, t.getMessage() + "\n" + t.toString(), Toast.LENGTH_SHORT).show();
//
//            }
//        });
//    }
//    public Bitmap getRotatedImage(){
//        try {
//            ExifInterface ei = new ExifInterface(mediaPath);
//            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
//                    ExifInterface.ORIENTATION_UNDEFINED);
//
//            Bitmap rotatedBitmap = null;
//            switch(orientation) {
//
//                case ExifInterface.ORIENTATION_ROTATE_90:
//                    rotatedBitmap = rotateImage(bitmap, 90);
//                    break;
//
//                case ExifInterface.ORIENTATION_ROTATE_180:
//                    rotatedBitmap = rotateImage(bitmap, 180);
//                    break;
//
//                case ExifInterface.ORIENTATION_ROTATE_270:
//                    rotatedBitmap = rotateImage(bitmap, 270);
//                    break;
//
//                case ExifInterface.ORIENTATION_NORMAL:
//                default:
//                    rotatedBitmap = bitmap;
//            }
//            return rotatedBitmap;
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }
//    public static Bitmap rotateImage(Bitmap source, float angle) {
//        Matrix matrix = new Matrix();
//        matrix.postRotate(angle);
//        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
//                matrix, true);
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        if (requestCode == 0) {
//            if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED
//                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//            }
//        }
//    }

    public void updateProfile(String name, final String email, String phone, String password, String username) {
        isRequesting = true;
        submitEmailButton.setEnabled(false);
        submitNameButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        Retrofit retrofit = new Retrofit.Builder().client(okHttpClient).baseUrl(EnqazService.baseURL).build();
        EnqazService enqazService = retrofit.create(EnqazService.class);
        enqazService.updateProfile(mSharedPreferences.getString("token", null), password, name, username, email, phone).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("TAGG", "Register Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                         boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            Toast.makeText(EditProfileActivity.this, R.string.account_updated, Toast.LENGTH_LONG).show();
                            //it's either an email or name which was changed
                            if(email!=null&&!email.isEmpty()){
                                mSharedPreferences.edit().putString(Config.USER_EMAIL,email).apply();
                            }
                            else if (name!=null&&!name.isEmpty()){
                                mSharedPreferences.edit().putString(Config.USER_NAME,name).apply();
                            }
                            recreate(); //recreate activtiy so changes could take effect on screen
                        }
                         else {
                            Toast.makeText(EditProfileActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    Toast.makeText(EditProfileActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                }
                isRequesting = false;
                submitEmailButton.setEnabled(true);
                submitNameButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(EditProfileActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                isRequesting = false;
                submitEmailButton.setEnabled(true);
                submitNameButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @OnClick({R.id.profile_imageview, R.id.change_name_buttton, R.id.change_email_button, R.id.change_phone_button, R.id.change_password_button, R.id.close_change_name_button, R.id.submit_name_button, R.id.close_change_email_button, R.id.submit_email_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.profile_imageview:
//                showPictureDialog();
                break;
            case R.id.change_name_buttton:
                changeNameLayout.setVisibility(View.VISIBLE);
                nameEdittext.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(nameEdittext, InputMethodManager.SHOW_IMPLICIT);
                break;
            case R.id.change_email_button:
                changeEmailLayout.setVisibility(View.VISIBLE);
                emailEdittext.requestFocus();
                InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm2.showSoftInput(emailEdittext, InputMethodManager.SHOW_IMPLICIT);
                break;
            case R.id.change_phone_button:
                Intent intent1 = new Intent(EditProfileActivity.this,ChangePhoneNumberActivity.class);
                startActivity(intent1);
                finish();
                break;
            case R.id.change_password_button:
                Intent intent2 = new Intent(EditProfileActivity.this,ChangePasswordActivity.class);
                startActivity(intent2);
                break;
            case R.id.close_change_name_button:
                changeNameLayout.setVisibility(View.GONE);
                break;
            case R.id.submit_name_button:
                String name = nameEdittext.getText().toString();
                if(!name.isEmpty()){
                    updateProfile(name,null,null,null,null);}
                break;
            case R.id.close_change_email_button:
                changeEmailLayout.setVisibility(View.GONE);
                break;
            case R.id.submit_email_button:
                String email = emailEdittext.getText().toString();
                if(!email.isEmpty()&&email.contains("@") && email.contains(".")) {
                    updateProfile(null, email, null, null, null);
                }
                else{emailEdittext.setError(getString(R.string.please_enter_valid_mail));}
                break;
        }
    }
//    public static String getPathFromUri(final Context context, final Uri uri) {
//
//        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
//
//        // DocumentProvider
//        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
//            // ExternalStorageProvider
//            if (isExternalStorageDocument(uri)) {
//                final String docId = DocumentsContract.getDocumentId(uri);
//                final String[] split = docId.split(":");
//                final String type = split[0];
//
//                if ("primary".equalsIgnoreCase(type)) {
//                    return Environment.getExternalStorageDirectory() + "/" + split[1];
//                }
//
//                // TODO handle non-primary volumes
//            }
//            // DownloadsProvider
//            else if (isDownloadsDocument(uri)) {
//
//                final String id = DocumentsContract.getDocumentId(uri);
//                final Uri contentUri = ContentUris.withAppendedId(
//                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//
//                return getDataColumn(context, contentUri, null, null);
//            }
//            // MediaProvider
//            else if (isMediaDocument(uri)) {
//                final String docId = DocumentsContract.getDocumentId(uri);
//                final String[] split = docId.split(":");
//                final String type = split[0];
//
//                Uri contentUri = null;
//                if ("image".equals(type)) {
//                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//                } else if ("video".equals(type)) {
//                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
//                } else if ("audio".equals(type)) {
//                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//                }
//
//                final String selection = "_id=?";
//                final String[] selectionArgs = new String[]{
//                        split[1]
//                };
//
//                return getDataColumn(context, contentUri, selection, selectionArgs);
//            }
//        }
//        // MediaStore (and general)
//        else if ("content".equalsIgnoreCase(uri.getScheme())) {
//
//            // Return the remote address
//            if (isGooglePhotosUri(uri))
//                return uri.getLastPathSegment();
//
//            return getDataColumn(context, uri, null, null);
//        }
//        // File
//        else if ("file".equalsIgnoreCase(uri.getScheme())) {
//            return uri.getPath();
//        }
//
//        return null;
//    }
//
//    public static String getDataColumn(Context context, Uri uri, String selection,
//                                       String[] selectionArgs) {
//
//        Cursor cursor = null;
//        final String column = "_data";
//        final String[] projection = {
//                column
//        };
//
//        try {
//            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
//                    null);
//            if (cursor != null && cursor.moveToFirst()) {
//                final int index = cursor.getColumnIndexOrThrow(column);
//                return cursor.getString(index);
//            }
//        } finally {
//            if (cursor != null)
//                cursor.close();
//        }
//        return null;
//    }
//
//
//    /**
//     * @param uri The Uri to check.
//     * @return Whether the Uri authority is ExternalStorageProvider.
//     */
//    public static boolean isExternalStorageDocument(Uri uri) {
//        return "com.android.externalstorage.documents".equals(uri.getAuthority());
//    }
//
//    /**
//     * @param uri The Uri to check.
//     * @return Whether the Uri authority is DownloadsProvider.
//     */
//    public static boolean isDownloadsDocument(Uri uri) {
//        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
//    }
//
//    /**
//     * @param uri The Uri to check.
//     * @return Whether the Uri authority is MediaProvider.
//     */
//    public static boolean isMediaDocument(Uri uri) {
//        return "com.android.providers.media.documents".equals(uri.getAuthority());
//    }
//
//    /**
//     * @param uri The Uri to check.
//     * @return Whether the Uri authority is Google Photos.
//     */
//    public static boolean isGooglePhotosUri(Uri uri) {
//        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
//    }
//
//    private void showPictureDialog() {
//
//        final AlertImageDialog[] items = {
//                new AlertImageDialog(getString(R.string.select_photo_gallery), android.R.drawable.ic_menu_gallery),
//                new AlertImageDialog(getString(R.string.capture_photo), android.R.drawable.ic_menu_camera),
//                new AlertImageDialog(getString(R.string.cancel), 0),//no icon for this one
//        };
//
//        ListAdapter adapter = new ArrayAdapter<AlertImageDialog>(
//                this,
//                R.layout.image_picker_dialog,
//                R.id.tv_dialog_image,
//                items) {
//            public View getView(int position, View convertView, ViewGroup parent) {
//                //Use super class to create the View
//                View v = super.getView(position, convertView, parent);
//                TextView tv = (TextView) v.findViewById(R.id.tv_dialog_image);
//
//                //Put the image on the TextView
//                tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);
//
//                //Add margin between image and text (support various screen densities)
//                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
//                tv.setCompoundDrawablePadding(dp5);
//
//                return v;
//            }
//        };
//
//        new AlertDialog.Builder(this)
//                .setTitle("Update Your Photo")
//                .setIcon(R.drawable.on_ride_driver_empty_image)
//
//                .setAdapter(adapter, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int item) {
//
//                        switch (item) {
//                            case 0:
//                                choosePhotoFromGallery();
//                                break;
//
//                            case 1:
//                                takePhotoFromCamera();
//                                break;
//
//                            case 2:
//                                dialog.dismiss();
//                        }
//                    }
//                }).show();
//
//
////        final android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(this);
////
////        pictureDialog.setTitle("Update Your Photo");
////
////        String[] pictureDialogItems = {
////                "Select photo from Gallery",
////                "Capture photo from Camera",
////                "Cancel"};
////        pictureDialog.setIcon(R.drawable.rate);
////
////        pictureDialog.setItems(pictureDialogItems,
////                new DialogInterface.OnClickListener() {
////                    @Override
////                    public void onClick(DialogInterface dialog, int which) {
////                        switch (which) {
////                            case 0:
////                                choosePhotoFromGallery();
////                                break;
////
////                            case 1:
////                                takePhotoFromCamera();
////                                break;
////
////                            case 2:
////                                dialog.dismiss();
////                        }
////                    }
////                });
////
////        pictureDialog.show();
//    }
//    public void choosePhotoFromGallery() {
//
//        Intent galleryIntent = new Intent();
//        galleryIntent.setType("image/jpeg");
//        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
//
//        startActivityForResult(galleryIntent, 100);
//    }
//
//    private void takePhotoFromCamera() {
//        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        File f = new File(Environment.getExternalStorageDirectory(), "CAMERA_IMAGE.jpg");
//        cameraIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
//        cameraImageUri = Uri.fromFile(f);
//
//        startActivityForResult(cameraIntent, 200);
//    }
//    public static class AlertImageDialog {
//
//        public final String type;
//        public final int icon;
//
//        public AlertImageDialog(String type, Integer icon) {
//            this.type = type;
//            this.icon = icon;
//        }
//
//        @Override
//        public String toString() {
//            return type;
//        }
//    }
}
