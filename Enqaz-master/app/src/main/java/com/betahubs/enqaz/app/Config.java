package com.betahubs.enqaz.app;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class Config {
    // Authentications Keys
    public static final String USER_API_TOKEN = "token";
    public static final String USER_ID = "userId";
    public static final String USER_NAME = "name";
    public static final String USER_PHONENUMBER = "phoneNumber";
    public static final String USER_USERNAME = "username";
    public static final String USER_EMAIL = "email";



    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 103;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 141;


    public static final String CAR_MANUFACTURER = "carManufacturer";
    public static final String CAR_MODEL = "carModel";
    public static final String CAR_YEAR = "carYear";
    public static final String CAR_PLATE = "carPlate";

    public static final String CAR_MANUFACTURER_ID = "carManufacturerId";
    public static final String CAR_MODEL_ID = "carModelId";

    public static final String MASKED_PAN = "masked_pan";
    public static final String PAYMENT_TYPE = "payment_type";

    public static final String SHARED_PREF = "ah_firebase";
    public static final String  REQUEST_ID = "REQUEST_ID";
    public static final float MAP_ZOOM_LEVEL = 15;

    //Used in ratingactivty cuz it's triggered from notification so i save name in duringtripactivity here and when trip is finish ratingactivty will start and get name from here
    // all of this cause end trip notification doesn't pass data with it
    public static String driver_name = "";
    public static String request_id = "";

    public static LatLng userLocation;

}