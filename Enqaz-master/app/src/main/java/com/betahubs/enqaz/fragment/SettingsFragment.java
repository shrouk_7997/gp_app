package com.betahubs.enqaz.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.AppRateActivity;
import com.betahubs.enqaz.CarChangeActivity;
import com.betahubs.enqaz.CarManufactoryActivtiy;
import com.betahubs.enqaz.EditProfileActivity;
import com.betahubs.enqaz.IntroActivity;
import com.betahubs.enqaz.R;
import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.Config;
import com.betahubs.enqaz.model.Car;
import com.betahubs.enqaz.util.SelectCarListItemClickListener;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SettingsFragment extends Fragment {
    @BindView(R.id.log_out_button)
    Button logoutBT;
    @BindView(R.id.name_textview)
    TextView nameTextview;
    Unbinder unbinder;
    @BindView(R.id.email_textview)
    TextView emailTextView;
    @BindView(R.id.car_details_textview)
    TextView carDetailsTV;
    @BindView(R.id.car_number_textview)
    TextView carNumberTV;
    @BindView(R.id.phonenumber_textview)
    TextView phonenumberTextview;
    @BindView(R.id.open_new_car_screen_button)
    Button carActionButton;
    @BindView(R.id.settings_layout)
    ConstraintLayout settingsLayout;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private static final int NEW_CAR_REQUEST_CODE = 411;
    private SharedPreferences mSharedPreferences;
    private Call mCall;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        unbinder = ButterKnife.bind(this, view);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        nameTextview.setText(mSharedPreferences.getString(Config.USER_NAME, ""));
        emailTextView.setText(mSharedPreferences.getString(Config.USER_EMAIL, ""));
        phonenumberTextview.setText(mSharedPreferences.getString(Config.USER_PHONENUMBER, ""));

        getCar();
        return view;
    }

    @OnClick(R.id.open_new_car_screen_button)
    void showCarLayout() {
        String buttonAction = carActionButton.getText().toString();
        if(buttonAction.equals(getString(R.string.add_new_car))){
            Intent intent = new Intent(getContext(),CarChangeActivity.class);
            startActivityForResult(intent,NEW_CAR_REQUEST_CODE);
        }
        else if (buttonAction.equals(getString(R.string.delete_car))){

        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode== NEW_CAR_REQUEST_CODE && resultCode==-1){
            updateCarDetailUI(data.getStringExtra(Config.CAR_MANUFACTURER),
                    data.getStringExtra(Config.CAR_MODEL),
                    data.getStringExtra(Config.CAR_PLATE));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void updateCarDetailUI(String carManufacturer, String carModel, String carPlateNumber){
        carDetailsTV.setText(carManufacturer + " / " + carModel);
        carNumberTV.setText(carPlateNumber);
//        carActionButton.setBackgroundColor(R.drawable.red_radius4dp_background);
    }

    @OnClick(R.id.log_out_button)
    void logoutVendor() {
        //Facebook logout
        LoginManager.getInstance().logOut();
        //Google logout
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);
        mGoogleSignInClient.signOut();

        String saveLanguage = mSharedPreferences.getString(getString(R.string.PREF_LOCALE_KEY), "EN");
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear();
        editor.commit();
        mSharedPreferences.edit().putString(saveLanguage, "EN").commit();
        Intent intent = new Intent(getActivity(), IntroActivity.class);
        startActivity(intent);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        getActivity().finish();
    }

    private void getCar() {
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        mCall= enqazService.getCars(mSharedPreferences.getString("token", null));
        mCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("TAG", " Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            JSONObject carsJSON = jsonObject.getJSONObject("cars");
                            updateCarDetailUI(carsJSON.getString("carManufactory"),
                                    carsJSON.getString("carModel"),
                                    carsJSON.getString("carNumber"));
                        } else {
                        }
                    } catch (JSONException | IOException | NullPointerException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
                if(getContext()!=null&&progressBar!=null) {
                    progressBar.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(getContext()!=null&&progressBar!=null) {
                    Toast.makeText(getContext(), R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }
            }
        });
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.edit_profile_button)
    public void onViewClicked() {
        Intent intent = new Intent(getContext(), EditProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.App_rate_button)
    public void openAppRateActivity() {
        Intent intent = new Intent(getContext(), AppRateActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.change_language_button)
    public void onChangeLanguage() {
        AlertDialog.Builder b = new AlertDialog.Builder(getContext());
        b.setTitle("Select Language");
        String[] types = {"English", "اللغة العربية"};
        b.setItems(types, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                switch (which) {
                    case 0:
                        mSharedPreferences.edit().putString(getString(R.string.PREF_LOCALE_KEY), "EN").commit();
                        break;
                    case 1:
                        mSharedPreferences.edit().putString(getString(R.string.PREF_LOCALE_KEY), "AR").commit();
                        break;
                }
                String lang = mSharedPreferences.getString(getString(R.string.PREF_LOCALE_KEY), "");
                Configuration config = getContext().getResources().getConfiguration();
                Locale locale = null;

                if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
                    locale = new Locale(lang);
                    Locale.setDefault(locale);
                    config.locale = locale;
                    getContext().getResources().updateConfiguration(config, getContext().getResources().getDisplayMetrics());
                    getActivity().recreate();
                }
            }
        });
        b.show();
    }

    @Override
    public void onStop() {
        if(mCall!=null) mCall.cancel();
        super.onStop();
    }
}
