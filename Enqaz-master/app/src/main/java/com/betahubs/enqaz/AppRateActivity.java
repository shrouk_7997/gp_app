package com.betahubs.enqaz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betahubs.enqaz.adapter.AppRatesAdapter;
import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.model.AppRates;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppRateActivity extends BaseActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_rate);
        ButterKnife.bind(this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        getServiesList();
    }

    public void getServiesList() {
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.getAppRates().enqueue(new Callback<AppRates>() {
            @Override
            public void onResponse(Call<AppRates> call, Response<AppRates> response) {
                AppRates respond = response.body();
                if (response.isSuccessful()) {
                    boolean success = respond.isSuccess();
                    if (success) {
                        AppRates.Rates rate = respond.getRates();
//                        mServices.clear();
//                        mServices.addAll(activeServices);
                        mAdapter = new AppRatesAdapter(AppRateActivity.this,rate);
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    } else {
                        Toast.makeText(AppRateActivity.this, "" + respond.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(AppRateActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<AppRates> call, Throwable t) {
                Toast.makeText(AppRateActivity.this, getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
