package com.betahubs.enqaz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.app.Config;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarManufactoryActivtiy extends BaseActivity {

    @BindView(R.id.listView) ListView listView;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.textview) TextView mTextView;
    private ArrayAdapter mAdapter;
    private String[] manufactorsOrModelsOrYearsData;
    private String carManufacturer;
    private String carManufacturerId;
    private String carModel;
    private String carModelId;
    private String carYear;
    private String toBeShown;
    private ActionBar mActionBar;
    private String[] manufactorsOrModelsId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_manufactory);
        ButterKnife.bind(this);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        //Using toBeShown instead of creating 3 activities for manfacturer/model/year
        toBeShown = intent.getStringExtra("TO_BE_SHOWN");
        carManufacturer = intent.getStringExtra(Config.CAR_MANUFACTURER);
        carModel = intent.getStringExtra(Config.CAR_MODEL);
        switch (toBeShown){
            case Config.CAR_MANUFACTURER:
                getCarManufactories();
                break;
            case Config.CAR_MODEL:
                getCarModels();
                break;
            case Config.CAR_YEAR:
                getCarYear();
                break;
        }
        //When Manufactor or model or year selected from list
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (toBeShown){
                    case Config.CAR_MANUFACTURER:
                        carManufacturer = manufactorsOrModelsOrYearsData[position];
                        carManufacturerId = manufactorsOrModelsId[position];
                        toBeShown = Config.CAR_MODEL;
                        getCarModels();
                        break;
                    case Config.CAR_MODEL:
                        carModel = manufactorsOrModelsOrYearsData[position];
                        carModelId = manufactorsOrModelsId[position];
                        toBeShown = Config.CAR_YEAR;
                        getCarYear();
                        break;
                    case Config.CAR_YEAR:
                        carYear = manufactorsOrModelsOrYearsData[position];
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(Config.CAR_MANUFACTURER,carManufacturer);
                        returnIntent.putExtra(Config.CAR_MODEL,carModel);
                        returnIntent.putExtra(Config.CAR_YEAR,carYear);
                        setResult(Activity.RESULT_OK,returnIntent);
                        finish();
                        break;
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void getCarManufactories() {
        mActionBar.setTitle(getString(R.string.select_car_manufacturer));
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.getCarManufactories().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            JSONArray manufactories = jsonObject.getJSONArray("message");
                            manufactorsOrModelsOrYearsData = new String[manufactories.length()];
                            manufactorsOrModelsId = new String[manufactories.length()];
                            for (int i = 0; i < manufactories.length(); i++) {
                                JSONObject carmanfs = manufactories.getJSONObject(i);
//                                manufactorsOrModelsId[i] = carmanfs.getString("make_id");
                                manufactorsOrModelsOrYearsData[i] = carmanfs.getString("carManufactory");
                            }
                             mAdapter = new ArrayAdapter<String>(CarManufactoryActivtiy.this, android.R.layout.simple_list_item_1, manufactorsOrModelsOrYearsData);
                            listView.setAdapter(mAdapter);
                        } else {
                            Toast.makeText(CarManufactoryActivtiy.this, "" + jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException |NullPointerException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(CarManufactoryActivtiy.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(CarManufactoryActivtiy.this, R.string.some_thing_went_wrong_please_try_later, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void getCarModels() {
        mActionBar.setTitle(getString(R.string.select_car_model));
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.getCarModels(carManufacturer).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            JSONArray manfs = jsonObject.getJSONArray("message");
                            manufactorsOrModelsOrYearsData = new String[manfs.length()];
                            manufactorsOrModelsId = new String[manfs.length()];
                            for(int i=0;i < manfs.length();i++){
                                JSONObject carmanfs = manfs.getJSONObject(i);
                                manufactorsOrModelsOrYearsData[i] = carmanfs.getString("carModel");
//                                manufactorsOrModelsId[i] = carmanfs.getString("model_id");
                                Log.d("MODEEEL",manufactorsOrModelsOrYearsData[i]);
//                                Log.d("MODEEEL",manufactorsOrModelsId[i]);

                            }
                            mAdapter = new ArrayAdapter<>(CarManufactoryActivtiy.this,android.R.layout.simple_list_item_1, manufactorsOrModelsOrYearsData);
                            listView.setAdapter(mAdapter);
                        } else {
                            Toast.makeText(CarManufactoryActivtiy.this, "" + jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(CarManufactoryActivtiy.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(CarManufactoryActivtiy.this, getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });
    }
    public void getCarYear() {
        mActionBar.setTitle(getString(R.string.select_car_year));
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.getCarYears(carModel).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();

                        JSONObject jsonObject = new JSONObject(res);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            JSONArray manfs = jsonObject.getJSONArray("message");
                            manufactorsOrModelsOrYearsData = new String[manfs.length()];
                            for(int i=0;i < manfs.length();i++){
                                JSONObject carmanfs = manfs.getJSONObject(i);
                                manufactorsOrModelsOrYearsData[i] = carmanfs.getString("year");
                            }
                            mAdapter = new ArrayAdapter<String>(CarManufactoryActivtiy.this,android.R.layout.simple_list_item_1, manufactorsOrModelsOrYearsData);
                            listView.setAdapter(mAdapter);
                        } else {
                            Toast.makeText(CarManufactoryActivtiy.this, "" + jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException |NullPointerException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(CarManufactoryActivtiy.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(CarManufactoryActivtiy.this, getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });
    }

}
