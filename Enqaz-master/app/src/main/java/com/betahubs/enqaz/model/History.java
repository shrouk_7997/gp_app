package com.betahubs.enqaz.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class History {

    @SerializedName("success")
    private Boolean success;
    @SerializedName("message")
    private ArrayList<Data> data;


    public Boolean getSuccess() {
        return success;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public class Data {
        @SerializedName("createdAt")
        private String createdAt;
        @SerializedName("finalCost")
        private String finalCost;

        @SerializedName("_id")
        private String Id;

        @SerializedName("userLocation")
        private Location userLocation;

        @SerializedName("startTripLocation")
        private Location startTripLocation;

        @SerializedName("endTripLocation")
        private Location endTripLocation;

        @SerializedName("status")
        private String status;

        public String getId() {
            return Id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getFinalCost() {
            return finalCost;
        }

        public Location getUserLocation() {
            return userLocation;
        }

        public Location getStartTripLocation() {
            return startTripLocation;
        }

        public Location getEndTripLocation() {
            return endTripLocation;
        }

        public String getStatus() {
            return status;
        }


        public class Location {
            @SerializedName("lat")
            private String lat;
            @SerializedName("long")
            private String lng;

            public Double getLat() {
                return Double.parseDouble(lat);
            }

            public Double getLng() {
                return Double.parseDouble(lng);
            }
        }
    }


}
