package com.betahubs.enqaz.model.RequestNotificationModel;

import com.betahubs.enqaz.model.RequestNotificationModel.Car;
import com.google.gson.annotations.SerializedName;

public class AcceptedRequestNotification {

    @SerializedName("carData")
    private Car carData;
    @SerializedName("driverData")
    private Driver driverData;
    @SerializedName("tripData")
    private Trip tripData;
    @SerializedName("notificationType")
    private String notificationType;

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public Car getCarData() {
        return carData;
    }

    public Driver getDriverData() {
        return driverData;
    }

    public Trip getTripData() {
        return tripData;
    }

    public String getNotificationType() {
        return notificationType;
    }
}
