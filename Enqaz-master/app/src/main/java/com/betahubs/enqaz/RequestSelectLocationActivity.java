package com.betahubs.enqaz;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.os.Process;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.betahubs.enqaz.adapter.ServicesAdapter;
import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;

import com.betahubs.enqaz.app.Config;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.paymob.acceptsdk.IntentConstants;
import com.paymob.acceptsdk.PayActivity;
import com.paymob.acceptsdk.PayActivityIntentKeys;
import com.paymob.acceptsdk.PayResponseKeys;
import com.paymob.acceptsdk.SaveCardResponseKeys;
import com.paymob.acceptsdk.ToastMaker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class RequestSelectLocationActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener{


    @BindView(R.id.pickup_location_textview)
    TextView pickupLocationTextview;
    @BindView(R.id.destination_location_textview)
    TextView destinationLocationTextview;
    @BindView(R.id.confirm_destination_textview)
    Button confirmDestinationButton;
    @BindView(R.id.pickup_icon_on_map_imageview)
    ImageView centerIconOnMap;

    private GoogleMap mMap;
    boolean firstTime = true;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 5000;  /* 5 secs */
    private LatLng currentLatLng;
    private LatLng pickUpLatLng;
    private LatLng destLatLng;

    private LocationCallback mLocationCallback;

    private boolean isPickUpNotDestination = true;
    private boolean isWinch = false;
    private Bundle requestBundleExtras;
    private ArrayList<String> serviceName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_select_location);
        ButterKnife.bind(this);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(R.string.select_pickup_point);

        Intent intent = getIntent();
        requestBundleExtras = intent.getExtras();
        serviceName = intent.getStringArrayListExtra("SERVICE_NAME");

        if (serviceName != null && serviceName.contains("Winch")) {
            isWinch = true;
            destinationLocationTextview.setVisibility(View.VISIBLE);
        }

         startLocationUpdates();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    @SuppressLint("MissingPermission")
    public void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                currentLatLng = new LatLng(locationResult.getLastLocation().getLatitude(),
                        locationResult.getLastLocation().getLongitude());
                updateMapWithNewLocation();
                Log.d("LOCA133", "currentLatLng" + currentLatLng);

            }
        };
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, mLocationCallback,
                Looper.myLooper());
    }

    private void updateMapWithNewLocation() {
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(currentLatLng).title("Me").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_my_location_now)));
//        updateAddress(currentLatLng);
        if (firstTime) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 14));
            firstTime = false;
        } else {
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(Config.userLocation!=null){
            currentLatLng = Config.userLocation;
            updateMapWithNewLocation();
        }
        mMap.setOnCameraIdleListener(this);
    }


    @Override
    public void onCameraIdle() {
        if (isPickUpNotDestination) {
            LatLng p = mMap.getCameraPosition().target;
            if (p != null) {
                pickUpLatLng = p;
                new LocationAsyncTask().execute(p);
            }
        } else {
            LatLng d = mMap.getCameraPosition().target;
            if (d != null) {
                destLatLng = d;

                new LocationAsyncTask().execute(d);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Google places autocompelete search
        if (requestCode == 211) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                pickupLocationTextview.setText(place.getAddress());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 14));
                pickUpLatLng = place.getLatLng();
            }
        } else if (requestCode == 212) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                destinationLocationTextview.setText(place.getAddress());
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 14));
            }
        }
    }

    @OnClick({R.id.confirm_destination_textview, R.id.pickup_location_textview, R.id.destination_location_textview})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.pickup_location_textview:
                Intent intent = null;
                try {
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                            .setCountry("EG")
                            .build();
                    intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(typeFilter).build(this);
                    isPickUpNotDestination = true;
                    getSupportActionBar().setTitle(R.string.select_pickup_location);
                    centerIconOnMap.setImageDrawable(getResources().getDrawable(R.drawable.map_pickup_icon));
                    confirmDestinationButton.setText(R.string.confirm_pickup_location);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                startActivityForResult(intent, 211);
                break;
            case R.id.destination_location_textview:
                Intent intent2 = null;
                try {
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                            .setCountry("EG")
                            .build();
                    intent2 = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).setFilter(typeFilter).build(this);
                    isPickUpNotDestination = false;
                    getSupportActionBar().setTitle(R.string.select_destination);
                    centerIconOnMap.setImageDrawable(getResources().getDrawable(R.drawable.map_dropoff_icon));
                    confirmDestinationButton.setText(getString(R.string.confirm_destination));

                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
                startActivityForResult(intent2, 212);
                break;
            case R.id.confirm_destination_textview:
                if (!isWinch) {
                    pickUpLatLng = mMap.getCameraPosition().target;
                    startRequestMakeRequestAcivity();

                } else {
                    if (isPickUpNotDestination) {
                        pickUpLatLng = mMap.getCameraPosition().target;
                        isPickUpNotDestination = false;
                        getSupportActionBar().setTitle(R.string.select_destination);
                        centerIconOnMap.setImageDrawable(getResources().getDrawable(R.drawable.map_dropoff_icon));
                        confirmDestinationButton.setText(getString(R.string.confirm_destination));
                    } else {
                        destLatLng = mMap.getCameraPosition().target;
                        startRequestMakeRequestAcivity();
                    }
                }
                break;
        }
    }

    private void startRequestMakeRequestAcivity(){
        Intent intent = new Intent(RequestSelectLocationActivity.this,RequestServiceActivity.class);
        requestBundleExtras.putParcelable("PICKUP_LATLNG",pickUpLatLng);
        requestBundleExtras.putParcelable("DEST_LATLNG",destLatLng);
        intent.putExtra("REQUEST_BUNDLE", requestBundleExtras);
        startActivity(intent);
    }

    @OnClick(R.id.map_get_my_location_imageview)
    void onCenterMyLocationIconClicked() {
        if (currentLatLng != null && mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 14));
        }
    }

    @Override
    protected void onStop() {
        getFusedLocationProviderClient(this).removeLocationUpdates(mLocationCallback);
        super.onStop();
    }

//    @Override
//    protected void onDestroy() {
//        getFusedLocationProviderClient(this).removeLocationUpdates(mLocationCallback);
//        finish();
//        super.onDestroy();
//    }

    private class LocationAsyncTask extends AsyncTask<LatLng, Void, String> {
        @Override
        protected String doInBackground(LatLng... params) {
            Geocoder geocoder;
            List<Address> addresses;
            Locale current = getResources().getConfiguration().locale;

            geocoder = new Geocoder(RequestSelectLocationActivity.this, current);

            try {
                if (params[0] != null) {
                    addresses = geocoder.getFromLocation(params[0].latitude, params[0].longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    if (addresses != null && addresses.size() > 0) {
                        if (addresses.get(0) != null) {
                            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            if (address != null) {
                                return address;
                            }
                        }
                    }
                }
            } catch (IOException | NullPointerException e) {
                e.getStackTrace();
                return "";
            }
            return "";
        }

        @Override
        protected void onPostExecute(final String address) {
            super.onPostExecute(address);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isPickUpNotDestination) {
                        pickupLocationTextview.setText(address);
                    } else {
                        destinationLocationTextview.setText(address);
                    }
                }
            });
        }
    }
}

