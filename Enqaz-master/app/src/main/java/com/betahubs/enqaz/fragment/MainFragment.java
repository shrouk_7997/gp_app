package com.betahubs.enqaz.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.betahubs.enqaz.R;
import com.betahubs.enqaz.RequestSelectLocationActivity;
import com.betahubs.enqaz.RequestServiceActivity;
import com.betahubs.enqaz.adapter.RequestServiceAdapter;
import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.Config;
import com.betahubs.enqaz.model.Services;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.betahubs.enqaz.app.Config.MAP_ZOOM_LEVEL;
import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class MainFragment extends Fragment implements OnMapReadyCallback, RequestServiceAdapter.ListItemClickListener {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private Unbinder unbinder;
    private GoogleMap mMap;
    private static final int REQUEST_LOCATION_PERMISSION = 786;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 7000;
    private long FASTEST_INTERVAL = 5000; /* 1 sec */
    private Double mLat;
    private Double mLng;
    boolean firstTime = true;
    private LocationCallback mLocationCallback;
    private ArrayList<Services.Service> mServices;
    private Context mContext;
    private LatLng currentLatLng;
    private String[] selectedServicesIds;
    private boolean[] mSelectedServices;
    private MapView mapView;

    private static final String MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey";
    private Call mCall;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        unbinder = ButterKnife.bind(this, view);

        mContext = getContext();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        mServices = new ArrayList<>();
        mAdapter = new RequestServiceAdapter(getContext(), MainFragment.this, mServices);
        mRecyclerView.setAdapter(mAdapter);
        getServiesList();
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAP_VIEW_BUNDLE_KEY);
        }

        mapView = view.findViewById(R.id.map);
        mapView.onCreate(mapViewBundle);
        mapView.getMapAsync(this);

//        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);

        return view;

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle mapViewBundle = outState.getBundle(MAP_VIEW_BUNDLE_KEY);
        if (mapViewBundle == null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAP_VIEW_BUNDLE_KEY, mapViewBundle);
        }

        mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onStart() {
        mapView.onStart();
        super.onStart();
    }


    @Override
    public void onPause() {
        mapView.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    public static boolean hasPermissions(Context context, String premission) {
        if (context != null && premission != null) {

            if (ActivityCompat.checkSelfPermission(context, premission) != PackageManager.PERMISSION_GRANTED) {
                return false;

            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startLocationUpdates();
        } else {
            Toast.makeText(getContext(), R.string.location_premission_needed, Toast.LENGTH_LONG).show();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @OnClick(R.id.map_get_my_location_imageview)
    void onCenterMyLocationIconClicked() {
        if (currentLatLng != null && mMap != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 14));
        }
    }

    @SuppressLint("MissingPermission")
    public void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(getContext());
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                currentLatLng = new LatLng(locationResult.getLastLocation().getLatitude(),
                        locationResult.getLastLocation().getLongitude());
                updateMapWithNewLocation();
                Log.d("LOCA12", "currentLatLng" + currentLatLng);
            }
        };
        getFusedLocationProviderClient(getContext()).requestLocationUpdates(mLocationRequest, mLocationCallback,
                Looper.myLooper());
    }

    private void updateMapWithNewLocation() {
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(currentLatLng).title("Me").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_my_location_now)));
        if (firstTime) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, MAP_ZOOM_LEVEL));
            firstTime = false;
        } else {
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLatLng));
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if(Config.userLocation!=null)
        {
            currentLatLng = Config.userLocation;
            updateMapWithNewLocation();
        }

        if (!hasPermissions(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
            Log.d("LOCA12", "currentLatLng 2");
        } else {
            startLocationUpdates();
            Log.d("LOCA12", "currentLatLng 3");
        }

    }

    public void getServiesList() {
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        mCall = enqazService.getServicesList();
        mCall.enqueue(new Callback<Services>() {
            @Override
            public void onResponse(Call<Services> call, Response<Services> response) {
                if (response.isSuccessful()) {
                    Services respond = response.body();
                    boolean success = respond.isSuccess();
                    if (success) {
                        ArrayList<Services.Service> allServices = respond.getServices();
                        ArrayList<Services.Service> activeServices = new ArrayList<>();

                        for (int i = 0; i < allServices.size(); i++) {
                            Services.Service service = allServices.get(i);
                            if (service.isActive()) {
                                activeServices.add(service);
                            }
                        }
                        mServices.clear();
                        mServices.addAll(activeServices);
                        mAdapter.notifyDataSetChanged();


                    } else {
                        Toast.makeText(mContext, "" + respond.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(mContext, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Services> call, Throwable t) {
                if(mContext!=null)Toast.makeText(mContext, "Something went wrong! Please try again later.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onStop() {
        if (mLocationCallback != null) {
            getFusedLocationProviderClient(getContext()).removeLocationUpdates(mLocationCallback);
        }
        mCall.cancel();
        super.onStop();
        mapView.onStop();

    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onListItemClick(boolean[] selectedServices) {
        mSelectedServices = selectedServices;
    }

    @OnClick({R.id.request_now_button, R.id.request_later_button})
    public void onRequestButtonsCLicked(View view) {


        switch (view.getId()) {
            case R.id.request_now_button:
                Intent intent = new Intent(mContext, RequestSelectLocationActivity.class);
                if (mSelectedServices != null && mSelectedServices.length > 0) {
                    ArrayList<String> servicesIds = new ArrayList<>();
                    ArrayList<String> servicesNames = new ArrayList<>();
                    ArrayList<Integer> servicesIndexes = new ArrayList<>();
                    for (int i = 0; i < mSelectedServices.length; i++) {
                        if (mSelectedServices[i]) {
                            servicesIds.add(mServices.get(i).get_id());
                            servicesNames.add(mServices.get(i).getName());
                            servicesIndexes.add(i);
                        }
                    }
                    if (servicesIds.size() > 0) {
                        intent.putExtra("SERVICE_ID", servicesIds);
                        intent.putExtra("SERVICE_INDEX", servicesIndexes);
                        intent.putExtra("SERVICE_NAME", servicesNames);
                        intent.putExtra("REQUEST_TYPE", "REQUEST_NOW");
                        mContext.startActivity(intent);
                    }

                } else {
                    Snackbar.make(getActivity().getWindow().getDecorView().getRootView(), "Select your services.", Snackbar.LENGTH_LONG)
                            .show();
                }

                break;
            case R.id.request_later_button:
                Intent intent2 = new Intent(mContext, RequestSelectLocationActivity.class);
                if (mSelectedServices != null && mSelectedServices.length > 0) {
                    ArrayList<String> servicesIds = new ArrayList<>();
                    ArrayList<String> servicesNames = new ArrayList<>();
                    ArrayList<Integer> servicesIndexes = new ArrayList<>();
                    for (int i = 0; i < mSelectedServices.length; i++) {
                        if (mSelectedServices[i]) {
                            servicesIds.add(mServices.get(i).get_id());
                            servicesNames.add(mServices.get(i).getName());
                            servicesIndexes.add(i);
                        } else {
                            Snackbar.make(getActivity().getWindow().getDecorView().getRootView(), "Select your services.", Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }
                    if (servicesIds.size() > 0) {
                        intent2.putExtra("SERVICE_ID", servicesIds);
                        intent2.putExtra("SERVICE_INDEX", servicesIndexes);
                        intent2.putExtra("SERVICE_NAME", servicesNames);
                        intent2.putExtra("REQUEST_TYPE", "REQUEST_LATER");
                        mContext.startActivity(intent2);
                    } else {
                        Snackbar.make(getActivity().getWindow().getDecorView().getRootView(), "Select your services.", Snackbar.LENGTH_LONG)
                                .show();
                    }

                } else {
                    Snackbar.make(getActivity().getWindow().getDecorView().getRootView(), "Select your services.", Snackbar.LENGTH_LONG)
                            .show();
                }


                break;

        }
    }
}
