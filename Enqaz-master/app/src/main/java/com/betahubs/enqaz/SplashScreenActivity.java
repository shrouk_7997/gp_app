package com.betahubs.enqaz;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.Config;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;


import io.fabric.sdk.android.Fabric;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class SplashScreenActivity extends AppCompatActivity {

    private SharedPreferences mSharedPreferences;
    private static final int REQUEST_LOCATION_PERMISSION = 786;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(SplashScreenActivity.this);

        boolean PRIVACY_ACCEPTED = mSharedPreferences.getBoolean("PRIVACY_ACCEPTED",false);
        if (!PRIVACY_ACCEPTED){
            startActivity(new Intent(this,PrivacyPolicyActivtiy.class));
            finish();
            return;
        }


        if (!hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);
            }
        } else {
            getLastKnownLocation();
        }
        checkIfUserExist();

    }

    //bos dyh 3shan al offline mode,ya3ny mynf3sh t5osh offline lowa mkonth 3aml login abl kda
    private void checkIfUserExist() {
        String apiToken = mSharedPreferences.getString("token", null);
        //lowa fy account kan m3mlo loggin shof b2a fy net wla la
        if (apiToken != null && !apiToken.isEmpty()) {
            Log.i("API TOKEN", apiToken);
            checkInternetConnection();
        }
        //tab mfysh 5osh login page 7ata lowa mfysh net
        else {
            navigateToActivity(IntroActivity.class);
        }
    }

    private void checkInternetConnection() {
        //Check if there is internetdd
        new InternetCheck(internet -> {
            //if there is internet try to login
            if (internet) {
                isCustomerInRequest();
            }
            //else go for offline mode
            else {
                Intent intent = new Intent(SplashScreenActivity.this, OfflineModeActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public static boolean hasPermissions(Context context, String premission) {
        if (context != null && premission != null) {

            if (ActivityCompat.checkSelfPermission(context, premission) != PackageManager.PERMISSION_GRANTED) {
                return false;

            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getLastKnownLocation();
        } else {
            Toast.makeText(this, R.string.location_premission_needed, Toast.LENGTH_LONG).show();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @SuppressLint("MissingPermission")
    private void getLastKnownLocation() {
        // Get last known recent location using new Google Play Services SDK (v11+)
        FusedLocationProviderClient locationClient = getFusedLocationProviderClient(this);

        locationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());
                            Config.userLocation = latLng;
                            Log.d("SSSDED", latLng.latitude+"");
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MapDemoActivity", "Error trying to get last GPS location");
                        e.printStackTrace();
                    }
                });
    }

    private void isCustomerInRequest() {
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.isCustomerInRequestSplash(mSharedPreferences.getString("token", null)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("TAGG", "social Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            Intent intent = new Intent();
                            intent.putExtra("isCustomerInRequest", result);
                            intent.setClass(SplashScreenActivity.this, DuringTripActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            navigateToActivity(MainActivity.class);
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        navigateToActivity(MainActivity.class);
                    }
                } else {
                    navigateToActivity(MainActivity.class);
                }
                ;
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                navigateToActivity(MainActivity.class);
            }
        });
    }

//
//    private void openMainActivtiy() {
//        new Handler().postDelayed(new Runnable() {
//
//            /*
//             * Showing splash screen with a timer. This will be useful when you
//             * want to show case your app logo / company
//             */
//
//            @Override
//            public void run() {
//                // This method will be executed once the timer is over
//                // Start your app main activity
//                Intent intent = new Intent();
//                intent.setClass(SplashScreenActivity.this, MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//
//                // close this activity
//                finish();
//            }
//        }, 1000);
//    }

    private void navigateToActivity(Class targetedClass) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent intent = new Intent(SplashScreenActivity.this,targetedClass);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        }, 1000);
    }

    //
//-------------------------------------------------------------------5
    //https://stackoverflow.com/a/27312494
    //checks if there is active internet connection
    public interface Consumer {
        void accept(Boolean internet);
    }

    class InternetCheck extends AsyncTask<Void, Void, Boolean> {
        private Consumer mConsumer;

        public InternetCheck(Consumer consumer) {
            mConsumer = consumer;
            execute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                Socket sock = new Socket();
                sock.connect(new InetSocketAddress("8.8.8.8", 53), 1500);
                sock.close();
                return true;
            } catch (IOException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean internet) {
            mConsumer.accept(internet);
        }
    }
//-------------------------------------------------------------------
}
