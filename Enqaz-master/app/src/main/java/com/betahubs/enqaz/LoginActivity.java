package com.betahubs.enqaz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.app.Config;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;



public class LoginActivity extends BaseActivity {

    @BindView(R.id.username)
    EditText mEmailView;
    @BindView(R.id.password) EditText mPasswordView;
    @BindView(R.id.login_progress) ProgressBar mProgressBar;
    @BindView(R.id.sign_in_button) Button mSignInBT;
    private SharedPreferences mSharedPreferences;

    private boolean isLoggingIn = false;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private String fcmToken;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mSharedPreferences  =  PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        getFCMToken();

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
    }

    private void getFCMToken() {

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( LoginActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmToken = instanceIdResult.getToken();
                Log.e("newToken",fcmToken);
            }
        });
    }

    @OnClick(R.id.sign_in_button) void signIn() {
        attemptLogin();
    }

    @OnClick(R.id.forgot_password) void forgotPassword() {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
        finish();
    }
    private void attemptLogin() {


        if (isLoggingIn) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String emailOrPhone = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        boolean isEmail = true;

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }
        else if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }
//-----------------------------------------------
        // Check for a valid email address.
        if (TextUtils.isEmpty(emailOrPhone)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else {
            if(isEmailValid(emailOrPhone)){isEmail = true;}
            else if (isPhoneVaild(emailOrPhone)){isEmail = false;}
            else{
                mEmailView.setError(getString(R.string.error_invalid_username));
                focusView = mEmailView;
                cancel = true;
            }

        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //  showProgress(true);
            if(isEmail){UserLogin(emailOrPhone, null ,password);}
            else{UserLogin(null, emailOrPhone ,password);}

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this,IntroActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(LoginActivity.this,IntroActivity.class);
                startActivity(intent);
                finish();
        }
        return true;
    }

    private void UserLogin(final String email, final String phoneNumber, final String password) {
        isLoggingIn = true;
        mSignInBT.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        Log.d( TAG , "Login Response: here ");
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.loginVendor(email,phoneNumber,password, fcmToken).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try {
                        String result = response.body().string();
                        Log.d( TAG , "Login Response: "+ result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if(success){
                            String apiToken = jsonObject.getString("token");
                            JSONObject userObject = jsonObject.getJSONObject("user");

                            Log.d(TAG ,"Token: " + jsonObject.getString("token"));
                            if(apiToken!=null&&!apiToken.isEmpty()){
                                SharedPreferences.Editor editor = mSharedPreferences.edit();
                                editor.putString(Config.USER_API_TOKEN, jsonObject.optString("token"));
                                editor.putString(Config.USER_ID, userObject.optString("_id")).apply();
                                editor.putString(Config.USER_NAME, userObject.optString("name"));
                                editor.putString(Config.USER_EMAIL, userObject.optString("email"));
                                editor.putString(Config.USER_PHONENUMBER, userObject.optString("phoneNumber"));
                                editor.putString(Config.USER_USERNAME, userObject.optString("username"));
                                editor.commit();
                                //Hide keyboard after login to not be shown in main activity
                                View currentView =  getCurrentFocus();
                                if (currentView != null) {
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(currentView.getWindowToken(), 0);
                                }

                                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }
                        else {
                            String message =  jsonObject.optString("message");
                            Toast.makeText(LoginActivity.this,"" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                            if(message!=null&&message.equals("please verify your phoneNumber first")){
                                JSONObject userData = jsonObject.getJSONObject("userData");
                                String phoneNumber = userData.getString("phoneNumber");
                                Intent intent = new Intent(LoginActivity.this,ActivationCodeActivity.class);
                                intent.putExtra(Intent.EXTRA_PHONE_NUMBER,phoneNumber);
                                intent.putExtra("COMING_FROM","register");
                                startActivity(intent);
                                finish();
                            }
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }

                else {
                    Toast.makeText(LoginActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    Log.d( TAG , "Login Response: here 2");


                }
                isLoggingIn = false;
                mProgressBar.setVisibility(View.GONE);
                mSignInBT.setEnabled(true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(LoginActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                Log.d( TAG , "Login Response: here 2");

                isLoggingIn = false;
                mProgressBar.setVisibility(View.GONE);
                mSignInBT.setEnabled(true);
            }
        });
    }



    private boolean isEmailValid(String email) {
        return email.contains("@") && email.contains(".");
    }

    private boolean isPasswordValid(String password) {
        // **Password minimum 6 chars.
        // return password.length() > 5;
        return true;
    }


    private boolean isPhoneVaild(String phoneNumber) {
        //String regexStr = "^[+]?[0-9]{10,13}$";
        return PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)&&phoneNumber.length()<15&&phoneNumber.length()>7;

    }
}
