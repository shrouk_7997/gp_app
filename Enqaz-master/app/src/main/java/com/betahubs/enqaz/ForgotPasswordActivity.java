package com.betahubs.enqaz;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForgotPasswordActivity extends BaseActivity {
    @BindView(R.id.emailEditText) EditText mEmailOrPhoneET;
    @BindView(R.id.submit_button)
    Button mSubmitButton;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    private boolean isEmail= true;
    private String emailOrPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.submit_button) void submitForgotPassword() {
        checkPhoneOrEmail();
    }


    public void checkPhoneOrEmail(){
        emailOrPhone = mEmailOrPhoneET.getText().toString();
        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(emailOrPhone)) {
            mEmailOrPhoneET.setError(getString(R.string.error_field_required));
            focusView = mEmailOrPhoneET;
            cancel = true;
        } else {
            if(isEmailValid(emailOrPhone)){isEmail = true;}
            else if (isPhoneVaild(emailOrPhone)){isEmail = false;}
            else{
                mEmailOrPhoneET.setError(getString(R.string.error_invalid_username));
                focusView = mEmailOrPhoneET;
                cancel = true;
            }

        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //  showProgress(true);

            showAlertDialog(emailOrPhone);
        }

    }
    void showAlertDialog(final String emailOrPhone) {
        AlertDialog.Builder builder;

            builder = new AlertDialog.Builder(this);
         builder.setTitle(R.string.number_confirmation)
                .setMessage(getString(R.string.are_you_sure_reset_pw) + emailOrPhone)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(isEmail){forgotPasswordRequest(emailOrPhone, null);}
                        else{forgotPasswordRequest(null, emailOrPhone);}                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();

    }

    private void forgotPasswordRequest(final String email, final String phoneNumber) {
        mSubmitButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.forgotPass(email,phoneNumber).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try {
                        String result = response.body().string();
                        Log.d( "TAG" , " Response: "+ result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if(success){
                            Intent intent = new Intent(ForgotPasswordActivity.this,ActivationCodeActivity.class);
                            if(email!=null){ intent.putExtra(Intent.EXTRA_EMAIL,email);}
                            else {intent.putExtra(Intent.EXTRA_PHONE_NUMBER,phoneNumber);}
                            startActivity(intent);
                        }
                        else {Toast.makeText(ForgotPasswordActivity.this,"" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(ForgotPasswordActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
                mSubmitButton.setEnabled(true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(ForgotPasswordActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
                mSubmitButton.setEnabled(true);
            }
        });
    }


    private boolean isEmailValid(String email) {
        return email.contains("@") && email.contains(".");
    }
    private boolean isPhoneVaild(String phoneNumber) {
        //String regexStr = "^[+]?[0-9]{10,13}$";
        return PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)&&phoneNumber.length()<15&&phoneNumber.length()>7;

    }

}
