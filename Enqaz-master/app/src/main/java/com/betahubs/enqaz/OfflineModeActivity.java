package com.betahubs.enqaz;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.Process;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.betahubs.enqaz.adapter.RequestServiceAdapter;
import com.betahubs.enqaz.adapter.ServicesAdapter;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.model.Services;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class OfflineModeActivity extends BaseActivity implements RequestServiceAdapter.ListItemClickListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private FusedLocationProviderClient mFusedLocationClient;
    private static final int REQUEST_LOCATION_PERMISSION = 786;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 5000;
    private LatLng currentLatLng;
    private ArrayList<Services.Service> mServices;
    private boolean[] mSelectedServices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_mode);
        ButterKnife.bind(this);

        if (!hasPermissions(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION);

        } else {
            startLocationUpdates();
        }

         mServices = new ArrayList<>();
        Services.Service service1 = new Services.Service(getString(R.string.winch));
        Services.Service service2 = new Services.Service(getString(R.string.tires));
        Services.Service service3 = new Services.Service(getString(R.string.battery));
        mServices.add(service1);
        mServices.add(service2);
        mServices.add(service3);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mAdapter = new RequestServiceAdapter(this, this, mServices);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(layoutManager);

        isLocationEnabledHIGNACCURACY(this);
    }

    public  void isLocationEnabledHIGNACCURACY(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            if(locationMode != Settings.Secure.LOCATION_MODE_HIGH_ACCURACY){buildAlertMessageNoGps();}


        }
//        else{
//            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
//            return !TextUtils.isEmpty(locationProviders);
//        }


    }
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("خاصية الموقع(GPS) لديك لا تعمل علي وضع عالي الدقة, برجاء تفعيل هذا الوضع")
                .setCancelable(false)
                .setPositiveButton("تفعيل الوضع", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("قمت بتفعيله", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        isLocationEnabledHIGNACCURACY(OfflineModeActivity.this);
//                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    public static boolean hasPermissions(Context context, String premission) {
        if (context != null && premission != null) {

            if (ActivityCompat.checkSelfPermission(context, premission) != PackageManager.PERMISSION_GRANTED) {
                return false;

            }
        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startLocationUpdates();
        } else {
            //kill the app
            Toast.makeText(this, getString(R.string.location_premission_needed), Toast.LENGTH_LONG).show();
            Process.killProcess(Process.myPid());
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @OnClick(R.id.request_via_sms_button)
    public void onViewClicked() {
        if(currentLatLng!=null) {
            StringBuffer s = new StringBuffer();

            if (mSelectedServices!=null&&mSelectedServices.length > 0) {
                for (int i = 0; i < mSelectedServices.length; i++) {
                    if (mSelectedServices[i]) {
                        s.append(mServices.get(i).getName()+", ");

                    }
                }
                if(!s.toString().isEmpty()) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + "01000907947"));
                    String message = String.format("Offline request: {Services: %s ,Lat: %s ,Lng: %s}", s, currentLatLng.latitude, currentLatLng.longitude);
                    intent.putExtra("sms_body", message);
                    startActivity(intent);
                }
                else {
                    Snackbar.make(getWindow().getDecorView().getRootView(), R.string.select_ur_service, Snackbar.LENGTH_LONG)
                            .show();
                }
            }
        }
        else {
            Toast.makeText(this, R.string.turn_on_gps, Toast.LENGTH_LONG).show();
        }

    }

    @SuppressLint("MissingPermission")
    public void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                currentLatLng = new LatLng(locationResult.getLastLocation().getLatitude(),
                        locationResult.getLastLocation().getLongitude());


                Log.d("LOCA1", "currentLatLng" + currentLatLng);

            }
        };
        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, mLocationCallback,
                Looper.myLooper());
    }
    @Override
    public void onListItemClick(boolean[] selectedServices) {
        mSelectedServices = selectedServices;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getFusedLocationProviderClient(this).removeLocationUpdates(mLocationCallback);
    }


    //        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
//        Log.d("PLAYGROUND", "Permission is not granted, requesting");
//        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 123);
//        button.setEnabled(false);
//    } else {
//        Log.d("PLAYGROUND", "Permission is granted");
//    }
//}
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        if (requestCode == 123) {
//            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                Log.d("PLAYGROUND", "Permission has been granted");
//                textView.setText("You can send SMS!");
//                button.setEnabled(true);
//            } else {
//                Log.d("PLAYGROUND", "Permission has been denied or request cancelled");
//                textView.setText("You can not send SMS!");
//                button.setEnabled(false);
//            }
//        }
//    }

}
