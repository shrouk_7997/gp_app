package com.betahubs.enqaz;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.app.Config;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class IntroActivity extends BaseActivity {

    @BindView(R.id.register_button)
    Button registerButton;
    @BindView(R.id.login_textview)
    TextView loginTextview;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.btn_fb_login)
    Button btnFbLogin;
    private CallbackManager callbackManager;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 372;
    private String fcmToken;
    private SharedPreferences mSharedPreferences;
    private String accessToken;
    private String gAccountToken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        ButterKnife.bind(this);
        getFCMToken();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestScopes(new Scope(Scopes.PLUS_ME))
                .requestIdToken(getResources().getString(R.string.server_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        accessToken = loginResult.getAccessToken().getToken();
                        Log.d("FACEBOOK token", loginResult.getAccessToken().getToken());
                        loginResult.getAccessToken().getUserId();
                        getFacebookData();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.d("FACEBOOK token", "canceled");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.d("FACEBOOK token", "error: " + exception);
                        if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                            }
                        }
                    }
                });
    }

    private void getFacebookData() {
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        Log.d("GRAPHRESPONSE", object.toString()+"");
                        String email = object.optString("email");
                        String id = object.optString("id");
                        isUserExist(id,email);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void getFCMToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( IntroActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmToken = instanceIdResult.getToken();
                Log.e("newToken",fcmToken);
            }
        });
    }

    private void loginSocial() {
        String loginWith = null;
        if(accessToken!=null){loginWith = "facebook";
        }
        else if (gAccountToken!=null){loginWith="google";}
        btnFbLogin.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.socialLogin(loginWith, accessToken,gAccountToken, fcmToken, null, null,null).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        JSONObject jsonObject = new JSONObject(result);
                        Log.d("TAGGINTRO", "social Response: " + response.body().string());

                        boolean success = jsonObject.getBoolean("success");
                        if (success) {

                            String apiToken = jsonObject.getString("token");
                            JSONObject userObject = jsonObject.getJSONObject("user");

                            if (apiToken != null&&!apiToken.isEmpty()) {
                                SharedPreferences.Editor editor = mSharedPreferences.edit();
                                editor.putString(Config.USER_API_TOKEN, jsonObject.optString("token"));
                                editor.putString(Config.USER_ID, userObject.optString("_id")).apply();
                                editor.putString(Config.USER_NAME, userObject.optString("name"));
                                editor.putString(Config.USER_EMAIL, userObject.optString("email"));
                                editor.putString(Config.USER_PHONENUMBER, userObject.optString("phoneNumber"));
                                editor.putString(Config.USER_USERNAME, userObject.optString("username"));
                                editor.commit();
                                Intent intent = new Intent(IntroActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        } else {

                            String message = jsonObject.optString("message");
                            Log.d("TAGG", "social Response: " +message);

                            Toast.makeText(IntroActivity.this, "" + message, Toast.LENGTH_LONG).show();
                            if(message!=null&&message.equals("please verify your phoneNumber first")){
                                JSONObject userData = jsonObject.getJSONObject("userData");
                                String phoneNumber = userData.getString("phoneNumber");
                                Intent intent = new Intent(IntroActivity.this, ActivationCodeActivity.class);
                                intent.putExtra("COMING_FROM","register");
                                intent.putExtra(Intent.EXTRA_PHONE_NUMBER,phoneNumber);
                                startActivity(intent);
                                finish();
                            }
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                btnFbLogin.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Toast.makeText(IntroActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                btnFbLogin.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void isUserExist(String fbAccountId,String email) {
        btnFbLogin.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.isUserExist(fbAccountId,email).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("IntroAct", "isUserExist Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");

                        if (success) {
                            loginSocial();
                        } else {
                            if(fbAccountId!=null) {
                                Intent intent = new Intent(IntroActivity.this,RegisterFacebookLogin.class);
                                intent.putExtra("accessToken",accessToken);
                                intent.putExtra("fcmToken",fcmToken);
                                intent.putExtra("email",email);
                                startActivity(intent);
                            }
                            else {
                                Intent intent = new Intent(IntroActivity.this,RegisterFacebookLogin.class);
                                intent.putExtra("gAccountToken",gAccountToken);
                                intent.putExtra("fcmToken",fcmToken);
                                intent.putExtra("email",email);
                                startActivity(intent);
                            }

                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                btnFbLogin.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(IntroActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                btnFbLogin.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            if (resultCode == Activity.RESULT_OK) {

                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);

                GoogleSignInAccount account = null;
                try {
                    account = task.getResult(ApiException.class);
                } catch (ApiException e) {
                    Log.w("TAG", "signInResult:failed code=" + e.getStatusCode());
                }
                task.addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        Log.e(" GOOGLELOGIN ERR", "CANCELED");

                    }
                });
                task.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        try {
                            Log.e(" GOOGLELOGIN ERR", e.toString());
                            Log.e(" GOOGLELOGIN ERR", e.getCause() + "");
                            Log.e(" GOOGLELOGIN ERR", e.getMessage());
                            Log.e(" GOOGLELOGIN ERR", e.getStackTrace() + "");
                        } catch (NullPointerException a) {
                            e.getStackTrace();
                        }

                    }
                });
                task.addOnSuccessListener(new OnSuccessListener<GoogleSignInAccount>() {
                    @Override
                    public void onSuccess(GoogleSignInAccount googleSignInAccount) {
                        Log.e(" GOOGLELOGIN ERR", googleSignInAccount.getEmail());
                        gAccountToken = googleSignInAccount.getIdToken();
                        String email = task.getResult().getEmail();
                        isUserExist(null, email);
                    }
                });
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.register_button)
    public void onRegisterButtonClicked() {
        Intent intent = new Intent(IntroActivity.this, TermsActivty.class);
        startActivity(intent);
    }

    @OnClick(R.id.login_textview)
    public void onLoginTextviewClicked() {
        Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_fb_login)
    public void onViewClicked() {

        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    @OnClick(R.id.btn_google_login)
    public void onViewClicked2() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);

    }
}
