package com.betahubs.enqaz.model;

public class AppRates {
    private boolean success;
    private String message;

    public String getMessage() {
        return message;
    }

    private Rates rates;

    public boolean isSuccess() {
        return success;
    }

    public Rates getRates() {
        return rates;
    }

    public class Rates {
        private Integer waitingTimePrice;
        private String kilometerPrice;
        private Service[] services;

        public Integer getWaitingTimePrice() {
            return waitingTimePrice;
        }

        public String getKilometerPrice() {
            return kilometerPrice;
        }

        public Service[] getServices() {
            return services;
        }

        public class Service{
            private String name;
            private Integer baseFare;

            public String getName() {
                return name;
            }

            public Integer getBaseFare() {
                return baseFare;
            }
        }
    }

}
