package com.betahubs.enqaz.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TripDetails {
    @SerializedName("success")
    private Boolean success;
    @SerializedName("trip_details")
    private TripDetails.Data data;
    @SerializedName("startTripLocation")
    private History.Data.Location startTripLocation;

    @SerializedName("endTripLocation")
    private History.Data.Location endTripLocation;
    @SerializedName("userRate")
    private Double userRate;

    public Double getUserRate() {
        return userRate;
    }

    public Boolean getSuccess() {
        return success;
    }

    public TripDetails.Data getData() {
        return data;
    }

    public History.Data.Location getStartTripLocation() {
        return startTripLocation;
    }

    public History.Data.Location getEndTripLocation() {
        return endTripLocation;
    }

    public class Data {
        @SerializedName("_id")
        private String Id;
        @SerializedName("driverName")
        private String driverName;
        @SerializedName("paymentType")
        private String paymentType;
        @SerializedName("carModel")
        private String carModel;
        @SerializedName("carManufactory")
        private String carManufactory;
        @SerializedName("date")
        private String createdAt;



        public String getDriverName() {
            return driverName;
        }

        public String getDist() {
            return dist;
        }

        public String getKmPrice() {
            return kmPrice;
        }

        public ArrayList<String> getServiceName() {
            return serviceName;
        }

        public String getServiceBaseFare() {
            return serviceBaseFare;
        }

        public String getWaitingTime() {
            return waitingTime;
        }

        public String getWaitingTimePrice() {
            return waitingTimePrice;
        }

        @SerializedName("finalCost")
        private String finalCost;
        @SerializedName("dist")
        private String dist;
        @SerializedName("kmPrice")
        private String kmPrice;
        @SerializedName("serviceName")
        private ArrayList<String> serviceName;
        @SerializedName("serviceBaseFare")
        private String serviceBaseFare;
        @SerializedName("waitingTime")
        private String waitingTime;
        @SerializedName("waitingTimePrice")
        private String waitingTimePrice;
        @SerializedName("serviceId")
        private String[] serviceId;

        public String getPaymentType() {
            return paymentType;
        }

        public String getCarModel() {
            return carModel;
        }

        public String getCarManufactory() {
            return carManufactory;
        }

        public String[] getServiceId() {
            return serviceId;
        }


        public String getId() {
            return Id;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public String getFinalCost() {
            return finalCost;
        }


        }



public class Location {
    @SerializedName("lat")
    private String lat;
    @SerializedName("long")
    private String lng;

    public Double getLat() {
        return Double.parseDouble(lat);
    }

    public Double getLng() {
        return Double.parseDouble(lng);
    }
}

}
