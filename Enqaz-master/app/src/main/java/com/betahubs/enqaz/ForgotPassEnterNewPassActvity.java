package com.betahubs.enqaz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ForgotPassEnterNewPassActvity extends BaseActivity {

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.submit_button)
    Button mSubmitButton;
    @BindView(R.id.new_password_editext)
    EditText mNewPasswordET;
    @BindView(R.id.repeat_password_edittext)
    EditText mRepeatPasswordET;
    private String apiToken;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass_enter_new_pass_actvity);
        ButterKnife.bind(this);
        apiToken = getIntent().getStringExtra("token");
    }

    void vaildatePassword(){
        String  password = mNewPasswordET.getText().toString();
        String  repeatPassword = mRepeatPasswordET.getText().toString();
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(password)) {
            mNewPasswordET.setError(getString(R.string.error_field_required));
            focusView = mNewPasswordET;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            mNewPasswordET.setError(getString(R.string.error_invalid_password));
            focusView = mNewPasswordET;
            cancel = true;
        }
        //--------------------

        if (TextUtils.isEmpty(repeatPassword)) {
            mRepeatPasswordET.setError(getString(R.string.error_field_required));
            focusView = mRepeatPasswordET;
            cancel = true;
        } else if (!isRepeatedPasswordVaild(password,repeatPassword)) {
            mRepeatPasswordET.setError(getString(R.string.error_invalid_repeat_password));
            focusView = mRepeatPasswordET;
            cancel = true;
        }
        //--------------------
        // Check for a valid email address.


        if (cancel) {
            // There was an error; don't attempt signUp and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //showProgress(true);
            newPasswordRequest(password);
        }
    }

    @OnClick(R.id.submit_button) void setNewPassword(){
        vaildatePassword();
    }

    public void newPasswordRequest(String password){
        mSubmitButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.updatePassword(apiToken,password).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if(response.isSuccessful()){
                    try {
                        String result = response.body().string();
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if(success){
                            finish();
                            Intent intent = new Intent(ForgotPassEnterNewPassActvity.this,LoginActivity.class);
                            startActivity(intent);
                            Toast.makeText(ForgotPassEnterNewPassActvity.this, R.string.password_was_updated, Toast.LENGTH_SHORT).show();

                        }
                        else {
                            Toast.makeText(ForgotPassEnterNewPassActvity.this,"" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(ForgotPassEnterNewPassActvity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
                mSubmitButton.setEnabled(true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(ForgotPassEnterNewPassActvity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
                mSubmitButton.setEnabled(true);
            }
        });
    }
    private boolean isPasswordValid(String password) {
        return true;
        //   return password.length() > 5;
    }
    private boolean isRepeatedPasswordVaild(String password, String repeatedPassword) {
        return repeatedPassword.equals(password);
    }
}
