package com.betahubs.enqaz.app;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.betahubs.enqaz.R;
import com.google.android.gms.analytics.Tracker;

import java.util.Locale;

public abstract class BaseActivity extends AppCompatActivity {
    //Not called in splashscreen activity

    private Locale locale = null;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        if (locale != null) {
//            newConfig.locale = locale;
//            Locale.setDefault(locale);
//            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
//        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Snackbar.make(findViewById(android.R.id.content),"test", Snackbar.LENGTH_SHORT).show();
//        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
//
//        Configuration config = getBaseContext().getResources().getConfiguration();
//
//        String lang = settings.getString(getString(R.string.PREF_LOCALE_KEY), "");
//        Log.e("LANGPLZ",lang);
//        if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
//            locale = new Locale(lang);
//            Locale.setDefault(locale);
//            config.locale = locale;
//            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
//        }
//        else{
//            locale = new Locale("EN");
//            Locale.setDefault(locale);
//            config.locale = locale;
//            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
//
//        }
    }
}
