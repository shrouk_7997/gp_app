package com.betahubs.enqaz.fragment;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.R;
import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class FreeRidesFragment extends Fragment {


    Unbinder unbinder;
    @BindView(R.id.promocode_textview)
    TextView promocodeTextview;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private Call mCall;
    private SharedPreferences mSharedPreferences;
    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_free_rides, container, false);
        unbinder = ButterKnife.bind(this, view);
        mContext = getContext();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        getHistory();
        return view;
    }

    public void getHistory() {
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        mCall = enqazService.getReferralCode(mSharedPreferences.getString("token", null));
        mCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        JSONObject resultJSON = new JSONObject(result);
                        boolean success = resultJSON.getBoolean("success");
                        if (success) {
                            JSONObject data = resultJSON.getJSONObject("data");
                            String refCode = data.getString("refCode");
                            promocodeTextview.setText(refCode);

                        } else {
                            Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException | IOException e) {
                        Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                    if(progressBar!=null)
                    progressBar.setVisibility(View.GONE);
            }
        });
    }

    @OnClick({R.id.share_invitation_button, R.id.copy_invitation_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.share_invitation_button:
                String shareBody = "This is a promocode for your first usage for EnQaz App: " + promocodeTextview.getText().toString();
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(sharingIntent);
                break;
            case R.id.copy_invitation_button:
                ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("promocode", promocodeTextview.getText().toString());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(mContext, "Code copied", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCall!=null) mCall.cancel();
        unbinder.unbind();
    }

}
