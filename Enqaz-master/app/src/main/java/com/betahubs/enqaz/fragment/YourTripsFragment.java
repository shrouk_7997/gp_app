package com.betahubs.enqaz.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.betahubs.enqaz.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class YourTripsFragment extends Fragment {


    Unbinder unbind;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    private HistoryFragment mHistoryFragment;
    private ScheduleHistoryFragment mScheduleHistoryFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_your_trips, container, false);
        unbind = ButterKnife.bind(this, view);
        tabLayout.setupWithViewPager(viewPager);

        FragmentPagerAdapter mFragmentPagerAdapter = new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public int getCount() {

                return 2;
            }

            @Override
            public Fragment getItem(int position) {

                if(position == 0){
                    mHistoryFragment = new HistoryFragment();
                }
                else {
                    mScheduleHistoryFragment = new ScheduleHistoryFragment();
                }

                return position == 0 ? mHistoryFragment : mScheduleHistoryFragment;

            }

            @Override
            public CharSequence getPageTitle(int position) {
                return position == 0 ? getString(R.string.history_tabview_title) : getString(R.string.schedule_tap_title);
            }

        };
        viewPager.setAdapter(mFragmentPagerAdapter);
        return view;
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbind.unbind();

    }
}
