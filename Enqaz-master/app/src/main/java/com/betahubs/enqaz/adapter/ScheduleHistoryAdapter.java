package com.betahubs.enqaz.adapter;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.betahubs.enqaz.HistoryTripDetailsActivity;
import com.betahubs.enqaz.R;
import com.betahubs.enqaz.RequestServiceActivity;
import com.betahubs.enqaz.ScheduleHistoryTripDetailsActivity;
import com.betahubs.enqaz.model.History;
import com.betahubs.enqaz.model.ScheduleHistory;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class ScheduleHistoryAdapter extends RecyclerView.Adapter<ScheduleHistoryAdapter.RecyclerViewHolder> {
    //        private ArrayList<Histories.History> mHistories;
    private Context mContext;
    private ArrayList<ScheduleHistory> mHistories;
    private ScheduleHistoryAdapter.ListItemClickListener mOnClickListener;


    public ScheduleHistoryAdapter(Context context, ScheduleHistoryAdapter.ListItemClickListener listener, ArrayList<ScheduleHistory> scheduleHistories) {
        mContext = context;
        mOnClickListener = listener;
        mHistories = scheduleHistories;
    }

    @Override
    public ScheduleHistoryAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_history_list_item, parent, false);
        ScheduleHistoryAdapter.RecyclerViewHolder viewHolder = new ScheduleHistoryAdapter.RecyclerViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ScheduleHistoryAdapter.RecyclerViewHolder holder, int position) {
        ScheduleHistory scheduleHistory = mHistories.get(position);
        //Next method used to prevent mints from being just one number like this 9:3 instead it will be 9:03
        String selectedMintString;
        if (scheduleHistory.getMin() < 10) selectedMintString = "0" + scheduleHistory.getMin();
        else selectedMintString = scheduleHistory.getMin() + "";
        holder.dateTV.setText(scheduleHistory.getDay() + "/" + scheduleHistory.getMonth() + "/" + scheduleHistory.getYear() +
                " at " + scheduleHistory.getHour() + ":" + selectedMintString);


        if (scheduleHistory.getCustomerLat() != null) {
            String from = getAddress(scheduleHistory.getCustomerLat(), scheduleHistory.getCustomerLng());
            holder.pickFromTV.setText(from);
        }
        if (scheduleHistory.getDestLat() != null) {
            String to = getAddress(scheduleHistory.getCustomerLat(), scheduleHistory.getCustomerLng());
            holder.dropOffTV.setText(to);
        }
        holder.finalCost.setText(scheduleHistory.getEstimatedFare());
    }

    public String getAddress(Double lat, Double lng) {

        Geocoder geocoder;
        List<Address> addresses;
        Locale current = mContext.getResources().getConfiguration().locale;

        geocoder = new Geocoder(mContext, current);

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                return address;
            }
        } catch (IOException ignored) {
            return "";
        }
        return "";
    }

    @Override
    public int getItemCount() {
        return mHistories.size();
    }

    public interface ListItemClickListener {
        void onListItemClick(int id);
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView dateTV;
        private TextView pickFromTV;
        private TextView dropOffTV;
        private TextView finalCost;
        private Button cancelButton;
        private Button editTimeButton;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            dateTV = itemView.findViewById(R.id.date_textview);
            pickFromTV = itemView.findViewById(R.id.address_from_textview);
            dropOffTV = itemView.findViewById(R.id.address_to_textview);
            finalCost = itemView.findViewById(R.id.final_cost_textview);
            cancelButton = itemView.findViewById(R.id.cancel_schedule_button);
            editTimeButton = itemView.findViewById(R.id.edit_time_button);
            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("schedule").child(mHistories.get(getAdapterPosition()).getFirebaseKey());
                    ref.removeValue(new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                            mOnClickListener.onListItemClick(getAdapterPosition());
                            Toast.makeText(mContext, R.string.cancel_scedule_request_toast, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            });
            editTimeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowScheduleScreen(getAdapterPosition());
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, ScheduleHistoryTripDetailsActivity.class);
                    intent.putExtra("scheduleHistory", mHistories.get(getAdapterPosition()));
                    mContext.startActivity(intent);
                }
            });
        }


        private void ShowScheduleScreen(int position) {
            DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                    long scheduleYear = year;
                    long scheduleDay = day;
                    long scheduleMonth = month;

                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    TimePickerDialog mTimePicker;
                    mTimePicker = new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                            long scheduleHour = selectedHour;
                            long scheduleMint = selectedMinute;
                            //Next method used to prevent mints from being just one number like this 9:3 instead it will be 9:03

//                        requestButtonBottomText.setText(scheduleDay + "-" + (scheduleMonth+1) + "-" + scheduleYear + " at " + selectedHour+":"+selectedMintString );
                            if (scheduleDay > 0 && (scheduleMint > 0 || scheduleHour > 0)) {
                                final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("schedule").child(mHistories.get(position).getFirebaseKey());
                                Map<String, Object> scheduleRequest = new HashMap<>();

                                scheduleRequest.put("year", scheduleYear);
                                scheduleRequest.put("month", scheduleMonth);
                                scheduleRequest.put("day", scheduleDay);
                                scheduleRequest.put("hour", scheduleHour);
                                scheduleRequest.put("min", scheduleMint);
                                ref.updateChildren(scheduleRequest, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                        Toast.makeText(mContext, R.string.your_schedule_successfuly, Toast.LENGTH_LONG).show();
                                        ScheduleHistory scheduleHistory = mHistories.get(position);
                                        mOnClickListener.onListItemClick(getAdapterPosition());

//                                    String selectedMintString;
//
//                                    if (selectedMinute < 10) selectedMintString = "0" + selectedHour;
//                                    else selectedMintString = selectedHour + "";
//                                    dateTV.setText(scheduleHistory.getDay()+"/"+scheduleHistory.getMonth()+"/"+scheduleHistory.getYear()+
//                                            " at "+scheduleHistory.getHour()+":"+scheduleHistory.getMin());
                                    }
                                });

                            }
                        }
                    }, hour, minute, true);//Yes 24 hour time
                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();

                }
            };
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            Calendar cal2 = Calendar.getInstance();
            int year2 = cal2.get(Calendar.YEAR);
            int month2 = cal2.get(Calendar.MONTH);
            int day2 = cal2.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog dialog = new DatePickerDialog(
                    mContext,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    mDateSetListener,
                    year, month, day);

            cal2.set(year2, month2, day2);
            dialog.getDatePicker().setMinDate(cal2.getTimeInMillis() - 1000);
            int maxDate = month + 1;
            if (month == 11) {
                maxDate = 0;
            }
            long now = System.currentTimeMillis() - 1000;
            cal.set(year, maxDate, day);
            dialog.getDatePicker().setMaxDate(cal.getTimeInMillis());

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


            dialog.show();

        }
    }

}

