package com.betahubs.enqaz.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betahubs.enqaz.R;
import com.betahubs.enqaz.adapter.HistoryAdapter;
import com.betahubs.enqaz.adapter.ScheduleHistoryAdapter;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.model.History;
import com.betahubs.enqaz.model.ScheduleHistory;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ScheduleHistoryFragment extends Fragment implements ScheduleHistoryAdapter.ListItemClickListener {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.no_history_layout)
    ConstraintLayout noHistoryLayout;
    private RecyclerView.Adapter mAdapter;
    private SharedPreferences mSharedPreferences;
    private Context mContext;
    Unbinder unbind;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        unbind = ButterKnife.bind(this, view);
        mContext = getContext();
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        getHistory();

        return view;
    }



    public void getHistory() {
        progressBar.setVisibility(View.VISIBLE);
        ArrayList<ScheduleHistory> scheduleHistories = new ArrayList<>();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("schedule");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                    String id = postSnapshot.child("userId").getValue().toString();
                    Log.d("FBSNAPSHOT1", postSnapshot.child("userId").getValue().toString());

                    if(id.equals(mSharedPreferences.getString("userId",""))) {
                        ScheduleHistory scheduleHistory = new ScheduleHistory();
                        scheduleHistory.setFirebaseKey(postSnapshot.getKey());
                        scheduleHistory.setId(postSnapshot.child("userId").getValue().toString());
                        scheduleHistory.setToken(postSnapshot.child("token").getValue().toString());
                        scheduleHistory.setCarModel(postSnapshot.child("carModel").getValue().toString());
                        scheduleHistory.setPaymentType(postSnapshot.child("paymentType").getValue().toString());
                        if(postSnapshot.child("destLat").exists()) {
                            scheduleHistory.setDestLat((Double) postSnapshot.child("destLat").getValue());
                            scheduleHistory.setDestLng((Double) postSnapshot.child("destLng").getValue());
                        }
                        scheduleHistory.setServiceName((ArrayList<String>) postSnapshot.child("serviceName").getValue());
                        scheduleHistory.setServiceId((ArrayList<String>) postSnapshot.child("serviceId").getValue());
                        scheduleHistory.setDay((long) postSnapshot.child("day").getValue());
                        scheduleHistory.setCarManufactory(postSnapshot.child("carManufactory").getValue().toString());
                        scheduleHistory.setMin((long) postSnapshot.child("min").getValue());
                        scheduleHistory.setHour((long) postSnapshot.child("hour").getValue());
                        scheduleHistory.setMonth((long) postSnapshot.child("month").getValue());
                        scheduleHistory.setYear((long) postSnapshot.child("year").getValue());
                        scheduleHistory.setCustomerLat((Double)postSnapshot.child("customerLat").getValue());
                        scheduleHistory.setCustomerLng((Double)postSnapshot.child("customerLng").getValue());
                        scheduleHistory.setEstimatedFare(postSnapshot.child("estimatedFare").getValue().toString());

                       scheduleHistories.add(scheduleHistory);
                    }
                }
                showScheduleHistory(scheduleHistories);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);
            }
        });
//                    History result = response.body();
//                    if (result != null && result.getSuccess()) {
//                        ArrayList<History.Data> data = result.getData();
//                        if(data.size()==0){
//                            noHistoryLayout.setVisibility(View.VISIBLE);
//                        }
//                        mAdapter = new HistoryAdapter(mContext, data);
//                        recyclerView.setAdapter(mAdapter);
//                    } else {
//                        Toast.makeText(mContext, "No history", Toast.LENGTH_SHORT).show();
//
//                    }

    }

    private void showScheduleHistory(ArrayList<ScheduleHistory> scheduleHistories) {
           mAdapter = new ScheduleHistoryAdapter(getContext(),this,scheduleHistories);
           recyclerView.setAdapter(mAdapter);
        if(scheduleHistories.size()<=0){
            noHistoryLayout.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onListItemClick(int id) {
        getHistory();
    }
}
