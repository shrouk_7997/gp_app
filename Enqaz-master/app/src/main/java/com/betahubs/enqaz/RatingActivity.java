package com.betahubs.enqaz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RatingActivity extends BaseActivity {
    @BindView(R.id.driver_name_textview)
    TextView driverNameTextview;
    @BindView(R.id.button)
    Button button;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.rate_textview)
    TextView rateTextview;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private SharedPreferences mSharedPreferences;
    private String requestId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        ButterKnife.bind(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(RatingActivity.this);
        String customerName = getIntent().getStringExtra("driver_name");
        requestId = getIntent().getStringExtra("requestId");
        driverNameTextview.setText(customerName);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                switch ((int) rating) {
                    case 1:
                        rateTextview.setText(R.string.very_bad);
                        break;
                    case 2:
                        rateTextview.setText(R.string.bad);
                        break;
                    case 3:
                        rateTextview.setText(R.string.good);
                        break;
                    case 4:
                        rateTextview.setText(R.string.very_good);
                        break;
                    case 5:
                        rateTextview.setText(R.string.excellent);
                        break;
                }
            }
        });
    }

    private void feedback(int rate) {
        ratingBar.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.feedback(mSharedPreferences.getString("token", null), requestId, rate, null).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("TAGG", "Register Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            Intent intent = new Intent(RatingActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(RatingActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                ratingBar.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RatingActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                ratingBar.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.button)
    public void onViewClicked() {
        int rating = (int) ratingBar.getRating();
        if (rating > 0) {
            feedback(rating);
        } else {
            Intent intent = new Intent(RatingActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @OnClick(R.id.report_problem_button)
    public void reportAProblemClicled() {
        Intent intent = new Intent(RatingActivity.this,ReportProblemActivtiy.class);
        intent.putExtra("requestId", requestId);
        startActivity(intent);
    }
}
