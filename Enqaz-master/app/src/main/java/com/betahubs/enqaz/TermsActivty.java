package com.betahubs.enqaz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.betahubs.enqaz.app.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermsActivty extends BaseActivity {

    @BindView(R.id.checkbox)
    CheckBox checkbox;
    @BindView(R.id.confirm_button)
    Button confirmButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_activty);
        ButterKnife.bind(this);
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    confirmButton.setEnabled(true);
                    confirmButton.setAlpha(1);
                } else {
                    confirmButton.setEnabled(false);
                    confirmButton.setAlpha(.7f);
                }
            }
        });
    }

    @OnClick(R.id.confirm_button)
    public void onViewClicked() {
        Intent intent = new Intent(TermsActivty.this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }
}
