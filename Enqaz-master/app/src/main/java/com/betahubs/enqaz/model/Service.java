package com.betahubs.enqaz.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Service implements Parcelable {
    public static final Parcelable.Creator<Service> CREATOR = new Parcelable.Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel source) {
            return new Service(source);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };
    private String _id;
    private String name;
    private String description;
    private Integer baseFare;
    private boolean active;

    public Service(String name) {
        this.name = name;
    }

    protected Service(Parcel in) {
        this._id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.baseFare = (Integer) in.readValue(Integer.class.getClassLoader());
        this.active = in.readByte() != 0;
    }

    public String get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getBaseFare() {
        return baseFare;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this._id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeValue(this.baseFare);
        dest.writeByte(this.active ? (byte) 1 : (byte) 0);
    }
}