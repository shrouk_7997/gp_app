package com.betahubs.enqaz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ReportProblemActivtiy extends BaseActivity {

    @BindView(R.id.problem_edittext)
    EditText problemEdittext;
    @BindView(R.id.submit_problem_button)
    Button submitProblemButton;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private String requestId;
    private SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_problem_activtiy);
        ButterKnife.bind(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        requestId = getIntent().getStringExtra("requestId");
    }

    @OnClick(R.id.submit_problem_button)
    public void onViewClicked() {
        if(!problemEdittext.getText().toString().isEmpty()){
            feedback();
        }
        else {
            Toast.makeText(this, R.string.descripe_your_problem, Toast.LENGTH_SHORT).show();
        }
    }

    private void feedback() {
        submitProblemButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.feedback(mSharedPreferences.getString("token", null), requestId, null, problemEdittext.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("TAGG", "Register Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            Intent intent = new Intent(ReportProblemActivtiy.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(ReportProblemActivtiy.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                submitProblemButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(ReportProblemActivtiy.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                submitProblemButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
