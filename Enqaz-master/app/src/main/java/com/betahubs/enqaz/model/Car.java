package com.betahubs.enqaz.model;

public class Car {
    private String carManufactory;
    private String carModel;
    private String carColor;
    private String carNumber;

    public Car(String carManufactory, String carModel, String carColor, String carNumber) {
        this.carManufactory = carManufactory;
        this.carModel = carModel;
        this.carColor = carColor;
        this.carNumber = carNumber;
    }

    public class CarManfactory{
        private String carManufactoryId;
        private String carManufactory;

        public CarManfactory(String carManufactoryId, String carManufactory) {
            this.carManufactoryId = carManufactoryId;
            this.carManufactory = carManufactory;
        }

        public String getCarManufactoryId() {
            return carManufactoryId;
        }

        public String getCarManufactory() {
            return carManufactory;
        }
    }

    public class CarModel{
        private String carModelId;
        private String carModel;

        public CarModel(String carModelId, String carModel) {
            this.carModelId = carModelId;
            this.carModel = carModel;
        }

        public String getCarModelId() {
            return carModelId;
        }

        public String getCarModel() {
            return carModel;
        }
    }
}
