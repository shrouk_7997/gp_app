package com.betahubs.enqaz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterFacebookLogin extends BaseActivity {

    @BindView(R.id.phone_number_edittext)
    EditText phoneNumberEdittext;
    @BindView(R.id.change_password_button)
    Button confirmButton;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.password_edittext)
    EditText passwordEdittext;
    @BindView(R.id.email_edittext2)
    EditText emailEdittext2;
    private SharedPreferences mSharedPreferences;
    private String accessToken;
    private String fcmToken;
    private String email;
    private String gAccountToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_register);
        ButterKnife.bind(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        accessToken = getIntent().getStringExtra("accessToken");
        gAccountToken = getIntent().getStringExtra("gAccountToken");

        fcmToken = getIntent().getStringExtra("fcmToken");
        email = getIntent().getStringExtra("email");
        if(email!=null){
        emailEdittext2.setText(email);}

    }

    private void validatePhone() {

        phoneNumberEdittext.setError(null);
        passwordEdittext.setError(null);
        emailEdittext2.setError(null);

        // Store values at the time of the login attempt.
        String phone = phoneNumberEdittext.getText().toString();
        String password = passwordEdittext.getText().toString();
        String email = emailEdittext2.getText().toString();

        boolean cancel = false;
        View focusView = null;
        if (isPhoneVaild(phone)) {
        } else {
            phoneNumberEdittext.setError(getString(R.string.error_invalid_phone));
            focusView = phoneNumberEdittext;
            cancel = true;
        }

        if (isPasswordValid(password)) {
        } else {
            passwordEdittext.setError(getString(R.string.error_invalid_password));
            focusView = passwordEdittext;
            cancel = true;
        }
        if(isEmailValid(email)){
        }
        else {
            emailEdittext2.setError(getString(R.string.please_enter_valid_mail));
            focusView = emailEdittext2;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            loginSocial(phone, password,email);
        }
    }

    private void loginSocial(String phone, String password, String email) {
        confirmButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        String loginWith= null;
        if(accessToken!=null){loginWith="facebook";}
        else if (gAccountToken!=null){loginWith="google";}
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.socialLogin(loginWith, accessToken,gAccountToken, fcmToken, phone, password, email).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("TAGGGOGGLE", "Register Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {

                            Intent intent = new Intent(RegisterFacebookLogin.this, ActivationCodeActivity.class);
                            intent.putExtra("COMING_FROM","register");
                            intent.putExtra(Intent.EXTRA_PHONE_NUMBER,phone);
                            startActivity(intent);
                            finish();
                        } else {
                            String message = jsonObject.optString("message");
                            Log.d("TAGG", "social Response: " +message);

                            Toast.makeText(RegisterFacebookLogin.this, "" + message, Toast.LENGTH_LONG).show();
                            if(message!=null&&message.equals("please verify your phoneNumber first")){
                                JSONObject userData = jsonObject.getJSONObject("userData");
                                String phoneNumber = userData.getString("phoneNumber");
                                Intent intent = new Intent(RegisterFacebookLogin.this, ActivationCodeActivity.class);
                                intent.putExtra("COMING_FROM","register");
                                intent.putExtra(Intent.EXTRA_PHONE_NUMBER,phoneNumber);
                                startActivity(intent);
                                finish();
                            }
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                confirmButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RegisterFacebookLogin.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                confirmButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    private boolean isPhoneVaild(String phoneNumber) {
        //String regexStr = "^[+]?[0-9]{10,13}$";
        return PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber) && phoneNumber.length() < 15 && phoneNumber.length() > 7;
    }
    private boolean isEmailValid(String email) {
        //String regexStr = "^[+]?[0-9]{10,13}$";
        return email.contains(".")&&email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //String regexStr = "^[+]?[0-9]{10,13}$";
        return password.length() > 5;
    }

    @OnClick(R.id.change_password_button)
    public void onViewClicked() {
        validatePhone();
    }
}
