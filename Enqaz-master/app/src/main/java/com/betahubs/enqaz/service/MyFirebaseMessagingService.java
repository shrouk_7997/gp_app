package com.betahubs.enqaz.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.betahubs.enqaz.DuringTripActivity;
import com.betahubs.enqaz.MainActivity;
import com.betahubs.enqaz.R;
import com.betahubs.enqaz.RatingActivity;
import com.betahubs.enqaz.app.Config;
import com.betahubs.enqaz.model.RequestNotificationModel.AcceptedRequestNotification;
import com.betahubs.enqaz.util.NotificationUtils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;
    private static final String NEW_TRIP_REQUEST_NOTIFICATION_CHANNEL_ID = "NEW_TRIP_REQUEST_NOTIFICATION_CHANNEL_ID";

    public static final String INTENT_FILTER = "INTENT_FILTER";



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        NotificationManager notificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mNotificationChannel = new NotificationChannel(
                    NEW_TRIP_REQUEST_NOTIFICATION_CHANNEL_ID,
                    this.getString(R.string.main_notification_channel_name),
                    NotificationManager.IMPORTANCE_HIGH);
            mNotificationChannel.enableVibration(true);
            mNotificationChannel.enableLights(true);
            notificationManager.createNotificationChannel(mNotificationChannel);
        }

        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e("HELPME", "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e("HELPME", "Data Payload2: " + remoteMessage.getData());

            try {

                if(remoteMessage.getData().get("notificationType").equals("driverAcceptedReq")){
                    Gson gson = new Gson();
                    AcceptedRequestNotification data = gson.fromJson(remoteMessage.getData().toString() ,AcceptedRequestNotification.class);
                    EventBus.getDefault().post(data);

                }
                else if(remoteMessage.getData().get("notificationType").equals("canceledReq")){
                    Intent intent = new Intent(MyFirebaseMessagingService.this,MainActivity.class);
                    intent.putExtra("notificationType","canceledReq");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);


                }
                else if(remoteMessage.getData().get("notificationType").equals("driverArrived")){
                    handleDataMessage("Your Driver Has Arrived to your location");
                    AcceptedRequestNotification data = new AcceptedRequestNotification();
                    data.setNotificationType("driverArrived");
                    EventBus.getDefault().post(data);

                }
                else if(remoteMessage.getData().get("notificationType").equals("requestDone")){
                    Intent intent = new Intent(MyFirebaseMessagingService.this,RatingActivity.class);

                    intent.putExtra("driver_name", Config.driver_name);
                    intent.putExtra("requestId", Config.request_id);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }

//                handleDataMessage(json);


            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        //  if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
        // app is in foreground, broadcast the push message
        Intent intent = new Intent(this,DuringTripActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1555 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,NEW_TRIP_REQUEST_NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
//            pushNotification.putExtra("message", message);
//
//            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//
//            // play notification sound
//            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
//            notificationUtils.playNotificationSound();
        //   }else{
        // If the app is in background, firebase itself handles the notification
        // }
    }

    private void handleDataMessage(String json) {

        Intent intent = new Intent(this,DuringTripActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,NEW_TRIP_REQUEST_NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle(title)
                .setContentText(json)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setPriority(NotificationManager.IMPORTANCE_MAX);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
