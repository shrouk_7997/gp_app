package com.betahubs.enqaz.model.RequestNotificationModel;

import com.google.gson.annotations.SerializedName;

public class Trip {
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public void setDestLat(String destLat) {
        this.destLat = destLat;
    }

    public void setDestLong(String destLong) {
        this.destLong = destLong;
    }

    public Trip( String destLat, String destLong) {
        this.destLat = destLat;
        this.destLong = destLong;
    }

    @SerializedName("requestId")
    private String requestId;
    @SerializedName("destLat")
    private String destLat;
    @SerializedName("destLong")
    private String destLong;

    public String getDestLat() {
        return destLat;
    }

    public String getDestLong() {
        return destLong;
    }

    public String getRequestId() {
        return requestId;
    }
}
