package com.betahubs.enqaz;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.model.TripDetails;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HistoryTripDetailsActivity extends BaseActivity implements OnMapReadyCallback {
    @BindView(R.id.date_textview)
    TextView dateTextview;
    @BindView(R.id.address_from_textview)
    TextView addressFromTextview;
    @BindView(R.id.address_to_textview)
    TextView addressToTextview;
    @BindView(R.id.car_model_textview)
    TextView carModelTextview;
    @BindView(R.id.service_model_textview2)
    TextView serviceModelTextview2;
    @BindView(R.id.driver_name_textview)
    TextView driverNameTextview;
    @BindView(R.id.driver_ratebar)
    RatingBar driverRatebar;
    @BindView(R.id.payment_type_textview)
    TextView paymentTypeTextView;
    @BindView(R.id.estimation_details_distance_tv)
    TextView estimationDetailsDistanceTv;
    @BindView(R.id.estimation_details_startingfare_tv)
    TextView estimationDetailsStartingfareTv;
    @BindView(R.id.estimation_details_waitingtime_cost_tv)
    TextView estimationDetailsWaitingtimeCostTv;
    @BindView(R.id.estimation_details_kilometer_cost_tv)
    TextView estimationDetailsKilometerCostTv;
    @BindView(R.id.estimation_details_total_cost_tv)
    TextView estimationDetailsTotalCostTv;
    @BindView(R.id.login_progress)
    ProgressBar progressBar;
    private SharedPreferences mSharedPreferences;
    private TripDetails.Data tripDetail;
    private TripDetails tripDetails;
    private GoogleMap mMap;
    private boolean apiCalled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_trip_details);
        ButterKnife.bind(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        String requestId = getIntent().getStringExtra("requestId");
        getTripDetails(requestId);
    }

    public void getTripDetails(String requestId) {
        progressBar.setVisibility(View.VISIBLE);
//        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.getTripDetails(mSharedPreferences.getString("token", null)
                , requestId).enqueue(new Callback<TripDetails>() {
            @Override
            public void onResponse(Call<TripDetails> call, Response<TripDetails> response) {
                if (response.isSuccessful()) {
                         tripDetails = response.body();
                        Log.d("TRIPDETAILS:", tripDetails.toString()+"");
                        boolean success = tripDetails.getSuccess();

                        if (success) {
                            apiCalled = true;
                            tripDetail = tripDetails.getData();
                           updateUI();
                            updateMap();
                        } else {
                            Toast.makeText(HistoryTripDetailsActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                        }
                } else {
                    Toast.makeText(HistoryTripDetailsActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<TripDetails> call, Throwable t) {
                Toast.makeText(HistoryTripDetailsActivity.this, "Something went wrong! Please try again later.", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });
    }
    private String formatDate(String date){
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        sourceFormat.setTimeZone(utc);
        try {
            Date convertedDate = sourceFormat.parse(date);
            String a =destFormat.format(convertedDate);
            return a;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void updateUI() {
        String date = formatDate(tripDetail.getCreatedAt());
        dateTextview.setText(date);
        if(tripDetails.getUserRate()!=null){
            double rate = tripDetails.getUserRate();
            driverRatebar.setRating((float) rate);
            driverNameTextview.setText(String.format(getString(R.string.you_rated_drivername), tripDetail.getDriverName()));
        }
        else {
        driverNameTextview.setText(tripDetail.getDriverName());}
        carModelTextview.setText(tripDetail.getCarManufactory()+" / "+tripDetail.getCarModel());
        StringBuffer servicesNames = new StringBuffer();

        for(int i = 0;i<tripDetail.getServiceName().size();i++){
            if(i==(tripDetail.getServiceName().size()-1)){
                servicesNames.append(tripDetail.getServiceName().get(i) );
            }
            else {
                servicesNames.append(tripDetail.getServiceName().get(i) +" & ");
            }
        }
        serviceModelTextview2.setText(servicesNames.toString());
        estimationDetailsTotalCostTv.setText(tripDetail.getFinalCost()+ getString(R.string.le));
        estimationDetailsDistanceTv.setText(tripDetail.getDist()+ getString(R.string.km));
        estimationDetailsStartingfareTv.setText(tripDetail.getServiceBaseFare()+getString(R.string.km));
        estimationDetailsKilometerCostTv.setText(tripDetail.getKmPrice() +getString(R.string.km));
        estimationDetailsWaitingtimeCostTv.setText(tripDetail.getWaitingTime()+getString(R.string.m_space)+tripDetail.getWaitingTimePrice()+getString(R.string.km));
        paymentTypeTextView.setText(tripDetail.getPaymentType());
        if(tripDetails.getStartTripLocation()!= null&&tripDetails.getStartTripLocation().getLat()!=null){
            String from = getAddress(tripDetails.getStartTripLocation().getLat(),tripDetails.getStartTripLocation().getLng());
            addressFromTextview.setText(from);}
        if(tripDetails.getEndTripLocation()!= null&&tripDetails.getEndTripLocation().getLat()!=null){
            String to = getAddress(tripDetails.getEndTripLocation().getLat(),tripDetails.getEndTripLocation().getLng());
            addressToTextview.setText(to);}



    }
    public String getAddress(Double lat,Double lng) {

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if(addresses!=null&&addresses.size()>0&&addresses.get(0)!=null) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                return address;
            }
        } catch (IOException ignored) {
            return "";
        }
        return "";
    }

    @OnClick(R.id.report_problem_button)
    public void onViewClicked() {
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap=googleMap;
        updateMap();
    }
    public void updateMap(){
        if (mMap!=null&&apiCalled){
            LatLng startTripLocation = new LatLng(tripDetails.getStartTripLocation().getLat(),tripDetails.getStartTripLocation().getLng());
            Marker marker2 = null;
            if(tripDetails.getEndTripLocation()!= null&&tripDetails.getEndTripLocation().getLat()!=null){
            LatLng endTripLocation = new LatLng(tripDetails.getEndTripLocation().getLat(),tripDetails.getEndTripLocation().getLng());
              marker2 =  mMap.addMarker(new MarkerOptions().position(endTripLocation).title(getString(R.string.me)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_dropoff_icon)));
            }

            LatLng centerLocation = new LatLng((tripDetails.getStartTripLocation().getLat()+tripDetails.getEndTripLocation().getLat())/2,
                    (tripDetails.getStartTripLocation().getLng()+tripDetails.getEndTripLocation().getLng())/2);

            Marker marker1 = mMap.addMarker(new MarkerOptions().position(startTripLocation).title(getString(R.string.km)).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pickup_icon)));

//            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//                builder.include(marker1.getPosition());
//            if (marker2 != null) {
//                builder.include(marker2.getPosition());
//            }
//            LatLngBounds bounds = builder.build();
//            int padding = 10; // offset from edges of the map in pixels
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(centerLocation,14));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
            return true;
        }
        return false;
    }

}
