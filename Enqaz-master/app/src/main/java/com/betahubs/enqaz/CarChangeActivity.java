package com.betahubs.enqaz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.Config;
import com.betahubs.enqaz.model.Car;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarChangeActivity extends AppCompatActivity {

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.car_plate_number_edittext)
    EditText carPlateNumberEdittext;
    @BindView(R.id.manufactory_name_textview)
    TextView manufactoryNameTextview;
    @BindView(R.id.model_name_textview)
    TextView modelNameTextview;
    @BindView(R.id.car_year_textview)
    TextView carYearTextview;
    private SharedPreferences mSharedPreferences;
    private String carManufacturer;
    private String carManufacturerId;
    private String carModel;
    private String carModelId;
    private String carYear;
    private static final int SELECT_CAR_DETAILS_CODE = 455;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_change);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        carPlateNumberEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (count <= carPlateNumberEdittext.getText().toString().length()
                        &&carPlateNumberEdittext.getText().toString().length()%2==1){
                    carPlateNumberEdittext.setText(carPlateNumberEdittext.getText().toString()+" ");
                    int pos = carPlateNumberEdittext.getText().length();
                    carPlateNumberEdittext.setSelection(pos);
                }else if (count >= carPlateNumberEdittext.getText().toString().length()
                        &&carPlateNumberEdittext.getText().toString().length()%2==1){
                    carPlateNumberEdittext.setText(carPlateNumberEdittext.getText().toString().substring(0,carPlateNumberEdittext.getText().toString().length()-1));
                    int pos = carPlateNumberEdittext.getText().length();
                    carPlateNumberEdittext.setSelection(pos);
                }
                count = carPlateNumberEdittext.getText().toString().length();
            }
        });

    }


    private void updateCarTextViews(){
        if(carManufacturer!=null){manufactoryNameTextview.setText(carManufacturer);}
        if(carModel!=null){modelNameTextview.setText(carModel);}
        if(carYear!=null){carYearTextview.setText(carYear);}
    }

    private void addCar() {
        //Create array of car objects
        progressBar.setVisibility(View.VISIBLE);
        Gson gson = new Gson();
        Car[] cars = new Car[]{new Car(carManufacturer, carModel, "black", carPlateNumberEdittext.getText().toString())};
        String carObject = gson.toJson(cars);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), carObject);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.addCar(mSharedPreferences.getString("token", null), "application/json", body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    String result = response.body().string();
                    Log.d("TAGG", " Response add Car: " + result);
                    JSONObject jsonObject = new JSONObject(result);
                    boolean success = jsonObject.getBoolean("success");
                    if (success) {
                        Toast.makeText(CarChangeActivity.this, R.string.car_added, Toast.LENGTH_SHORT).show();
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(Config.CAR_MANUFACTURER,carManufacturer);
                        returnIntent.putExtra(Config.CAR_MODEL,carModel);
                        returnIntent.putExtra(Config.CAR_YEAR,carYear);
                        returnIntent.putExtra(Config.CAR_PLATE,carPlateNumberEdittext.getText().toString());
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        Toast.makeText(CarChangeActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(CarChangeActivity.this, R.string.some_thing_went_wrong_please_try_later, Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_CAR_DETAILS_CODE && resultCode == RESULT_OK) {
            carManufacturer = data.getStringExtra(Config.CAR_MANUFACTURER);
            carManufacturerId = data.getStringExtra(Config.CAR_MANUFACTURER_ID);
            carModel = data.getStringExtra(Config.CAR_MODEL);
            carModelId = data.getStringExtra(Config.CAR_MODEL_ID);
            carYear = data.getStringExtra(Config.CAR_YEAR);
            updateCarTextViews();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick({R.id.change_manufacturer_buttton, R.id.change_car_model_button, R.id.change_car_year_button, R.id.add_new_car})
    public void onAddCarScreenClicks(View view) {
        Intent intent = new Intent(CarChangeActivity.this, CarManufactoryActivtiy.class);
        intent.putExtra(Config.CAR_MANUFACTURER, carManufacturer);
        intent.putExtra(Config.CAR_MANUFACTURER_ID, carManufacturerId);
        intent.putExtra(Config.CAR_MODEL, carModel);
        intent.putExtra(Config.CAR_MODEL_ID, carModelId);
        switch (view.getId()) {
            case R.id.change_manufacturer_buttton:
                intent.putExtra("TO_BE_SHOWN", Config.CAR_MANUFACTURER);
                startActivityForResult(intent, SELECT_CAR_DETAILS_CODE);
                break;
            case R.id.change_car_model_button:
                intent.putExtra("TO_BE_SHOWN", Config.CAR_MODEL);
                startActivityForResult(intent, SELECT_CAR_DETAILS_CODE);
                break;
            case R.id.change_car_year_button:
                intent.putExtra("TO_BE_SHOWN", Config.CAR_YEAR);
                startActivityForResult(intent, SELECT_CAR_DETAILS_CODE);
                break;
            case R.id.add_new_car:
                Log.d("TAGG", "TAGG");
                if (carManufacturer != null && !carManufacturer.isEmpty()) {
                    carPlateNumberEdittext.setError(null);
                    if (carPlateNumberEdittext.getText().toString().isEmpty()) {
                        carPlateNumberEdittext.setError(getString(R.string.error_field_required));
                        carPlateNumberEdittext.requestFocus();
                    } else {
                        addCar();
                        View currentView = getCurrentFocus();
                        if (currentView != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(currentView.getWindowToken(), 0);
                        }
                    }
                }
                break;
        }
    }
}
