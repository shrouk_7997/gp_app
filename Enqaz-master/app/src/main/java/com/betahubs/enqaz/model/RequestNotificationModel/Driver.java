package com.betahubs.enqaz.model.RequestNotificationModel;

import com.google.gson.annotations.SerializedName;

public class Driver {
    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Driver(String name, String id, String phoneNumber, Double rating) {
        this.name = name;
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.rating = rating;
    }

    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private String id;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("rating")
    private Double rating;


    public Double getRating() {
        return rating;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

}