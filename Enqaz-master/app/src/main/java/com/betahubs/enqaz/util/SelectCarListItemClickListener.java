package com.betahubs.enqaz.util;

public interface SelectCarListItemClickListener {
    void onSelectCarItemClick(String key, String value);
}