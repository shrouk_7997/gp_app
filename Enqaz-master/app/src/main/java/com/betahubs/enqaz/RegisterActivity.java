package com.betahubs.enqaz;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.username_edittext)
    EditText usernameEdittext;
    @BindView(R.id.mobileET)
    EditText mobileEditText;
    @BindView(R.id.nameET)
    EditText nameEditText;
    @BindView(R.id.emailET)
    EditText emailEditText;
    @BindView(R.id.passwordET)
    EditText passwordEditText;
    @BindView(R.id.sign_up_button)
    Button signUpButton;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    private boolean isRequesting = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegisterActivity.this,IntroActivity.class);
        startActivity(intent);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(RegisterActivity.this,IntroActivity.class);
                startActivity(intent);
                finish();
        }
        return true;
    }
    @OnClick(R.id.sign_up_button)
    public void onViewClicked() {
        attemptLogin();
    }

    private void attemptLogin() {


        if (isRequesting) {
            return;
        }

        // Reset errors.
        nameEditText.setError(null);
        emailEditText.setError(null);
        mobileEditText.setError(null);
        passwordEditText.setError(null);
        usernameEdittext.setError(null);

        // Store values at the time of the login attempt.
        String name = nameEditText.getText().toString();
        String email = emailEditText.getText().toString();
        String phoneNumber = mobileEditText.getText().toString();
        String password = passwordEditText.getText().toString();
        String username = usernameEdittext.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(name)) {
            nameEditText.setError(getString(R.string.error_field_required));
            focusView = nameEditText;
            cancel = true;
        }
        //--------------------
        if (TextUtils.isEmpty(username)) {
            usernameEdittext.setError(getString(R.string.error_field_required));
            focusView = usernameEdittext;
            cancel = true;
        }
        //--------------------
        if (TextUtils.isEmpty(email)) {
            emailEditText.setError(getString(R.string.error_field_required));
            focusView = emailEditText;
            cancel = true;
        } else if (!isEmailValid(email)) {
            emailEditText.setError(getString(R.string.error_invalid_username));
            focusView = emailEditText;
            cancel = true;
        }
        //--------------------
        if (TextUtils.isEmpty(phoneNumber)) {
            mobileEditText.setError(getString(R.string.error_field_required));
            focusView = mobileEditText;
            cancel = true;
        } else if (!isPhoneVaild(phoneNumber)) {
            mobileEditText.setError(getString(R.string.error_invalid_phone));
            focusView = mobileEditText;
            cancel = true;
        }
        //--------------------
        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.error_field_required));
            focusView = passwordEditText;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            passwordEditText.setError(getString(R.string.error_invalid_password));
            focusView = passwordEditText;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt signUp and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //showProgress(true);
            registerRequest(name, email, phoneNumber, password, username);
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@") && email.contains(".");
    }

    private boolean isPasswordValid(String password) {

        return password.length() > 5;
    }

    private boolean isPhoneVaild(String phoneNumber) {
        //String regexStr = "^[+]?[0-9]{10,13}$";
        return PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber) && phoneNumber.length() < 15 && phoneNumber.length() > 7;
    }

    public void registerRequest(String name, final String email, String phone, String password, String username) {
        isRequesting = true;
        signUpButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.registerCustomer(name, email, phone, username, password).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("TAGG", "Register Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            Toast.makeText(RegisterActivity.this, R.string.account_registered_successfully, Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(RegisterActivity.this, ActivationCodeActivity.class);
                            intent.putExtra("COMING_FROM","register");
                            intent.putExtra(Intent.EXTRA_PHONE_NUMBER,phone);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(RegisterActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
                isRequesting = false;
                signUpButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                isRequesting = false;
                signUpButton.setEnabled(true);
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
