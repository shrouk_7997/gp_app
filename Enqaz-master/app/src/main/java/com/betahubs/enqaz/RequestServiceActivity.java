package com.betahubs.enqaz;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.betahubs.enqaz.adapter.ServicesAdapter;
import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.app.Config;
import com.betahubs.enqaz.model.EstimateFare;
import com.betahubs.enqaz.model.PromoCode;
import com.betahubs.enqaz.model.Services;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.paymob.acceptsdk.IntentConstants;
import com.paymob.acceptsdk.PayActivity;
import com.paymob.acceptsdk.PayActivityIntentKeys;
import com.paymob.acceptsdk.PayResponseKeys;
import com.paymob.acceptsdk.SaveCardResponseKeys;
import com.paymob.acceptsdk.ToastMaker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class RequestServiceActivity extends BaseActivity implements OnMapReadyCallback, ServicesAdapter.ListItemClickListener {



    @BindView(R.id.recyclerView3)
    RecyclerView mRecyclerView;
    @BindView(R.id.estimated_fare_textview)
    TextView estimatedFareTextview;
    @BindView(R.id.payment_type_textview)
    TextView paymentTypeTextview;
    @BindView(R.id.car_details_textview)
    TextView carDetailsTextview;
    @BindView(R.id.request_now_button)
    LinearLayout requestNowButton;
    @BindView(R.id.request_layout)
    ConstraintLayout requestLayout;
    @BindView(R.id.payment_layout)
    ConstraintLayout paymentLayout;
    @BindView(R.id.request_estimation_details_layout)
    ConstraintLayout estimationDetailsLayout;
    @BindView(R.id.rating_layout)
    ConstraintLayout ratingLayout;
    @BindView(R.id.req_progress_bar)
    ProgressBar mProgressBar;

    @BindView(R.id.cash_radiobt_imageview)
    ImageView cashRadiobtImageview;
    @BindView(R.id.credit_textview)
    TextView creditTextview;
    @BindView(R.id.credit_radiobt_imageview)
    ImageView creditRadiobtImageview;
    @BindView(R.id.credit_layout)
    ConstraintLayout creditLayout;
    @BindView(R.id.add_new_credit_card_button)
    Button addNewCreditCardButton;
    @BindView(R.id.estimation_details_distance_tv)
    TextView estimationDetailsDistanceTv;
    @BindView(R.id.estimation_details_startingfare_tv)
    TextView estimationDetailsStartingfareTv;
    @BindView(R.id.estimation_details_waitingtime_cost_tv)
    TextView estimationDetailsWaitingtimeCostTv;
    @BindView(R.id.estimation_details_kilometer_cost_tv)
    TextView estimationDetailsKilometerCostTv;
    @BindView(R.id.estimation_details_total_cost_tv)
    TextView estimationDetailsTotalCostTv;
    @BindView(R.id.request_button_bottom_text)
    TextView requestButtonBottomText;
    @BindView(R.id.request_button_top_text)
    TextView requestButtonTopText;

    @BindView(R.id.promoCodeLayout)
    ConstraintLayout promoCodeLayout;
    @BindView(R.id.promoCodeEditText)
    EditText promoCodeEditText;
    @BindView(R.id.confirmPromoCodeButton)
    Button confirmPromoCodeButton;
    @BindView(R.id.addPromoCodeShowButton)
    Button addPromoCodeShowButton;



    private RecyclerView.Adapter mAdapter;

    private GoogleMap mMap;
    private LatLng pickUpLatLng;
    private LatLng destLatLng;
    private String paymentType = "Cash";
    private String carManufacturer;
    private String carModel;
    private ArrayList<String> serviceId;
    private ArrayList<Integer> serviceIndex;
    private LocationCallback mLocationCallback;
    static final int ACCEPT_PAYMENT_REQUEST = 10;
    private String paymentKey;
    private ArrayList<Services.Service> mServices;
    //    private MenuItem cancelMenuButton;
    private SharedPreferences mSharedPreferences;
    private String requestId;

    private boolean isWinch = false;
    private String mRequestType;

    //Schedule vars
    private int scheduleHour;
    private int scheduleMint;
    private int scheduleDay;
    private int scheduleMonth;
    private int scheduleYear;
    private double firstEstimate;
    private double lastEstimate;
    private ArrayList<String> servicesNames;
    private String carYear;
    private static final  int NEW_CAR_REQUEST_CODE = 411;
    private String carPlateNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_service);
        ButterKnife.bind(this);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setTitle(R.string.select_pickup_point);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        getCar();


        Intent intent = getIntent();
        Bundle requestBundleExtras = getIntent().getBundleExtra("REQUEST_BUNDLE");
        serviceId = requestBundleExtras.getStringArrayList("SERVICE_ID");
        serviceIndex = requestBundleExtras.getIntegerArrayList("SERVICE_INDEX");
        servicesNames = requestBundleExtras.getStringArrayList("SERVICE_NAME");
        destLatLng = requestBundleExtras.getParcelable("DEST_LATLNG");
        pickUpLatLng = requestBundleExtras.getParcelable("PICKUP_LATLNG");
        //Check if request now or scheduled request, if scheduled let user select request time/date
        mRequestType = requestBundleExtras.getString("REQUEST_TYPE");
        if (mRequestType.equals("REQUEST_LATER")) {
            requestButtonTopText.setText(R.string.schedule_request);
            ShowScheduleScreen();
        }


        String paymentType = mSharedPreferences.getString(Config.PAYMENT_TYPE, "Cash");
        String maskedPan = mSharedPreferences.getString(Config.MASKED_PAN, null);
        if (maskedPan != null) {
            creditLayout.setVisibility(View.VISIBLE);
            creditTextview.setText(maskedPan);

        }
        if (paymentType.equals("Cash")) {
            paymentType = "Cash";
            cashRadiobtImageview.setImageDrawable(getResources().getDrawable(R.drawable.payment_points_check));
        } else if (paymentType.equals("Visa")) {
            paymentType = "Visa";
            creditRadiobtImageview.setImageDrawable(getResources().getDrawable(R.drawable.payment_points_check));
            getCards();
        }


        if (servicesNames != null && servicesNames.contains("Winch")) {
            isWinch = true;
        }
        mServices = new ArrayList<>();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mAdapter = new ServicesAdapter(RequestServiceActivity.this, RequestServiceActivity.this, mServices);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(layoutManager);

        getServiesList();
        getEstimateFareRequest();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }



    public void getServiesList() {
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.getServicesList().enqueue(new Callback<Services>() {
            @Override
            public void onResponse(Call<Services> call, Response<Services> response) {
                if (response.isSuccessful()) {
                    Services respond = response.body();
                    boolean success = respond.isSuccess();
                    if (success) {

                        ArrayList<Services.Service> allServices = respond.getServices();
                        ArrayList<Services.Service> activeServices = new ArrayList<>();

                        for (int i = 0; i < allServices.size(); i++) {
                            Services.Service service = allServices.get(i);
                            if (service.isActive()) {
                                activeServices.add(service);
                            }
                        }

                        mServices.clear();
                        for (int i = 0; i < serviceIndex.size(); i++) {
                            mServices.add(activeServices.get(serviceIndex.get(i)));
                        }
                        mAdapter.notifyDataSetChanged();


                    } else {
                        Toast.makeText(RequestServiceActivity.this, "" + respond.getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(RequestServiceActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Services> call, Throwable t) {
                Toast.makeText(RequestServiceActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            }
        });
    }




    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }




    @OnClick({R.id.estimate_fare_details, R.id.payment_change_textview, R.id.car_change_textview, R.id.request_now_button,
            R.id.cash_layout, R.id.credit_layout, R.id.add_new_credit_card_button, R.id.estimation_details_confirm_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.estimate_fare_details:
                requestLayout.setVisibility(View.GONE);
                estimationDetailsLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.payment_change_textview:
                requestLayout.setVisibility(View.GONE);
                paymentLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.car_change_textview:
                Intent carIntent = new Intent(RequestServiceActivity.this,CarChangeActivity.class);
                startActivityForResult(carIntent,NEW_CAR_REQUEST_CODE);
                break;
            case R.id.request_now_button:
                //request now or schedule request
                if (mRequestType.equals("REQUEST_NOW")) {
                    makeRequest();
                } else if (mRequestType.equals("REQUEST_LATER")) {
                    makeScheduleRequest();
                }
                break;
            case R.id.cash_layout:
                paymentType = "Cash";
                paymentTypeTextview.setText(R.string.cash);
                paymentLayout.setVisibility(View.GONE);
                requestLayout.setVisibility(View.VISIBLE);

                mSharedPreferences.edit().putString(Config.PAYMENT_TYPE, "Cash").apply();
                cashRadiobtImageview.setImageDrawable(getResources().getDrawable(R.drawable.payment_points_check));
                creditRadiobtImageview.setImageDrawable(getResources().getDrawable(R.drawable.payment_points_non_checked));
                break;
            case R.id.credit_layout:
                paymentType = "Visa";
                paymentTypeTextview.setText(getString(R.string.credit) + " " + mSharedPreferences.getString(Config.MASKED_PAN, ""));
                paymentLayout.setVisibility(View.GONE);
                requestLayout.setVisibility(View.VISIBLE);

                mSharedPreferences.edit().putString(Config.PAYMENT_TYPE, "Visa").apply();
                cashRadiobtImageview.setImageDrawable(getResources().getDrawable(R.drawable.payment_points_non_checked));
                creditRadiobtImageview.setImageDrawable(getResources().getDrawable(R.drawable.payment_points_check));
                break;
            case R.id.add_new_credit_card_button:
                getPaymentKey();
                break;

            case R.id.estimation_details_confirm_button:
                requestLayout.setVisibility(View.VISIBLE);
                estimationDetailsLayout.setVisibility(View.GONE);
                break;
        }
    }



    private void makeScheduleRequest() {
        if (carModel == null || carModel == null) {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView().getRootView(), "Please add a car", Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            snackbar.show();
        } else {
            if (scheduleDay > 0 && (scheduleMint > 0 || scheduleHour > 0)) {
                mProgressBar.setVisibility(View.VISIBLE);
                final DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("schedule");
                Map<String, Object> scheduleRequest = new HashMap<>();
                scheduleRequest.put("customerLat", pickUpLatLng.latitude);
                scheduleRequest.put("customerLng", pickUpLatLng.longitude);
                if (isWinch && destLatLng != null) {
                    scheduleRequest.put("destLat", destLatLng.latitude);
                    scheduleRequest.put("destLng", destLatLng.longitude);
                }
                scheduleRequest.put("estimatedFare", firstEstimate + "-" + lastEstimate);
                scheduleRequest.put("serviceName", servicesNames);
                scheduleRequest.put("serviceId", serviceId);
                scheduleRequest.put("carModel", carModel);
                scheduleRequest.put("carManufactory", carManufacturer);
                scheduleRequest.put("paymentType", paymentType);
                Log.d("USERID", mSharedPreferences.getString("userId", ""));
                scheduleRequest.put("customerName", mSharedPreferences.getString(Config.USER_NAME, ""));
                scheduleRequest.put("customerPhone", mSharedPreferences.getString(Config.USER_PHONENUMBER, ""));
                scheduleRequest.put("userId", mSharedPreferences.getString(Config.USER_ID, ""));
                scheduleRequest.put("token", mSharedPreferences.getString(Config.USER_API_TOKEN, ""));
                scheduleRequest.put("year", scheduleYear);
                scheduleRequest.put("month", scheduleMonth);
                scheduleRequest.put("day", scheduleDay);
                scheduleRequest.put("hour", scheduleHour);
                scheduleRequest.put("min", scheduleMint);
                ref.push().setValue(scheduleRequest, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                        Toast.makeText(RequestServiceActivity.this, "Your schedule request was created successfuly", Toast.LENGTH_LONG).show();
                        Intent intent4 = new Intent(RequestServiceActivity.this, MainActivity.class);
                        intent4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent4);
                        finish();
                    }
                });

            } else {
                ShowScheduleScreen();
            }
        }
    }

    private void ShowScheduleScreen() {
        DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                scheduleYear = year;
                scheduleDay = day;
                scheduleMonth = month;

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(RequestServiceActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        scheduleHour = selectedHour;
                        scheduleMint = selectedMinute;
                        //Next method used to prevent mints from being just one number like this 9:3 instead it will be 9:03
                        String selectedMintString;
                        if (selectedMinute < 10) selectedMintString = "0" + selectedHour;
                        else selectedMintString = selectedHour + "";
                        requestButtonBottomText.setText(scheduleDay + "-" + (scheduleMonth + 1) + "-" + scheduleYear + " at " + selectedHour + ":" + selectedMintString);

                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        };
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

//        Calendar cal2 = Calendar.getInstance();
//        int year2 = cal2.get(Calendar.YEAR);
//        int month2 = cal2.get(Calendar.MONTH);
//        int day2 = cal2.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog dialog = new DatePickerDialog(
                RequestServiceActivity.this,
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                mDateSetListener,
                year, month, day);

//        cal2.set(year2, month2, day2);
        dialog.getDatePicker().setMinDate(cal.getTimeInMillis() - 1000);
        int maxMonth = month + 1;
        if (month == 11) {
            maxMonth = 0;
            year++;
        }
        cal.set(year, maxMonth, day);
        dialog.getDatePicker().setMaxDate(cal.getTimeInMillis());

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        dialog.show();

    }


    private void getPaymentKey() {
        addNewCreditCardButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.getPaymentKey(mSharedPreferences.getString("token", null)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    String result = null;
                    try {
                        result = response.body().string();

                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            paymentKey = jsonObject.getString("token");
                            startPayActivityNoToken(false);
                        } else {
                            Toast.makeText(RequestServiceActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(RequestServiceActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
                addNewCreditCardButton.setEnabled(true);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RequestServiceActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
                addNewCreditCardButton.setEnabled(true);

            }
        });
    }
    private void getEstimateFareRequest() {
        String destLat = null;
        String destLong = null;
        if (isWinch && destLatLng != null) {
            destLat = destLatLng.latitude + "";
            destLong = destLatLng.longitude + "";
        }
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.estimateFare(mSharedPreferences.getString("token", null), serviceId,
                pickUpLatLng.latitude + "",
                pickUpLatLng.longitude + "",
                destLat,
                destLong).enqueue(new Callback<EstimateFare>() {
            @Override
            public void onResponse(Call<EstimateFare> call, Response<EstimateFare> response) {
                if (response.isSuccessful()) {

                    EstimateFare result = response.body();
                    Log.d("ESTIMATE RESPONSE:", result.toString());

                    if (result.getSuccess()) {
                        firstEstimate = result.getFirstEstimate();
                        lastEstimate = result.getLastEstimate();
                        Double distance = result.getDistance();
                        Integer startingFare = result.getStartingFare();
                        Integer waitingTimeCost = result.getWaitingTimeCost();
                        Double kilometersCost = result.getKilometersCost();
                        estimatedFareTextview.setText(firstEstimate + " : " + lastEstimate + getString(R.string.le));
                        if(distance!=null)estimationDetailsDistanceTv.setText(distance + getString(R.string.km));
                        estimationDetailsKilometerCostTv.setText(kilometersCost + getString(R.string.le));
                        estimationDetailsStartingfareTv.setText(startingFare + getString(R.string.le));
                        estimationDetailsWaitingtimeCostTv.setText(waitingTimeCost + getString(R.string.le));
                        estimationDetailsTotalCostTv.setText(firstEstimate + " : " + lastEstimate + getString(R.string.le));

                        if (result.getPromoCodeFormat() != null) {
                            addPromoCodeShowButton.setText(result.getPromoCodeFormat());
                        }
                        getSupportActionBar().setTitle(R.string.request_a_service);

                    } else {
                        Toast.makeText(RequestServiceActivity.this, "" + result.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(RequestServiceActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<EstimateFare> call, Throwable t) {
                Toast.makeText(RequestServiceActivity.this, getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
            }
        });

    }

    private void makeRequest() {
        String destLat = null;
        String destLong = null;
        if (isWinch && destLatLng != null) {
            destLat = destLatLng.latitude + "";
            destLong = destLatLng.longitude + "";
        }
        requestNowButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.requestService(mSharedPreferences.getString("token", null), serviceId, servicesNames, pickUpLatLng.latitude + "", pickUpLatLng.longitude + "", paymentType, carManufacturer, carModel
                , destLat, destLong, null).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("REQUEST RESPONSE:", result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            requestId = jsonObject.getString("newRequestId");

                            Intent intent = new Intent(RequestServiceActivity.this, DuringTripActivity.class);
                            intent.putExtra(Config.REQUEST_ID, requestId);
                            intent.putExtra("pickUpLat", pickUpLatLng.latitude);
                            intent.putExtra("pickUpLong", pickUpLatLng.longitude);
                            intent.putExtra("servicesNames", servicesNames);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            String message = jsonObject.optString("message");
                            if (message != null & message.equals("please send us all required fields")) {
                                AlertDialog.Builder builder;
                                builder = new AlertDialog.Builder(RequestServiceActivity.this);
                                builder.setTitle(R.string.car_details_are_missing)
                                        .setMessage(getString(R.string.please_enter_car_details))
                                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                            }
                                        }).show();
                            } else {
                                Toast.makeText(RequestServiceActivity.this, "" + message, Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(RequestServiceActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
                requestNowButton.setEnabled(true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RequestServiceActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
                requestNowButton.setEnabled(true);
            }
        });
    }

    @Override
    protected void onStop() {
//        getFusedLocationProviderClient(this).removeLocationUpdates(mLocationCallback);
        super.onStop();
    }

    private void startPayActivityNoToken(Boolean showSaveCard) {
        Intent pay_intent = new Intent(this, PayActivity.class);
        putNormalExtras(pay_intent);

        pay_intent.putExtra(PayActivityIntentKeys.SAVE_CARD_DEFAULT, true);
        pay_intent.putExtra(PayActivityIntentKeys.SHOW_ALERTS, showSaveCard);
        pay_intent.putExtra(PayActivityIntentKeys.SHOW_SAVE_CARD, showSaveCard);
        pay_intent.putExtra(PayActivityIntentKeys.THEME_COLOR, 0xff4d4d4f);

        startActivityForResult(pay_intent, ACCEPT_PAYMENT_REQUEST);
    }

    private void putNormalExtras(Intent intent) {
        // Pass the correct values for the billing data keys
        intent.putExtra(PayActivityIntentKeys.FIRST_NAME, "Mr/Mrs");
        intent.putExtra(PayActivityIntentKeys.LAST_NAME, mSharedPreferences.getString(Config.USER_NAME, "none"));
        intent.putExtra(PayActivityIntentKeys.BUILDING, "none");
        intent.putExtra(PayActivityIntentKeys.FLOOR, "none");
        intent.putExtra(PayActivityIntentKeys.APARTMENT, "none");
        intent.putExtra(PayActivityIntentKeys.CITY, "none");
        intent.putExtra(PayActivityIntentKeys.STATE, "none");
        intent.putExtra(PayActivityIntentKeys.COUNTRY, "Egypt");
        intent.putExtra(PayActivityIntentKeys.EMAIL, mSharedPreferences.getString(Config.USER_EMAIL, "none"));
        intent.putExtra(PayActivityIntentKeys.PHONE_NUMBER, mSharedPreferences.getString(Config.USER_PHONENUMBER, "none"));
        intent.putExtra(PayActivityIntentKeys.POSTAL_CODE, "none");

        intent.putExtra(PayActivityIntentKeys.PAYMENT_KEY, paymentKey);

        intent.putExtra(PayActivityIntentKeys.THREE_D_SECURE_ACTIVITY_TITLE, "Verification");
    }


    @Override
    public void onListItemClick(int id) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


         if (requestCode == NEW_CAR_REQUEST_CODE && resultCode == RESULT_OK){
            carManufacturer = data.getStringExtra(Config.CAR_MANUFACTURER);
            carModel = data.getStringExtra(Config.CAR_MODEL);
            carPlateNumber = data.getStringExtra(Config.CAR_PLATE);
            updateCarDetailUI(carManufacturer, carModel);
        }

        //PAYMOB -->

        else if (requestCode == ACCEPT_PAYMENT_REQUEST) {
            Bundle extras = data.getExtras();

            if (resultCode == IntentConstants.USER_CANCELED) {
                // User canceled and did no payment request was fired
                ToastMaker.displayShortToast(this, "User canceled!!");
            } else if (resultCode == IntentConstants.MISSING_ARGUMENT) {
                // You forgot to pass an important key-value pair in the intent's extras
                ToastMaker.displayShortToast(this, "Missing Argument == " + extras.getString(IntentConstants.MISSING_ARGUMENT_VALUE));
            } else if (resultCode == IntentConstants.TRANSACTION_ERROR) {
                // An error occurred while handling an API's response
                ToastMaker.displayShortToast(this, "Reason == " + extras.getString(IntentConstants.TRANSACTION_ERROR_REASON));
            } else if (resultCode == IntentConstants.TRANSACTION_REJECTED) {
                // User attempted to pay but their transaction was rejected

                // Use the static keys declared in PayResponseKeys to extract the fields you want
                ToastMaker.displayShortToast(this, extras.getString(PayResponseKeys.DATA_MESSAGE));
            } else if (resultCode == IntentConstants.TRANSACTION_REJECTED_PARSING_ISSUE) {
                // User attempted to pay but their transaction was rejected. An error occured while reading the returned JSON
                ToastMaker.displayShortToast(this, extras.getString(IntentConstants.RAW_PAY_RESPONSE));
            } else if (resultCode == IntentConstants.TRANSACTION_SUCCESSFUL) {
                // User finished their payment successfully

                // Use the static keys declared in PayResponseKeys to extract the fields you want
                ToastMaker.displayShortToast(this, extras.getString(PayResponseKeys.DATA_MESSAGE));
            } else if (resultCode == IntentConstants.TRANSACTION_SUCCESSFUL_PARSING_ISSUE) {
                // User finished their payment successfully. An error occured while reading the returned JSON.
                ToastMaker.displayShortToast(this, "TRANSACTION_SUCCESSFUL - Parsing Issue");

                // ToastMaker.displayShortToast(this, extras.getString(IntentConstants.RAW_PAY_RESPONSE));
            } else if (resultCode == IntentConstants.TRANSACTION_SUCCESSFUL_CARD_SAVED) {
                // User finished their payment successfully and card was saved.
                Log.i("TOKENIDENTIFIER REQUEST", extras.getString(SaveCardResponseKeys.TOKEN));
                addCardRequest(extras.getString(SaveCardResponseKeys.TOKEN), extras.getString(SaveCardResponseKeys.MASKED_PAN));
                // Use the static keys declared in PayResponseKeys to extract the fields you want
                // Use the static keys declared in SaveCardResponseKeys to extract the fields you want
//                ToastMaker.displayShortToast(this, "Token == " + extras.getString(SaveCardResponseKeys.TOKEN));
            } else if (resultCode == IntentConstants.USER_CANCELED_3D_SECURE_VERIFICATION) {
                ToastMaker.displayShortToast(this, "User canceled 3-d secure verification!!");

                // Note that a payment process was attempted. You can extract the original returned values
                // Use the static keys declared in PayResponseKeys to extract the fields you want
                ToastMaker.displayShortToast(this, extras.getString(PayResponseKeys.PENDING));
            } else if (resultCode == IntentConstants.USER_CANCELED_3D_SECURE_VERIFICATION_PARSING_ISSUE) {
                ToastMaker.displayShortToast(this, "User canceled 3-d scure verification - Parsing Issue!!");

                // Note that a payment process was attempted.
                // User finished their payment successfully. An error occured while reading the returned JSON.
                ToastMaker.displayShortToast(this, extras.getString(IntentConstants.RAW_PAY_RESPONSE));
            }
        }
    }

    private void updateCarDetailUI(String carManufacturer, String carModel){
        carDetailsTextview.setText(carManufacturer + " / " + carModel);
        carDetailsTextview.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    private void addCardRequest(String tokenIdentifier, final String maskedPan) {
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.sendPaymentTokenIdentifier(mSharedPreferences.getString("token", null)
                , tokenIdentifier, maskedPan).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("AddCard RESPONSE:", result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");

                        if (success) {
                            SharedPreferences.Editor editor = mSharedPreferences.edit();
                            editor.putString(Config.MASKED_PAN, maskedPan);
                            editor.putString(Config.PAYMENT_TYPE, "Visa");
                            editor.apply();
                            creditRadiobtImageview.setImageDrawable(getResources().getDrawable(R.drawable.payment_points_check));
                            cashRadiobtImageview.setImageDrawable(getResources().getDrawable(R.drawable.payment_points_non_checked));
                            creditLayout.setVisibility(View.VISIBLE);
                            creditTextview.setText(maskedPan);
                            Toast.makeText(RequestServiceActivity.this, R.string.card_is_saved, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(RequestServiceActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(RequestServiceActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                }
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RequestServiceActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);

            }
        });
    }

    @OnClick({R.id.addPromoCodeShowButton, R.id.cancelPromoCodeButton, R.id.confirmPromoCodeButton})
    public void onPromoButtonsClicked(View view) {
        switch (view.getId()) {
            case R.id.addPromoCodeShowButton:
                promoCodeLayout.setVisibility(View.VISIBLE);
                requestLayout.setVisibility(View.GONE);
                break;
            case R.id.cancelPromoCodeButton:
                promoCodeLayout.setVisibility(View.GONE);
                requestLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.confirmPromoCodeButton:
                //add Promocode api
                addPromoCode();

                break;
        }
    }

    private void addPromoCode() {
        addNewCreditCardButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.addPromoCode(mSharedPreferences.getString("token", null), promoCodeEditText.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();

                        Log.d("addPromo requestActiv", "Payment key " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            promoCodeLayout.setVisibility(View.GONE);
                            requestLayout.setVisibility(View.VISIBLE);

                            String promocodeJSON = jsonObject.getJSONObject("promoCode").toString();
                            Gson gson = new Gson();
                            PromoCode promoCode1 = gson.fromJson(promocodeJSON, PromoCode.class);
                            addPromoCodeShowButton.setText(promoCode1.code + " " + promoCode1.discountRate + "%");
                        } else {
                            Toast.makeText(RequestServiceActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_SHORT).show();


                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(RequestServiceActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);
                addNewCreditCardButton.setEnabled(true);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RequestServiceActivity.this, getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
                addNewCreditCardButton.setEnabled(true);

            }
        });
    }

    private void getCar() {
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.getCars(mSharedPreferences.getString("token", null)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("TAG", " Response: " + result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            JSONObject carsJSON = jsonObject.getJSONObject("cars");
                            carManufacturer = carsJSON.getString("carManufactory");
                            carModel = carsJSON.getString("carModel");
//                            carPlateNumber = carsJSON.getString("carNumber");
                            updateCarDetailUI(carManufacturer,carModel);

                        } else {
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(RequestServiceActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                }
                mProgressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RequestServiceActivity.this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    private void getCards() {
        addNewCreditCardButton.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.getCards(mSharedPreferences.getString("token", null)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if (success) {
                            JSONArray cardsJSON = jsonObject.getJSONArray("cards");
                            JSONObject cardJSON = cardsJSON.getJSONObject(cardsJSON.length()-1);
                            String maskedPan = cardJSON.getString("maskedPan");
                            if (maskedPan != null) {
                                paymentTypeTextview.setText(getString(R.string.credit) + " " +maskedPan);
                                creditLayout.setVisibility(View.VISIBLE);
                                creditTextview.setText(maskedPan);
                            }
                        } else {
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }
                mProgressBar.setVisibility(View.GONE);
                addNewCreditCardButton.setEnabled(true);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                    mProgressBar.setVisibility(View.GONE);
                    addNewCreditCardButton.setEnabled(true);

            }
        });
    }
//
//    @Override
//    protected void onDestroy() {
//        getFusedLocationProviderClient(this).removeLocationUpdates(mLocationCallback);
//        finish();
//        super.onDestroy();
//    }
}

