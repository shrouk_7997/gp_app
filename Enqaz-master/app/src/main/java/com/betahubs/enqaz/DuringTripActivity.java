package com.betahubs.enqaz;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.app.Config;
import com.betahubs.enqaz.model.RequestNotificationModel.AcceptedRequestNotification;
import com.betahubs.enqaz.model.RequestNotificationModel.Car;
import com.betahubs.enqaz.model.RequestNotificationModel.Driver;
import com.betahubs.enqaz.model.RequestNotificationModel.Trip;
import com.betahubs.enqaz.service.MyFirebaseMessagingService;
import com.betahubs.enqaz.util.DataParser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CustomCap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.betahubs.enqaz.app.Config.MAP_ZOOM_LEVEL;
import static com.betahubs.enqaz.util.GoogleDirectionUtil.downloadUrl;
import static com.betahubs.enqaz.util.GoogleDirectionUtil.getUrl;

public class DuringTripActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    @BindView(R.id.car_number_textview)
    TextView carNumberTextview;
    @BindView(R.id.driver_name_textview)
    TextView driverNameTextview;
    @BindView(R.id.driver_rating_textview)
    TextView driverRatingTV;
    @BindView(R.id.driver_cancel_button)
    Button driverCancelButton;
    @BindView(R.id.trip_status_textview)
    TextView tripStatusTextview;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.during_trip_layout)
    ConstraintLayout duringTripLayout;
    @BindView(R.id.finding_your_ride_layout)
    ConstraintLayout findingYourRideLayout;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.other_reason_edittext)
    EditText otherReasonEdittext;
    @BindView(R.id.cancelation_reason_confirm_button)
    Button cancelationReasonConfirmButton;
    @BindView(R.id.cancelation_reason_layout)
    ConstraintLayout cancelationReasonLayout;


    private GoogleMap mMap;
    private SharedPreferences mSharedPreferences;
    private DatabaseReference ref;
    private ValueEventListener valueEventListener;
    private Driver driver;
    private Car car;
    private Trip trip;
    private Bundle bundle;
    private LatLng currentLatLng;
    private long UPDATE_INTERVAL = 5000;  /* 5 secs */
    boolean firstTime = true;
    private String buttonStatus = "arrive";
    private Double customerLng;
    private Double customerLat;
    private Double driverLat;
    private Double driverLng;
    //WaitScreen
    private MenuItem cancelMenuButton;
    private String requestId;
    private boolean  firstStartActivity= true;
    Polyline mRoutePolyline;
    ArrayList<LatLng> MarkerPoints;
    GoogleApiClient mGoogleApiClient;
    private LatLng pickUpLatLng;

    //30 secs 3ado 3shan kda ycancel
    //this variable used for cancel request if not accepted in 30secs
    private boolean requestNotAcceptedYet = true;
    private  Runnable r;
    private Handler handler;
    private String isCustomerInRequest;
    private boolean isDestination = false;
    private ArrayList<String> servicesNames;


    private void driverArrived() {
        tripStatusTextview.setText(R.string.the_driver_has_arrived);
        if (trip.getDestLong() != null && !trip.getDestLong().isEmpty() && trip.getDestLat() != null) {
            isDestination = true;
            //next location update will draw polyline to destination if whinch other wise nothing would happen
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_during_trip);
        ButterKnife.bind(this);
//        registerReceiver(myReceiver, new IntentFilter(MyFirebaseMessagingService.INTENT_FILTER));
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(DuringTripActivity.this);
        MarkerPoints = new ArrayList<>();


        Intent intent = getIntent();
        servicesNames = intent.getStringArrayListExtra("servicesNames");

         isCustomerInRequest = intent.getStringExtra("isCustomerInRequest");
        //if customer is in request handle it in option menu if not continue normal
        if(isCustomerInRequest==null) {
            Double pickUpLat = intent.getDoubleExtra("pickUpLat", 0);
            Double pickUpLong = intent.getDoubleExtra("pickUpLong", 0);
            if (pickUpLat != 0 && pickUpLong != 0) {
                pickUpLatLng = new LatLng(pickUpLat, pickUpLong);
            }


            findingYourRideLayout.setVisibility(View.VISIBLE);
            requestId = getIntent().getStringExtra(Config.REQUEST_ID);
            handler = new Handler();
            r = new Runnable() {
                @Override
                public void run() {
                    if (requestNotAcceptedYet) {
                        sendRequestToAdminPanel();
                    }

                }
            };
            handler.postDelayed(r, 60000);


        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(DuringTripActivity.this);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d("RADIOOO", checkedId + "");
                if (checkedId == R.id.other_reason_radio) {
                    otherReasonEdittext.setBackground(getResources().getDrawable(R.drawable.white_background_border));
                    otherReasonEdittext.setEnabled(true);
                } else {
                    otherReasonEdittext.setBackground(getResources().getDrawable(R.drawable.gray_background_border));
                    otherReasonEdittext.setEnabled(false);
                }


            }
        });
    }

    @OnClick(R.id.cancelation_reason_confirm_button)
    public void onViewClicked() {
        cancelationReasonLayout.setVisibility(View.GONE);
        String cancelationReason ="";
        if(radioGroup.getCheckedRadioButtonId() == R.id.other_reason_edittext){
            cancelationReason = otherReasonEdittext.getText().toString();
        }
        else {
            RadioButton radioButton =  findViewById(radioGroup.getCheckedRadioButtonId());
            cancelationReason = radioButton.getText().toString();
        }
        cancelTrip(cancelationReason);

    }

    //Eventbus triggered when push notification recived if request was accepted or driverArrived
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(AcceptedRequestNotification request) {

        Log.d("SHITBAB",request.getNotificationType());
        String notificationType = request.getNotificationType();

        if(notificationType!=null&& notificationType.equals("driverAcceptedReq")) {
            requestNotAcceptedYet = false;
            Log.d("SHITBAB",request.getTripData().getRequestId()+"");
            progressBar.setVisibility(View.VISIBLE);
            findingYourRideLayout.setVisibility(View.GONE);
            duringTripLayout.setVisibility(View.VISIBLE);
            cancelMenuButton.setVisible(false);

            driver = request.getDriverData();
            car = request.getCarData();
            trip = request.getTripData();

            carNumberTextview.setText(car.getCarNumber());
            driverNameTextview.setText(driver.getName());
            driverRatingTV.setText(driver.getRating()+"");

            Config.driver_name = driver.getName();
            Config.request_id = requestId;
            onMapReady(mMap);
        }
        else if(notificationType!=null&& notificationType.equals("driverArrived")) {
            driverArrived();
            Log.d("SHITBAB","2");
        }
    }


//    // This method will be called when a SomeOtherEvent is posted
//    @Subscribe
//    public void handleSomethingElse(SomeOtherEvent event) {
//        doSomethingWith(event);
//    }
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!firstStartActivity){
            isCustomerInRequest();
        }
        firstStartActivity = false;

    }



    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    //This method is used to check "is user in a a request or not" if the app was in background and then came to foregroung
    public void isCustomerInRequest() {
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.isCustomerInRequest(mSharedPreferences.getString("token", null),requestId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try {
                        String result = response.body().string();
                        Log.d("RESULLT",result);
                        Log.d("RESULLT",requestId+"");
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if(success){
                            parseRequest(jsonObject);
                        }
                        else {
                            String message = jsonObject.getString("message");
                            Log.d("RESULLT", message+requestNotAcceptedYet);
                            if(message.equals("no vendor")&&requestNotAcceptedYet){
                               }
                            else{
                            Intent intent = new Intent(DuringTripActivity.this,MainActivity.class);
                            intent.putExtra("notificationType","canceledReq");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);}
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
                else{
                        Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                    }
            progressBar.setVisibility(View.GONE);

            }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);

        }
    });
}

    public void sendRequestToAdminPanel() {
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.sendRequestAdmin(mSharedPreferences.getString("token", null),requestId,pickUpLatLng.latitude,pickUpLatLng.longitude,
                mSharedPreferences.getString(Config.USER_NAME,""),mSharedPreferences.getString(Config.USER_PHONENUMBER,""),servicesNames).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try {
                        String result = response.body().string();

                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");
                        if(success){
                            Toast.makeText(DuringTripActivity.this, R.string.request_send_to_admin, Toast.LENGTH_SHORT).show();
                        }
                        else {
//                            Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
//                    Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);

            }
        });
    }
    //If there was updated to the trip when the app was in the background it will handle this updates
    private void parseRequest(JSONObject jsonObject) throws JSONException {
        Log.d("parseREQUEST", jsonObject.toString());


        JSONObject message = jsonObject.getJSONObject("message");
        String requestStatus = message.getString("status");
        requestId = message.getString("_id");

        if(requestStatus.equals("requested")){return;}

        progressBar.setVisibility(View.VISIBLE);
        findingYourRideLayout.setVisibility(View.GONE);
        duringTripLayout.setVisibility(View.VISIBLE);
        cancelMenuButton.setVisible(false);
        car = new Car(jsonObject.optString("carNumber"));
        driver = new Driver(jsonObject.optString("driverName"),
                message.optString("driverId"), jsonObject.optString("driverPhoneNumber"),jsonObject.optDouble("rating"));

        Config.driver_name = driver.getName();
        Config.request_id = requestId;

        JSONObject destJSON = message.optJSONObject("destinationLocation");

        String destLat=null;
        String destLong=null;
        if(destJSON!=null){
             destLat = destJSON.optString("lat");
             destLong = destJSON.optString("long");
        }
        trip = new Trip(
                destLat,destLong);
        Log.d("parseREQUEST", trip.getDestLat()+"");
        Log.d("parseREQUEST", trip.getDestLong()+"");
        Log.d("parseREQUEST", requestStatus+"");

       ;
        if(requestStatus.equals("accepted")){
            bindDataAndUpdateSomeVar();
        }
        else if(requestStatus.equals("running")){
            bindDataAndUpdateSomeVar();
            driverArrived();
        }
        else if(requestStatus.equals("finished")){
            Intent intent = new Intent(DuringTripActivity.this,RatingActivity.class);
            intent.putExtra("driver_name", driver.getName());
            intent.putExtra("requestId", requestId);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        else{
//            if(handler!=null){
//            handler.postDelayed(r, 8000);}
//            else {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        handler = new Handler();
//                        r = new Runnable() {
//                            @Override
//                            public void run() {
//                                if (requestNotAcceptedYet) {
//                                    sendRequestToAdminPanel();
//                                }
//
//                            }
//                        };
//                        handler.postDelayed(r, 8000);
//                    }
//                });
//            }
        }
    }


    private void bindDataAndUpdateSomeVar() {
        requestNotAcceptedYet=false;
        carNumberTextview.setText(car.getCarNumber());
        driverNameTextview.setText(driver.getName());
        driverRatingTV.setText(driver.getRating() +"");


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(DuringTripActivity.this);
    }

    private void updateMapNewLocation(Double driverLat, Double driverLng) {

        LatLng origin = new LatLng(driverLat, driverLng);

        LatLng dest;
        if (!isDestination) {
            dest = new LatLng(customerLat, customerLng);
        } else {
            Double destLat = Double.parseDouble(trip.getDestLat());
            Double destLng = Double.parseDouble(trip.getDestLong());
            dest = new LatLng(destLat, destLng);
        }
        // Getting URL to the Google Directions API
        String url = getUrl(origin, dest);
        FetchUrl FetchUrl = new FetchUrl();
        // Start downloading json data from Google Directions API
        FetchUrl.execute(url);
        //USELESS -->
        if (firstTime) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(dest, 14));
            firstTime = false;
        } else {
        }

    }

    public void trackDriverLocation() {
          ref = FirebaseDatabase.getInstance().getReference().child("requests").child(requestId);
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                            for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                if (dataSnapshot.getValue() != null) {
                    driverLat = (Double) dataSnapshot.child("driverLat").getValue();
                    Log.d("HELPMEDURING1", driverLat + "");

                    driverLng = (Double) dataSnapshot.child("driverLng").getValue();
                    Log.d("HELPMEDURING2", driverLng + "");

                    try{
                    customerLat = Double.parseDouble(Objects.requireNonNull(dataSnapshot.child("customerLat").getValue()).toString());
                    Log.d("HELPMEDURING3", customerLat + "");
                    customerLng = Double.parseDouble(Objects.requireNonNull(dataSnapshot.child("customerLng").getValue()).toString());
                    Log.d("HELPMEDURING4", customerLng + "");}
                    catch (NullPointerException e){}

                    if (driverLat != null && driverLng != null && customerLng != null && customerLng != null) {
                        updateMapNewLocation(driverLat, driverLng);
                        //TODO NOT TESTED
                        LatLng customerLatLng = new LatLng(customerLat, customerLng);
                        LatLng driverLatLng = new LatLng(driverLat, driverLng);
                        mMap.addMarker(new MarkerOptions().position(customerLatLng).title("Pickup Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pickup_icon)));
                        mMap.addMarker(new MarkerOptions().position(driverLatLng).title("Rescue Specialist").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_car_icon)));
                        //show Destination icon on map
                        if (trip.getDestLat() != null && !trip.getDestLat().isEmpty() && trip.getDestLong() != null) {
                            LatLng destLatLng = new LatLng(Double.parseDouble(trip.getDestLat()), Double.parseDouble(trip.getDestLong()));
                            mMap.addMarker(new MarkerOptions().position(destLatLng).title("Destination").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_dropoff_icon)));
                        }
                    }

                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressBar.setVisibility(View.GONE);

                Log.d("HELPMEDURING", "nooo");

            }
        };
        ref.addValueEventListener(valueEventListener);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.request_service_menu, menu);
        cancelMenuButton = menu.findItem(R.id.app_bar_cancel_trip_button);

        if(isCustomerInRequest!=null){
            try {
                JSONObject jsonObject = new JSONObject(isCustomerInRequest);
                parseRequest(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.app_bar_cancel_trip_button){
            cancelTripBeforeAccept();
            return  true;
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //Initialize Google Play Services
        if(pickUpLatLng!=null){
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pickUpLatLng, 14));
        }

        else if (Config.userLocation!=null){
            currentLatLng = Config.userLocation;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, MAP_ZOOM_LEVEL));
        }
        buildGoogleApiClient();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        trackDriverLocation();
        Log.e("ERROR", "3");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("ERROR", "21");
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("ERROR", "2");
        progressBar.setVisibility(View.GONE);

    }


    @OnClick({R.id.driver_cancel_button, R.id.driver_call_button})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.driver_cancel_button:
                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(this);
                builder.setTitle("Cancel Trip Confirmation")
                        .setMessage("Are you sure that you want to cancel the trip?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                cancelationReasonLayout.setVisibility(View.VISIBLE);
                                }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
                break;
            case R.id.driver_call_button:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + driver.getPhoneNumber()));
                startActivity(intent);
                break;
        }
    }

    public void onCenterMyLocationIconClicked(View view) {
            if (currentLatLng != null && mMap != null) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 14));
            }
    }

    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DataParser parser = new DataParser();

                // Starts parsing data
                routes = parser.parse(jObject);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                Bitmap sourceBitmap = createBitmapFromDrawable(DuringTripActivity.this,
                        R.drawable.map_car_icon);
                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                float density = getResources().getDisplayMetrics().density;
                float width = (3 * density);
                lineOptions.width(width);
                lineOptions.color(Color.GRAY).zIndex(4).startCap(new CustomCap(BitmapDescriptorFactory.fromBitmap(sourceBitmap)));

                Log.d("onPostExecute", "onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mMap.clear();
                LatLng customerLatLng = new LatLng(customerLat, customerLng);
                mMap.addMarker(new MarkerOptions().position(customerLatLng).title("Pickup Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pickup_icon)));

                //show Destination icon on map
                if (trip.getDestLat() != null && !trip.getDestLat().isEmpty() && trip.getDestLong() != null) {
                    LatLng destLatLng = new LatLng(Double.parseDouble(trip.getDestLat()), Double.parseDouble(trip.getDestLong()));
                    mMap.addMarker(new MarkerOptions().position(destLatLng).title("Destination").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_dropoff_icon)));
                }
//                mMap.addMarker(new MarkerOptions().position(currentLatLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_my_location_now)));
                mMap.addPolyline(lineOptions);
                mRoutePolyline = mMap.addPolyline(lineOptions);
                float density = getResources().getDisplayMetrics().density;
                float width = (3 * density);
                ObjectAnimator.ofFloat(mRoutePolyline, "width",
                        0f, width).setDuration(500).start();
            } else {
                Log.d("onPostExecute", "without Polylines drawn");
            }
            progressBar.setVisibility(View.GONE);

        }
    }

    public static Bitmap createBitmapFromDrawable(Activity activity, int resource) {
        Drawable drawable = activity.getResources().getDrawable(resource);
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

    }


    public void cancelTrip(String cancellationReason) {
        driverCancelButton.setEnabled(false);
        driverCancelButton.setAlpha(.7f);
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.cancelTrip(mSharedPreferences.getString("token", null)
                , requestId,cancellationReason).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("END RESPONSE:", result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");

                        if (success) {
                            FirebaseDatabase.getInstance().getReference().child("requests").child(requestId).removeValue();
                            Intent intent = new Intent(DuringTripActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                            Toast.makeText(DuringTripActivity.this, "Trip is canceled", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(DuringTripActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                }
                progressBar.setVisibility(View.GONE);
                driverCancelButton.setEnabled(true);
                driverCancelButton.setAlpha(1f);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                driverCancelButton.setEnabled(true);
                driverCancelButton.setAlpha(1f);

            }
        });
    }
    public void cancelTripBeforeAccept() {
        cancelMenuButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.cancelTripBeforeAccept(mSharedPreferences.getString("token",null)
                , requestId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("END RESPONSE:", result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");

                        if (success) {
                            FirebaseDatabase.getInstance().getReference().child("requests").child(requestId).removeValue();
                            Intent intent = new Intent(DuringTripActivity.this,MainActivity.class);
                            startActivity(intent);
                            finish();

                            Toast.makeText(DuringTripActivity.this, R.string.trip_is_canceled, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(DuringTripActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                }
                progressBar.setVisibility(View.GONE);
                cancelMenuButton.setEnabled(true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                cancelMenuButton.setEnabled(true);

            }
        });
    }

    public void cancelAfter30Secs() {
        cancelMenuButton.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        enqazService.cancelTripBeforeAccept(mSharedPreferences.getString("token",null)
                , requestId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String result = response.body().string();
                        Log.d("END RESPONSE:", result);
                        JSONObject jsonObject = new JSONObject(result);
                        boolean success = jsonObject.getBoolean("success");

                        if (success) {
                            FirebaseDatabase.getInstance().getReference().child("requests").child(requestId).removeValue();
                            finish();
                                Intent intent = new Intent(DuringTripActivity.this,MainActivity.class);
                                intent.putExtra("notificationType","CanceledNoDrivers");
                                startActivity(intent);
                            Toast.makeText(DuringTripActivity.this, "Trip is canceled", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(DuringTripActivity.this, "" + jsonObject.optString("message"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();

                }
                progressBar.setVisibility(View.GONE);
                cancelMenuButton.setEnabled(true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(DuringTripActivity.this, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                cancelMenuButton.setEnabled(true);

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(handler!=null){
        handler.removeCallbacksAndMessages(null);}
        if(handler!=null&&r!=null){
            handler.removeCallbacks(r);}

    }



    @Override
    protected void onDestroy() {
        if(ref!=null&&valueEventListener!=null){
        ref.removeEventListener(valueEventListener);}
        if(mGoogleApiClient!=null){
        mGoogleApiClient.disconnect();}
//        unregisterReceiver(myReceiver);
        finish();
        super.onDestroy();
    }
}
