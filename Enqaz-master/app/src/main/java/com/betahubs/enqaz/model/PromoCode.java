package com.betahubs.enqaz.model;

public class PromoCode {
    public String id;
    public String code;
    public String expiresAt;
    public Integer discountRate;
    public Integer v;

    public String getId() {
        return id;
    }



    public String getCode() {
        return code;
    }

    public String getExpiresAt() {
        return expiresAt;
    }

    public Integer getDiscountRate() {
        return discountRate;
    }

    public Integer getV() {
        return v;
    }
}
