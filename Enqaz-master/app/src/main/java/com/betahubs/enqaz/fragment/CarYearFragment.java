//package com.betahubs.enqaz.fragment;
//
//import android.content.Context;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.ListView;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.betahubs.enqaz.R;
//import com.betahubs.enqaz.api.ApiServiceUtil;
//import com.betahubs.enqaz.api.EnqazService;
//import com.betahubs.enqaz.util.SelectCarListItemClickListener;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
//import okhttp3.ResponseBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//public class CarYearFragment extends Fragment {
//
//    @BindView(R.id.listView)
//    ListView listView;
//    @BindView(R.id.progress_bar)
//    ProgressBar progressBar;
//    Unbinder unbinder;
//    @BindView(R.id.textview)
//    TextView textiew;
//    private Context mContext;
////    private OnFragmentInteractionListener mListener;
//     private SelectCarListItemClickListener mOnClickListener;
//     private String[] manfactories;
//
//    public CarYearFragment() {
//        // Required empty public constructor
//    }
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_car_manufactory, container, false);
//        unbinder = ButterKnife.bind(this, view);
//        mContext = getContext();
//        handleBackPress(view);
//        textiew.setText(R.string.select_car_year);
//
//        mOnClickListener = (SelectCarListItemClickListener) getParentFragment();
//        if(mOnClickListener==null){
//            mOnClickListener = (SelectCarListItemClickListener) getContext();
//        }
//        String carModel = getArguments().getString("carModel","");
//            getCarYear(carModel);
//
//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    mOnClickListener.onSelectCarItemClick("carYear",manfactories[position]);
//                }
//            });
//        return view;
//    }
//
//    public void getCarYear(String carModel) {
//        progressBar.setVisibility(View.VISIBLE);
//        EnqazService enqazService = ApiServiceUtil.getEnqazService();
//        enqazService.getCarYears(carManufacturerId, carModel).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if (response.isSuccessful()) {
//                    try {
//                        String res = response.body().string();
//
//                        JSONObject jsonObject = new JSONObject(res);
//                        boolean success = jsonObject.getBoolean("success");
//                        if (success) {
//                            JSONArray manfs = jsonObject.getJSONArray("message");
//                            manfactories = new String[manfs.length()];
//                            for(int i=0;i < manfs.length();i++){
//                                JSONObject carmanfs = manfs.getJSONObject(i);
//                                manfactories[i] = carmanfs.getString("year");
//                            }
//                            ArrayAdapter mAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,manfactories);
//                            listView.setAdapter(mAdapter);
//                        } else {
//                            Toast.makeText(mContext, "" + jsonObject.getString("message"), Toast.LENGTH_LONG).show();
//                        }
//                    } catch (IOException | JSONException |NullPointerException e) {
//                        e.printStackTrace();
//                    }
//
//                } else {
//                    Toast.makeText(mContext, getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
//                }
//                progressBar.setVisibility(View.GONE);
//
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Toast.makeText(mContext, getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
//                progressBar.setVisibility(View.GONE);
//
//            }
//        });
//    }
//
//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//
//    }
//
//    //To handle on back button pressed from fragment (without this onBackPressed() method will be called on mainActivity and will be closed)
//
//    private void handleBackPress(View view) {
//        view.setFocusableInTouchMode(true);
//        view.requestFocus();
//        view.setOnKeyListener( new View.OnKeyListener()
//        {
//            @Override
//            public boolean onKey( View v, int keyCode, KeyEvent event )
//            {
//                if( keyCode == KeyEvent.KEYCODE_BACK )
//                {
//                    mOnClickListener.onSelectCarItemClick("ClickEvent","onBackPressed");
//
//                    return true;
//                }
//                return false;
//            }
//        } );
//    }
//
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        unbinder.unbind();
//    }
//
//
////
////    public interface OnFragmentInteractionListener {
////        // TODO: Update argument type and name
////        void onFragmentInteraction(Uri uri);
////    }
//}
