package com.betahubs.enqaz.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import java.util.ArrayList;

public class ScheduleHistory implements Parcelable {


    public String firebaseKey;
    public String id;
    public String carManufactory;
    public String carModel;
    public Double customerLat;
    public Double customerLng;
    public long day;
    public Double destLat;
    public Double destLng;
    public String estimatedFare;
    public long hour;
    public long min;
    public long month;
    public String paymentType;
    public ArrayList<String> serviceId;
    public ArrayList<String> serviceName;
    public String token;
    public String userId;
    public long year;

    public String getFirebaseKey() {
        return firebaseKey;
    }

    public void setFirebaseKey(String firebaseKey) {
        this.firebaseKey = firebaseKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCarManufactory() {
        return carManufactory;
    }

    public void setCarManufactory(String carManufactory) {
        this.carManufactory = carManufactory;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public Double getCustomerLat() {
        return customerLat;
    }

    public void setCustomerLat(Double customerLat) {
        this.customerLat = customerLat;
    }

    public Double getCustomerLng() {
        return customerLng;
    }

    public void setCustomerLng(Double customerLng) {
        this.customerLng = customerLng;
    }

    public long getDay() {
        return day;
    }

    public void setDay(long day) {
        this.day = day;
    }

    public Double getDestLat() {
        return destLat;
    }

    public void setDestLat(Double destLat) {
        this.destLat = destLat;
    }

    public Double getDestLng() {
        return destLng;
    }

    public void setDestLng(Double destLng) {
        this.destLng = destLng;
    }

    public String getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(String estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public long getHour() {
        return hour;
    }

    public void setHour(long hour) {
        this.hour = hour;
    }

    public long getMin() {
        return min;
    }

    public void setMin(long min) {
        this.min = min;
    }

    public long getMonth() {
        return month;
    }

    public void setMonth(long month) {
        this.month = month;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public ArrayList<String> getServiceId() {
        return serviceId;
    }

    public void setServiceId(ArrayList<String> serviceId) {
        this.serviceId = serviceId;
    }

    public ArrayList<String> getServiceName() {
        return serviceName;
    }

    public void setServiceName(ArrayList<String> serviceName) {
        this.serviceName = serviceName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.firebaseKey);
        dest.writeString(this.id);
        dest.writeString(this.carManufactory);
        dest.writeString(this.carModel);
        dest.writeValue(this.customerLat);
        dest.writeValue(this.customerLng);
        dest.writeLong(this.day);
        dest.writeValue(this.destLat);
        dest.writeValue(this.destLng);
        dest.writeString(this.estimatedFare);
        dest.writeLong(this.hour);
        dest.writeLong(this.min);
        dest.writeLong(this.month);
        dest.writeString(this.paymentType);
        dest.writeStringList(this.serviceId);
        dest.writeStringList(this.serviceName);
        dest.writeString(this.token);
        dest.writeString(this.userId);
        dest.writeLong(this.year);
    }

    public ScheduleHistory() {
    }

    protected ScheduleHistory(Parcel in) {
        this.firebaseKey = in.readString();
        this.id = in.readString();
        this.carManufactory = in.readString();
        this.carModel = in.readString();
        this.customerLat = (Double) in.readValue(Double.class.getClassLoader());
        this.customerLng = (Double) in.readValue(Double.class.getClassLoader());
        this.day = in.readLong();
        this.destLat = (Double) in.readValue(Double.class.getClassLoader());
        this.destLng = (Double) in.readValue(Double.class.getClassLoader());
        this.estimatedFare = in.readString();
        this.hour = in.readLong();
        this.min = in.readLong();
        this.month = in.readLong();
        this.paymentType = in.readString();
        this.serviceId = in.createStringArrayList();
        this.serviceName = in.createStringArrayList();
        this.token = in.readString();
        this.userId = in.readString();
        this.year = in.readLong();
    }

    public static final Creator<ScheduleHistory> CREATOR = new Creator<ScheduleHistory>() {
        @Override
        public ScheduleHistory createFromParcel(Parcel source) {
            return new ScheduleHistory(source);
        }

        @Override
        public ScheduleHistory[] newArray(int size) {
            return new ScheduleHistory[size];
        }
    };
}
