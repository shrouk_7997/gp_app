package com.betahubs.enqaz;

import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.betahubs.enqaz.app.BaseActivity;
import com.betahubs.enqaz.model.ScheduleHistory;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ScheduleHistoryTripDetailsActivity extends BaseActivity implements OnMapReadyCallback {
    @BindView(R.id.date_textview)
    TextView dateTextview;
    @BindView(R.id.address_from_textview)
    TextView addressFromTextview;
    @BindView(R.id.address_to_textview)
    TextView addressToTextview;
    @BindView(R.id.car_model_textview)
    TextView carModelTextview;
    @BindView(R.id.service_model_textview2)
    TextView serviceModelTextview2;

    @BindView(R.id.payment_type_textview)
    TextView paymentTypeTextView;
    @BindView(R.id.estimation_details_total_cost_tv)
    TextView estimationDetailsTotalCostTv;
    @BindView(R.id.login_progress)
    ProgressBar progressBar;
    private SharedPreferences mSharedPreferences;
    private ScheduleHistory scheduleHistory;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_history_trip_details);
        ButterKnife.bind(this);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        scheduleHistory = getIntent().getParcelableExtra("scheduleHistory");
        getSupportActionBar().setHomeButtonEnabled(true);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        updateActivtiy();

    }

    public void updateActivtiy() {
        updateUI();
        updateMap();
    }


    private void updateUI() {
        //Next method used to prevent mints from being just one number like this 9:3 instead it will be 9:03
        String selectedMintString;
        if (scheduleHistory.getMin() < 10) selectedMintString = "0" + scheduleHistory.getMin();
        else selectedMintString = scheduleHistory.getMin() + "";
        dateTextview.setText(scheduleHistory.getDay() + "/" + scheduleHistory.getMonth() + "/" + scheduleHistory.getYear() +
                " at " + scheduleHistory.getHour() + ":" + selectedMintString);

        carModelTextview.setText(scheduleHistory.getCarManufactory() + "/" + scheduleHistory.getCarModel());

        StringBuffer servicesNames = new StringBuffer();
        for (int i = 0; i < scheduleHistory.getServiceName().size(); i++) {
            servicesNames.append(scheduleHistory.getServiceName().get(i) + " ");
        }
        serviceModelTextview2.setText(servicesNames.toString());
        estimationDetailsTotalCostTv.setText(scheduleHistory.getEstimatedFare() + " L.E");

        paymentTypeTextView.setText(scheduleHistory.getPaymentType());
        if (scheduleHistory.getCustomerLat() != null) {
            String from = getAddress(scheduleHistory.getCustomerLat(), scheduleHistory.getCustomerLng());
            addressFromTextview.setText(from);
        }
        if (scheduleHistory.getDestLat() != null) {
            String to = getAddress(scheduleHistory.getDestLat(), scheduleHistory.getDestLng());
            addressToTextview.setText(to);
        }

    }

    public String getAddress(Double lat, Double lng) {

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses != null && addresses.size() > 0 && addresses.get(0) != null) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                return address;
            }
        } catch (IOException ignored) {
            return "";
        }
        return "";
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        updateMap();
    }

    public void updateMap() {
        if (mMap != null) {
            LatLng startTripLocation = new LatLng(scheduleHistory.getCustomerLat(), scheduleHistory.getCustomerLng());
            Marker marker2 = null;
            if (scheduleHistory.getDestLat() != null) {
                LatLng endTripLocation = new LatLng(scheduleHistory.getDestLat(), scheduleHistory.getDestLng());
                marker2 = mMap.addMarker(new MarkerOptions().position(endTripLocation).title("Me").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_dropoff_icon)));
            }
            LatLng centerLocation;
            if (scheduleHistory.getDestLat() != null) {
                centerLocation = new LatLng((scheduleHistory.getCustomerLat() + scheduleHistory.getDestLat()) / 2,
                        (scheduleHistory.getCustomerLng() + scheduleHistory.getDestLng()) / 2);
            } else {
                centerLocation = startTripLocation;
            }

            Marker marker1 = mMap.addMarker(new MarkerOptions().position(startTripLocation).title("Me").icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pickup_icon)));

//            LatLngBounds.Builder builder = new LatLngBounds.Builder();
//                builder.include(marker1.getPosition());
//            if (marker2 != null) {
//                builder.include(marker2.getPosition());
//            }
//            LatLngBounds bounds = builder.build();
//            int padding = 10; // offset from edges of the map in pixels
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(centerLocation, 14));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return false;
    }
}
