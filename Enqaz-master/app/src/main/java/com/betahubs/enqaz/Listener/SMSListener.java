//package com.betahubs.enqaz.Listener;
//
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.telephony.SmsMessage;
//
//public class SMSListener extends BroadcastReceiver {
//
//    private static OTPListener mListener;
//
//    @Override
//    public void onReceive(Context context, Intent intent) {
//        Bundle data = intent.getExtras();
//
//        Object[] pdus = new Object[0];
//        if (data != null) {
//            pdus = (Object[]) data.get("pdus");
//        }
//
//        if (pdus != null) {
//            for (Object pdu : pdus) {
//                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
//                if (mListener!=null && smsMessage.getDisplayMessageBody().startsWith("Enqaz"))
//                    mListener.onOTPReceived(smsMessage.getDisplayMessageBody().substring(27,33));
//                break;
//            }
//        }
//    }
//
//    public static void bindListener(OTPListener listener) {
//        mListener = listener;
//    }
//
//    public static void unbindListener() {
//        mListener = null;
//    }
//}
