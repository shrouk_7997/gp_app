//package com.betahubs.enqaz.fragment;
//
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.ListView;
//import android.widget.ProgressBar;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.betahubs.enqaz.R;
//import com.betahubs.enqaz.api.ApiServiceUtil;
//import com.betahubs.enqaz.api.EnqazService;
//import com.betahubs.enqaz.util.SelectCarListItemClickListener;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import butterknife.Unbinder;
//import okhttp3.ResponseBody;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//import retrofit2.Retrofit;
//
//public class CarModelFragment extends Fragment {
//
//    @BindView(R.id.listView)
//    ListView listView;
//    @BindView(R.id.progress_bar)
//    ProgressBar progressBar;
//    Unbinder unbinder;
//    @BindView(R.id.textview)
//    TextView textiew;
////    private OnFragmentInteractionListener mListener;
//     private SelectCarListItemClickListener mOnClickListener;
//     private String[] manfactories;
//
//    public CarModelFragment() {
//        // Required empty public constructor
//    }
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_car_manufactory, container, false);
//        unbinder = ButterKnife.bind(this, view);
//        handleBackPress(view);
//        textiew.setText(R.string.select_car_model);
//
//        mOnClickListener = (SelectCarListItemClickListener) getParentFragment();
//        if(mOnClickListener==null){
//            mOnClickListener = (SelectCarListItemClickListener) getContext();
//        }
//            String carManufacturer = getArguments().getString("carManufacturer","");
//            getCarManufactories(carManufacturer);
//            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    mOnClickListener.onSelectCarItemClick("carModel",manfactories[position]);
//                }
//            });
//        return view;
//    }
//
//    public void getCarManufactories(String carManufacturer) {
//        progressBar.setVisibility(View.VISIBLE);
//        EnqazService enqazService = ApiServiceUtil.getEnqazService();
//        enqazService.getCarModels(carManufacturer).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if (response.isSuccessful()) {
//                    try {
//                        String res = response.body().string();
//
//                        JSONObject jsonObject = new JSONObject(res);
//                        boolean success = jsonObject.getBoolean("success");
//                        if (success) {
//                            JSONArray manfs = jsonObject.getJSONArray("cars");
//                            manfactories = new String[manfs.length()];
//                            for(int i=0;i < manfs.length();i++){
//                                JSONObject carmanfs = manfs.getJSONObject(i);
//                                manfactories[i] = carmanfs.getString("carModel");
//                            }
//                            ArrayAdapter mAdapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_list_item_1,manfactories);
//                            listView.setAdapter(mAdapter);
//                        } else {
//                            Toast.makeText(getContext(), "" + jsonObject.getString("message"), Toast.LENGTH_LONG).show();
//                        }
//                    } catch (IOException | JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                } else {
//                    Toast.makeText(getContext(), getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
//                }
//                progressBar.setVisibility(View.GONE);
//
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Toast.makeText(getContext(), getString(R.string.some_thing_went_wrong_please_try_later), Toast.LENGTH_SHORT).show();
//                progressBar.setVisibility(View.GONE);
//
//            }
//        });
//    }
//
//
//    private void handleBackPress(View view) {
//        view.setFocusableInTouchMode(true);
//        view.requestFocus();
//        view.setOnKeyListener( new View.OnKeyListener()
//        {
//            @Override
//            public boolean onKey( View v, int keyCode, KeyEvent event )
//            {
//                if( keyCode == KeyEvent.KEYCODE_BACK )
//                {
//                    mOnClickListener.onSelectCarItemClick("ClickEvent","onBackPressed");
//
//                    return true;
//                }
//                return false;
//            }
//        } );
//    }
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        unbinder.unbind();
//    }
//}
