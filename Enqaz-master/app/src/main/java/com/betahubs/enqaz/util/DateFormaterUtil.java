package com.betahubs.enqaz.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateFormaterUtil {

    public static String formatToDateOnly(String date){
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy/MM/dd");

        sourceFormat.setTimeZone(utc);
        try {
            Date convertedDate = sourceFormat.parse(date);
            String a =destFormat.format(convertedDate);
            return a;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String formatToDateAndTime(String date){
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        sourceFormat.setTimeZone(utc);
        try {
            Date convertedDate = sourceFormat.parse(date);
            String a = destFormat.format(convertedDate);
            return a;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
