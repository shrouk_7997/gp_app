package com.betahubs.enqaz.fragment;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.betahubs.enqaz.R;
import com.betahubs.enqaz.adapter.HistoryAdapter;
import com.betahubs.enqaz.api.ApiServiceUtil;
import com.betahubs.enqaz.api.EnqazService;
import com.betahubs.enqaz.model.History;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HistoryFragment extends Fragment {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.no_history_layout)
    ConstraintLayout noHistoryLayout;

    private RecyclerView.Adapter mAdapter;
    private SharedPreferences mSharedPreferences;
    private Context mContext;
    Unbinder unbind;
    private Call mCall;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        unbind = ButterKnife.bind(this, view);
        mContext = getContext();

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        getHistory();
        return view;
    }


    public void getHistory() {
        progressBar.setVisibility(View.VISIBLE);
        EnqazService enqazService = ApiServiceUtil.getEnqazService();
        mCall = enqazService.getHistory(mSharedPreferences.getString("token", null));
       mCall.enqueue(new Callback<History>() {
            @Override
            public void onResponse(Call<History> call, Response<History> response) {
                if (response.isSuccessful()) {
                    History result = response.body();
                    if (result != null && result.getSuccess()) {
                        ArrayList<History.Data> data = result.getData();
                        if(data.size()==0){
                            noHistoryLayout.setVisibility(View.VISIBLE);
                        }
                        mAdapter = new HistoryAdapter(mContext, data);
                        recyclerView.setAdapter(mAdapter);
                    } else {
                        noHistoryLayout.setVisibility(View.VISIBLE);
                    }
                } else {
                    noHistoryLayout.setVisibility(View.VISIBLE);
                }
                progressBar.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Call<History> call, Throwable t) {
                if(progressBar!=null) {
                    progressBar.setVisibility(View.GONE);
                }

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(mCall!=null)mCall.cancel();
    }
}
