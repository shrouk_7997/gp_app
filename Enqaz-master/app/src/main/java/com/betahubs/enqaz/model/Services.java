package com.betahubs.enqaz.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Services {
    private boolean success;
    private String message;
    private ArrayList<Service> services;

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<Service> getServices() {
        return services;
    }

    public static class Service {
        private String _id;
        private String name;
        private String description;
        private Integer baseFare;
        private boolean active;

        public Service(String name) {
            this.name = name;
        }

        public String get_id() {
            return _id;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }

        public Integer getBaseFare() {
            return baseFare;
        }

        public boolean isActive() {
            return active;
        }
    }
}